/* 
 *    HMAC generator for strings. 
 * 
 *    Author: andrei.sambra@telecom-sudparis.eu 
 *    GPLv3 License applies to this code. 
 * 
 * */ 
#include <linux/module.h> /* Needed by all modules */ 
#include <linux/kernel.h> /* Needed for KERN_INFO */ 
#include "linux/init.h" /* Needed for the macros */ 
#include "linux/crypto.h" 
#include "linux/err.h" 
#include "linux/scatterlist.h" 
#include "linux/gfp.h" 
#include "crypto/algapi.h" 
#include "hmac.c"
 
/* 
 * Desired output: 694c7f0b84c0a8f0043c20aea9355a7eb51647f3 
 */ 
static char hmac[40]; 
static char *key = "passphrase"; 
static char *mystring = "22"; 


/* 
static int hmac_sha1(char *key, size_t klen, // key and key length 
			char *data_in, size_t dlen, // data in and length 
			    char *hash_out, size_t outlen) {

 
 return 0; 
}
*/
static int __init init_main(void) 
{ 
    printk(KERN_INFO "[CRYPTO] -> Successfully loaded crypto module.\n"); 
    struct crypto_shash hash;
    struct shash_desc ctx;
    hmac_init (&ctx);
    hmac_setkey(&hash,key,strlen(key));
    hmac_update(&ctx,mystring,strlen(mystring));
    hmac_final(&ctx,hmac); 
    //hmac_sha1(key, strlen(key), mystring, strlen(mystring), hmac, sizeof(hmac)); 
    crypto_free_shash (&hash);
    printk(KERN_INFO "FINAL MAC:%s", hmac); 
 
    return 0; 
} 
 
static void __exit cleanup_main(void) 
{ 
     printk(KERN_INFO "[CRYPTO] < - Successfully unloaded crypto module.\n"); 

} 
 
module_init(init_main); 
module_exit(cleanup_main); 
 
/*  
 *    Declaring code as GPL.  
 */ 
MODULE_LICENSE("GPL"); 
MODULE_AUTHOR("Andrei Sambra - andrei.sambra@telecom-sudparis.eu"); /* Who wrote this module? */ 
MODULE_DESCRIPTION("SHA1-HMAC"); /* What does this module do */ 
