#include <linux/module.h> /* Needed by all modules */
#include <linux/kernel.h> /* Needed for KERN_INFO */
#include <linux/init.h> /* Needed for the macros */
#include <linux/crypto.h>
#include <linux/err.h>
#include <linux/scatterlist.h>
#include <linux/gfp.h>
#include <crypto/hash.h>
#include "hmac.c"

/*
 * Desired output: 694c7f0b84c0a8f0043c20aea9355a7eb51647f3
 */
static char hmac[40];
static char *key = "passphrase";
static char *mystring = "22";

static int __init init_main(void)
{
	printk(KERN_INFO "[CRYPTO] -> Successfully loaded crypto module.\n");

	hmac_sha1(key, strlen(key), mystring, strlen(mystring), hmac, sizeof(hmac));
	printk(KERN_INFO "FINAL MAC:%s", hmac);

	return 0;
}

static void __exit cleanup_main(void)
{

#if DEBUG > 0
	printk(KERN_INFO "[CRYPTO] < - Successfully unloaded crypto module.\n");
#endif
}

module_init(init_main);
module_exit(cleanup_main);
