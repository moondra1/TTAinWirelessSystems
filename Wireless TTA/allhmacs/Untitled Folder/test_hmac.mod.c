#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x1635e2b6, "module_layout" },
	{ 0xf0fdf6cb, "__stack_chk_fail" },
	{ 0x5f2bf844, "hmac_sha256" },
	{ 0xd0d8621b, "strlen" },
	{ 0x4f68e5c9, "do_gettimeofday" },
	{ 0x50eedeb8, "printk" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=hmac_self";


MODULE_INFO(srcversion, "4F32C9F52DE778C3AF07114");
