#include <linux/module.h>
#include <linux/init.h>
#include <linux/in.h>
#include <net/sock.h>
#include <linux/skbuff.h>
#include <linux/delay.h>
#include <linux/inet.h>
#include <linux/string.h>
#include "hmac.c"

#define SERVER_PORT 5556
static struct socket *udpsocket=NULL;
static struct socket *clientsocket=NULL;

static char hmac[40];
static char *key = "passphrase";

static DECLARE_COMPLETION( threadcomplete );
struct workqueue_struct *wq;

struct wq_wrapper{
        struct work_struct worker;
    struct sock * sk;
};

struct wq_wrapper wq_data;

static void cb_data(struct sock *sk, int bytes){
    wq_data.sk = sk;
    queue_work(wq, &wq_data.worker);
}

void send_answer(struct work_struct *data){
    struct  wq_wrapper * foo = container_of(data, struct  wq_wrapper, worker);
    int len = 0;
    /* as long as there are messages in the receive queue of this socket*/
    while((len = skb_queue_len(&foo->sk->sk_receive_queue)) > 0){
        struct sk_buff *skb = NULL;
        unsigned short * port;
        int len;
        struct msghdr msg;
        struct iovec iov;
        mm_segment_t oldfs;
        struct sockaddr_in to;
        char temp_message[20];
        char inmessage[51];
        char message[51];
        /* receive packet */
        skb = skb_dequeue(&foo->sk->sk_receive_queue);
        printk(KERN_INFO "[CRYPTO] -> Successfully loaded crypto module.\n");
        memset(&inmessage[0], 0, sizeof(inmessage));
        //hmac_sha1(key, strlen(key), skb->data, strlen(skb->data), hmac, sizeof(hmac));
        //printk(KERN_INFO "FINAL MAC:%s", hmac);

        printk("message len: %i message: %s\n", skb->len - 8, skb->data +8); /*8 for udp header*/
        strcpy(temp_message,skb->data +8);    
        strncpy(inmessage,temp_message,11);
        printk("message len: %i inmessage: %s\n", strlen(inmessage), inmessage);    

        hmac_sha1(key, strlen(key), inmessage, 11, hmac, sizeof(hmac));
        printk("message len: %i  FINAL MAC:%s \n",strlen(hmac), hmac);
        
        int temp=0;
        int ptr = 11;
        while(temp<40)
        {   
            inmessage[ptr++]=  hmac[temp++];
        }

        memset(&message[0], 0, sizeof(message));
        printk("message len: %i inmessage: %s\n", strlen(inmessage), inmessage);    
        strncpy(message,inmessage,51);
        printk( "message len: %i FINAL MSG:%s \n",strlen(message),message);

        /* generate answer message */
        memset(&to,0, sizeof(to));
        to.sin_family = AF_INET;
        //to.sin_addr.s_addr = in_aton("127.0.0.1");  
        to.sin_addr.s_addr = in_aton("192.168.56.103");
        port = (unsigned short *)skb->data;
        to.sin_port = *port;
	//to.sin_port = (unsigned short) 3879;
	//printk(KERN_INFO"Port: %i \n",to.sin_port);
        memset(&msg,0,sizeof(msg));
        msg.msg_name = &to;
        msg.msg_namelen = sizeof(to);
        /* send the message back */
        iov.iov_base = inmessage;
        iov.iov_len  = 51;
        msg.msg_control = NULL;
        msg.msg_controllen = 0;
        msg.msg_iov = &iov;
        msg.msg_iovlen = 1;
        /* adjust memory boundaries */    
        oldfs = get_fs();
        set_fs(KERNEL_DS);
        len = sock_sendmsg(clientsocket, &msg, 51);
        set_fs(oldfs);
        /* free the initial skb */
        kfree_skb(skb);
    }
}

static int __init server_init( void )
{
    struct sockaddr_in server;
    int servererror;
    printk("INIT MODULE\n");
    /* socket to receive data */
    if (sock_create(PF_INET, SOCK_DGRAM, IPPROTO_UDP, &udpsocket) < 0) {
        printk( KERN_ERR "server: Error creating udpsocket.n" );
        return -EIO;
    }
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( (unsigned short)SERVER_PORT);
  
    servererror = udpsocket->ops->bind(udpsocket, (struct sockaddr *) &server, sizeof(server ));
    if (servererror) {
        sock_release(udpsocket);
        return -EIO;
    }
    udpsocket->sk->sk_data_ready = cb_data;
    
    /* create work queue */    
    INIT_WORK(&wq_data.worker, send_answer);
    wq = create_singlethread_workqueue("myworkqueue");
    if (!wq){
        return -ENOMEM;
    }
    
    /* socket to send data */
    if (sock_create(PF_INET, SOCK_DGRAM, IPPROTO_UDP, &clientsocket) < 0) {
        printk( KERN_ERR "server: Error creating clientsocket.n" );
        return -EIO;
    }
    return 0;
}

static void __exit server_exit( void )
{
    if (udpsocket)
        sock_release(udpsocket);
    if (clientsocket)
        sock_release(clientsocket);

    if (wq) {
                flush_workqueue(wq);
                destroy_workqueue(wq);
    }
    printk("EXIT MODULE");
}

module_init(server_init);
module_exit(server_exit);
MODULE_LICENSE("GPL"); 
