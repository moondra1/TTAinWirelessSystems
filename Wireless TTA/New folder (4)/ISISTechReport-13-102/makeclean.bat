@echo Delete all *.bak / *.aux / *.dvi / *.log / *.out / *.toc files
del *.bak
del *.aux
del *.dvi
del *.log
del *.out
del *.toc
del *.blg
del *.bbl
del *.gz