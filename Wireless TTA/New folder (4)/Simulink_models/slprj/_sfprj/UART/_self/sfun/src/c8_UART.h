#ifndef __c8_UART_h__
#define __c8_UART_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc8_UARTInstanceStruct
#define typedef_SFc8_UARTInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c8_sfEvent;
  uint8_T c8_tp_IDLE_TX;
  uint8_T c8_tp_LOAD_TX;
  uint8_T c8_tp_SHIFT_TX;
  uint8_T c8_tp_STOP_TX;
  boolean_T c8_isStable;
  uint8_T c8_is_active_c8_UART;
  uint8_T c8_is_c8_UART;
  uint8_T c8_TX_reg[10];
  uint8_T c8_TX_reg_in[8];
  uint16_T c8_TX_bit_count;
  uint8_T c8_doSetSimStateSideEffects;
  const mxArray *c8_setSimStateSideEffectsInfo;
} SFc8_UARTInstanceStruct;

#endif                                 /*typedef_SFc8_UARTInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c8_UART_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c8_UART_get_check_sum(mxArray *plhs[]);
extern void c8_UART_method_dispatcher(SimStruct *S, int_T method, void *data);

#endif
