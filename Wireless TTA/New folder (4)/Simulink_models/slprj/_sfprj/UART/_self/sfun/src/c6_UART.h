#ifndef __c6_UART_h__
#define __c6_UART_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc6_UARTInstanceStruct
#define typedef_SFc6_UARTInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c6_sfEvent;
  uint8_T c6_tp_S1;
  uint8_T c6_tp_S2;
  boolean_T c6_isStable;
  uint8_T c6_is_active_c6_UART;
  uint8_T c6_is_c6_UART;
  uint8_T c6_doSetSimStateSideEffects;
  const mxArray *c6_setSimStateSideEffectsInfo;
} SFc6_UARTInstanceStruct;

#endif                                 /*typedef_SFc6_UARTInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c6_UART_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c6_UART_get_check_sum(mxArray *plhs[]);
extern void c6_UART_method_dispatcher(SimStruct *S, int_T method, void *data);

#endif
