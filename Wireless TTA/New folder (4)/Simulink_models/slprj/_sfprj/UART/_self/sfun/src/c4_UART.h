#ifndef __c4_UART_h__
#define __c4_UART_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc4_UARTInstanceStruct
#define typedef_SFc4_UARTInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c4_sfEvent;
  uint8_T c4_tp_S1;
  uint8_T c4_tp_S2;
  boolean_T c4_isStable;
  uint8_T c4_is_active_c4_UART;
  uint8_T c4_is_c4_UART;
  real_T c4_divisor;
  uint8_T c4_doSetSimStateSideEffects;
  const mxArray *c4_setSimStateSideEffectsInfo;
} SFc4_UARTInstanceStruct;

#endif                                 /*typedef_SFc4_UARTInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c4_UART_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c4_UART_get_check_sum(mxArray *plhs[]);
extern void c4_UART_method_dispatcher(SimStruct *S, int_T method, void *data);

#endif
