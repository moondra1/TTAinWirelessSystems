/* Include files */

#include <stddef.h>
#include "blas.h"
#include "UART_sfun.h"
#include "c3_UART.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "UART_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define c3_event_WakeUp                (0)
#define CALL_EVENT                     (-1)
#define c3_IN_NO_ACTIVE_CHILD          ((uint8_T)0U)
#define c3_IN_IDLE_TX                  ((uint8_T)1U)
#define c3_IN_LOAD_TX                  ((uint8_T)2U)
#define c3_IN_SHIFT_TX                 ((uint8_T)3U)
#define c3_IN_STOP_TX                  ((uint8_T)4U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c3_debug_family_names[2] = { "nargin", "nargout" };

static const char * c3_b_debug_family_names[2] = { "nargin", "nargout" };

/* Function Declarations */
static void initialize_c3_UART(SFc3_UARTInstanceStruct *chartInstance);
static void initialize_params_c3_UART(SFc3_UARTInstanceStruct *chartInstance);
static void enable_c3_UART(SFc3_UARTInstanceStruct *chartInstance);
static void disable_c3_UART(SFc3_UARTInstanceStruct *chartInstance);
static void c3_update_debugger_state_c3_UART(SFc3_UARTInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c3_UART(SFc3_UARTInstanceStruct
  *chartInstance);
static void set_sim_state_c3_UART(SFc3_UARTInstanceStruct *chartInstance, const
  mxArray *c3_st);
static void c3_set_sim_state_side_effects_c3_UART(SFc3_UARTInstanceStruct
  *chartInstance);
static void finalize_c3_UART(SFc3_UARTInstanceStruct *chartInstance);
static void sf_gateway_c3_UART(SFc3_UARTInstanceStruct *chartInstance);
static void c3_chartstep_c3_UART(SFc3_UARTInstanceStruct *chartInstance);
static void initSimStructsc3_UART(SFc3_UARTInstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c3_machineNumber, uint32_T
  c3_chartNumber, uint32_T c3_instanceNumber);
static const mxArray *c3_sf_marshallOut(void *chartInstanceVoid, void *c3_inData);
static real_T c3_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance, const
  mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId);
static void c3_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData);
static void c3_make_TX_reg(SFc3_UARTInstanceStruct *chartInstance);
static void c3_make_TX_reg1(SFc3_UARTInstanceStruct *chartInstance);
static const mxArray *c3_b_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData);
static int8_T c3_b_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId);
static void c3_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData);
static const mxArray *c3_c_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData);
static int32_T c3_c_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId);
static void c3_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData);
static const mxArray *c3_d_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData);
static uint8_T c3_d_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance,
  const mxArray *c3_b_tp_IDLE_TX, const char_T *c3_identifier);
static uint8_T c3_e_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId);
static void c3_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData);
static const mxArray *c3_e_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData);
static boolean_T c3_f_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance,
  const mxArray *c3_TX_empty, const char_T *c3_identifier);
static boolean_T c3_g_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId);
static void c3_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData);
static const mxArray *c3_f_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData);
static void c3_h_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance, const
  mxArray *c3_b_TX_reg, const char_T *c3_identifier, uint8_T c3_y[10]);
static void c3_i_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance, const
  mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, uint8_T c3_y[10]);
static void c3_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData);
static const mxArray *c3_g_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData);
static void c3_j_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance, const
  mxArray *c3_b_TX_reg_in, const char_T *c3_identifier, uint8_T c3_y[8]);
static void c3_k_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance, const
  mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, uint8_T c3_y[8]);
static void c3_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData);
static const mxArray *c3_h_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData);
static uint16_T c3_l_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance,
  const mxArray *c3_b_TX_bit_count, const char_T *c3_identifier);
static uint16_T c3_m_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId);
static void c3_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData);
static const mxArray *c3_n_emlrt_marshallIn(SFc3_UARTInstanceStruct
  *chartInstance, const mxArray *c3_b_setSimStateSideEffectsInfo, const char_T
  *c3_identifier);
static const mxArray *c3_o_emlrt_marshallIn(SFc3_UARTInstanceStruct
  *chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId);
static void init_dsm_address_info(SFc3_UARTInstanceStruct *chartInstance);

/* Function Definitions */
static void initialize_c3_UART(SFc3_UARTInstanceStruct *chartInstance)
{
  int32_T c3_i0;
  int32_T c3_i1;
  boolean_T *c3_TX_empty;
  boolean_T *c3_txd;
  c3_txd = (boolean_T *)ssGetOutputPortSignal(chartInstance->S, 2);
  c3_TX_empty = (boolean_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c3_doSetSimStateSideEffects = 0U;
  chartInstance->c3_setSimStateSideEffectsInfo = NULL;
  chartInstance->c3_tp_IDLE_TX = 0U;
  chartInstance->c3_tp_LOAD_TX = 0U;
  chartInstance->c3_tp_SHIFT_TX = 0U;
  chartInstance->c3_tp_STOP_TX = 0U;
  chartInstance->c3_is_active_c3_UART = 0U;
  chartInstance->c3_is_c3_UART = c3_IN_NO_ACTIVE_CHILD;
  for (c3_i0 = 0; c3_i0 < 10; c3_i0++) {
    chartInstance->c3_TX_reg[c3_i0] = 0U;
  }

  for (c3_i1 = 0; c3_i1 < 8; c3_i1++) {
    chartInstance->c3_TX_reg_in[c3_i1] = 0U;
  }

  chartInstance->c3_TX_bit_count = 0U;
  if (!(sf_get_output_port_reusable(chartInstance->S, 1) != 0)) {
    *c3_TX_empty = true;
  }

  if (!(sf_get_output_port_reusable(chartInstance->S, 2) != 0)) {
    *c3_txd = true;
  }
}

static void initialize_params_c3_UART(SFc3_UARTInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c3_UART(SFc3_UARTInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c3_UART(SFc3_UARTInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c3_update_debugger_state_c3_UART(SFc3_UARTInstanceStruct
  *chartInstance)
{
  uint32_T c3_prevAniVal;
  c3_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c3_is_active_c3_UART == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 2U, chartInstance->c3_sfEvent);
  }

  if (chartInstance->c3_is_c3_UART == c3_IN_IDLE_TX) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
  }

  if (chartInstance->c3_is_c3_UART == c3_IN_LOAD_TX) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c3_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c3_sfEvent);
  }

  if (chartInstance->c3_is_c3_UART == c3_IN_SHIFT_TX) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c3_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c3_sfEvent);
  }

  if (chartInstance->c3_is_c3_UART == c3_IN_STOP_TX) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c3_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c3_sfEvent);
  }

  _SFD_SET_ANIMATION(c3_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static const mxArray *get_sim_state_c3_UART(SFc3_UARTInstanceStruct
  *chartInstance)
{
  const mxArray *c3_st;
  const mxArray *c3_y = NULL;
  boolean_T c3_hoistedGlobal;
  boolean_T c3_u;
  const mxArray *c3_b_y = NULL;
  boolean_T c3_b_hoistedGlobal;
  boolean_T c3_b_u;
  const mxArray *c3_c_y = NULL;
  uint16_T c3_c_hoistedGlobal;
  uint16_T c3_c_u;
  const mxArray *c3_d_y = NULL;
  int32_T c3_i2;
  uint8_T c3_d_u[10];
  const mxArray *c3_e_y = NULL;
  int32_T c3_i3;
  uint8_T c3_e_u[8];
  const mxArray *c3_f_y = NULL;
  uint8_T c3_d_hoistedGlobal;
  uint8_T c3_f_u;
  const mxArray *c3_g_y = NULL;
  uint8_T c3_e_hoistedGlobal;
  uint8_T c3_g_u;
  const mxArray *c3_h_y = NULL;
  boolean_T *c3_TX_empty;
  boolean_T *c3_txd;
  c3_txd = (boolean_T *)ssGetOutputPortSignal(chartInstance->S, 2);
  c3_TX_empty = (boolean_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c3_st = NULL;
  c3_st = NULL;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_createcellmatrix(7, 1), false);
  c3_hoistedGlobal = *c3_TX_empty;
  c3_u = c3_hoistedGlobal;
  c3_b_y = NULL;
  sf_mex_assign(&c3_b_y, sf_mex_create("y", &c3_u, 11, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c3_y, 0, c3_b_y);
  c3_b_hoistedGlobal = *c3_txd;
  c3_b_u = c3_b_hoistedGlobal;
  c3_c_y = NULL;
  sf_mex_assign(&c3_c_y, sf_mex_create("y", &c3_b_u, 11, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c3_y, 1, c3_c_y);
  c3_c_hoistedGlobal = chartInstance->c3_TX_bit_count;
  c3_c_u = c3_c_hoistedGlobal;
  c3_d_y = NULL;
  sf_mex_assign(&c3_d_y, sf_mex_create("y", &c3_c_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c3_y, 2, c3_d_y);
  for (c3_i2 = 0; c3_i2 < 10; c3_i2++) {
    c3_d_u[c3_i2] = chartInstance->c3_TX_reg[c3_i2];
  }

  c3_e_y = NULL;
  sf_mex_assign(&c3_e_y, sf_mex_create("y", c3_d_u, 3, 0U, 1U, 0U, 1, 10), false);
  sf_mex_setcell(c3_y, 3, c3_e_y);
  for (c3_i3 = 0; c3_i3 < 8; c3_i3++) {
    c3_e_u[c3_i3] = chartInstance->c3_TX_reg_in[c3_i3];
  }

  c3_f_y = NULL;
  sf_mex_assign(&c3_f_y, sf_mex_create("y", c3_e_u, 3, 0U, 1U, 0U, 1, 8), false);
  sf_mex_setcell(c3_y, 4, c3_f_y);
  c3_d_hoistedGlobal = chartInstance->c3_is_active_c3_UART;
  c3_f_u = c3_d_hoistedGlobal;
  c3_g_y = NULL;
  sf_mex_assign(&c3_g_y, sf_mex_create("y", &c3_f_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c3_y, 5, c3_g_y);
  c3_e_hoistedGlobal = chartInstance->c3_is_c3_UART;
  c3_g_u = c3_e_hoistedGlobal;
  c3_h_y = NULL;
  sf_mex_assign(&c3_h_y, sf_mex_create("y", &c3_g_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c3_y, 6, c3_h_y);
  sf_mex_assign(&c3_st, c3_y, false);
  return c3_st;
}

static void set_sim_state_c3_UART(SFc3_UARTInstanceStruct *chartInstance, const
  mxArray *c3_st)
{
  const mxArray *c3_u;
  uint8_T c3_uv0[10];
  int32_T c3_i4;
  uint8_T c3_uv1[8];
  int32_T c3_i5;
  boolean_T *c3_TX_empty;
  boolean_T *c3_txd;
  c3_txd = (boolean_T *)ssGetOutputPortSignal(chartInstance->S, 2);
  c3_TX_empty = (boolean_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c3_u = sf_mex_dup(c3_st);
  *c3_TX_empty = c3_f_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c3_u, 0)), "TX_empty");
  *c3_txd = c3_f_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c3_u,
    1)), "txd");
  chartInstance->c3_TX_bit_count = c3_l_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c3_u, 2)), "TX_bit_count");
  c3_h_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c3_u, 3)),
                        "TX_reg", c3_uv0);
  for (c3_i4 = 0; c3_i4 < 10; c3_i4++) {
    chartInstance->c3_TX_reg[c3_i4] = c3_uv0[c3_i4];
  }

  c3_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c3_u, 4)),
                        "TX_reg_in", c3_uv1);
  for (c3_i5 = 0; c3_i5 < 8; c3_i5++) {
    chartInstance->c3_TX_reg_in[c3_i5] = c3_uv1[c3_i5];
  }

  chartInstance->c3_is_active_c3_UART = c3_d_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c3_u, 5)), "is_active_c3_UART");
  chartInstance->c3_is_c3_UART = c3_d_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c3_u, 6)), "is_c3_UART");
  sf_mex_assign(&chartInstance->c3_setSimStateSideEffectsInfo,
                c3_n_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c3_u, 7)), "setSimStateSideEffectsInfo"), true);
  sf_mex_destroy(&c3_u);
  chartInstance->c3_doSetSimStateSideEffects = 1U;
  c3_update_debugger_state_c3_UART(chartInstance);
  sf_mex_destroy(&c3_st);
}

static void c3_set_sim_state_side_effects_c3_UART(SFc3_UARTInstanceStruct
  *chartInstance)
{
  if (chartInstance->c3_doSetSimStateSideEffects != 0) {
    if (chartInstance->c3_is_c3_UART == c3_IN_IDLE_TX) {
      chartInstance->c3_tp_IDLE_TX = 1U;
    } else {
      chartInstance->c3_tp_IDLE_TX = 0U;
    }

    if (chartInstance->c3_is_c3_UART == c3_IN_LOAD_TX) {
      chartInstance->c3_tp_LOAD_TX = 1U;
    } else {
      chartInstance->c3_tp_LOAD_TX = 0U;
    }

    if (chartInstance->c3_is_c3_UART == c3_IN_SHIFT_TX) {
      chartInstance->c3_tp_SHIFT_TX = 1U;
    } else {
      chartInstance->c3_tp_SHIFT_TX = 0U;
    }

    if (chartInstance->c3_is_c3_UART == c3_IN_STOP_TX) {
      chartInstance->c3_tp_STOP_TX = 1U;
    } else {
      chartInstance->c3_tp_STOP_TX = 0U;
    }

    chartInstance->c3_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c3_UART(SFc3_UARTInstanceStruct *chartInstance)
{
  sf_mex_destroy(&chartInstance->c3_setSimStateSideEffectsInfo);
}

static void sf_gateway_c3_UART(SFc3_UARTInstanceStruct *chartInstance)
{
  int32_T c3_i6;
  int32_T c3_i7;
  int32_T c3_i8;
  boolean_T *c3_TX_empty;
  boolean_T *c3_TX_data_wr;
  boolean_T *c3_txd;
  boolean_T *c3_TX_clk_signal;
  uint8_T (*c3_TX_data)[8];
  c3_TX_clk_signal = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 2);
  c3_TX_data = (uint8_T (*)[8])ssGetInputPortSignal(chartInstance->S, 1);
  c3_txd = (boolean_T *)ssGetOutputPortSignal(chartInstance->S, 2);
  c3_TX_data_wr = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 0);
  c3_TX_empty = (boolean_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c3_set_sim_state_side_effects_c3_UART(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 2U, chartInstance->c3_sfEvent);
  _SFD_DATA_RANGE_CHECK((real_T)*c3_TX_empty, 0U);
  _SFD_DATA_RANGE_CHECK((real_T)*c3_TX_data_wr, 1U);
  _SFD_DATA_RANGE_CHECK((real_T)*c3_txd, 2U);
  for (c3_i6 = 0; c3_i6 < 10; c3_i6++) {
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c3_TX_reg[c3_i6], 3U);
  }

  for (c3_i7 = 0; c3_i7 < 8; c3_i7++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*c3_TX_data)[c3_i7], 4U);
  }

  for (c3_i8 = 0; c3_i8 < 8; c3_i8++) {
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c3_TX_reg_in[c3_i8], 5U);
  }

  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c3_TX_bit_count, 6U);
  _SFD_DATA_RANGE_CHECK((real_T)*c3_TX_clk_signal, 7U);
  chartInstance->c3_sfEvent = c3_event_WakeUp;
  _SFD_CE_CALL(EVENT_BEFORE_BROADCAST_TAG, c3_event_WakeUp,
               chartInstance->c3_sfEvent);
  c3_chartstep_c3_UART(chartInstance);
  _SFD_CE_CALL(EVENT_AFTER_BROADCAST_TAG, c3_event_WakeUp,
               chartInstance->c3_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_UARTMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void c3_chartstep_c3_UART(SFc3_UARTInstanceStruct *chartInstance)
{
  boolean_T c3_out;
  int32_T c3_i9;
  int32_T c3_i10;
  boolean_T c3_b_out;
  boolean_T c3_c_out;
  boolean_T c3_d_out;
  int32_T c3_i11;
  boolean_T c3_e_out;
  int32_T c3_i12;
  boolean_T c3_f_out;
  boolean_T *c3_TX_data_wr;
  boolean_T *c3_TX_empty;
  boolean_T *c3_TX_clk_signal;
  uint8_T (*c3_TX_data)[8];
  boolean_T guard1 = false;
  c3_TX_clk_signal = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 2);
  c3_TX_data = (uint8_T (*)[8])ssGetInputPortSignal(chartInstance->S, 1);
  c3_TX_data_wr = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 0);
  c3_TX_empty = (boolean_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 2U, chartInstance->c3_sfEvent);
  if (chartInstance->c3_is_active_c3_UART == 0U) {
    _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 2U, chartInstance->c3_sfEvent);
    chartInstance->c3_is_active_c3_UART = 1U;
    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c3_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
    chartInstance->c3_is_c3_UART = c3_IN_IDLE_TX;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
    chartInstance->c3_tp_IDLE_TX = 1U;
  } else {
    switch (chartInstance->c3_is_c3_UART) {
     case c3_IN_IDLE_TX:
      CV_CHART_EVAL(2, 0, 1);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 1U,
                   chartInstance->c3_sfEvent);
      c3_out = (CV_TRANSITION_EVAL(1U, (int32_T)_SFD_CCP_CALL(1U, 0, (int32_T)
                  *c3_TX_data_wr == 1 != 0U, chartInstance->c3_sfEvent)) != 0);
      if (c3_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, chartInstance->c3_sfEvent);
        chartInstance->c3_tp_IDLE_TX = 0U;
        chartInstance->c3_is_c3_UART = c3_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_TRANS_ACTION_TAG, 1U,
                     chartInstance->c3_sfEvent);
        for (c3_i9 = 0; c3_i9 < 8; c3_i9++) {
          chartInstance->c3_TX_reg_in[c3_i9] = (*c3_TX_data)[c3_i9];
        }

        for (c3_i10 = 0; c3_i10 < 8; c3_i10++) {
          _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c3_TX_reg_in[c3_i10], 5U);
        }

        chartInstance->c3_is_c3_UART = c3_IN_LOAD_TX;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c3_sfEvent);
        chartInstance->c3_tp_LOAD_TX = 1U;
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U,
                     chartInstance->c3_sfEvent);
        *c3_TX_empty = true;
        _SFD_DATA_RANGE_CHECK((real_T)*c3_TX_empty, 0U);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c3_sfEvent);
      break;

     case c3_IN_LOAD_TX:
      CV_CHART_EVAL(2, 0, 2);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 2U,
                   chartInstance->c3_sfEvent);
      c3_b_out = (CV_TRANSITION_EVAL(2U, (int32_T)_SFD_CCP_CALL(2U, 0, (int32_T)*
        c3_TX_clk_signal == 1 != 0U, chartInstance->c3_sfEvent)) != 0);
      if (c3_b_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c3_sfEvent);
        chartInstance->c3_tp_LOAD_TX = 0U;
        chartInstance->c3_is_c3_UART = c3_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c3_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_TRANS_ACTION_TAG, 2U,
                     chartInstance->c3_sfEvent);
        chartInstance->c3_TX_bit_count = 9U;
        _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c3_TX_bit_count, 6U);
        c3_make_TX_reg(chartInstance);
        chartInstance->c3_is_c3_UART = c3_IN_SHIFT_TX;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c3_sfEvent);
        chartInstance->c3_tp_SHIFT_TX = 1U;
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U,
                     chartInstance->c3_sfEvent);
        *c3_TX_empty = false;
        _SFD_DATA_RANGE_CHECK((real_T)*c3_TX_empty, 0U);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c3_sfEvent);
      break;

     case c3_IN_SHIFT_TX:
      CV_CHART_EVAL(2, 0, 3);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 5U,
                   chartInstance->c3_sfEvent);
      c3_c_out = (CV_TRANSITION_EVAL(5U, (int32_T)_SFD_CCP_CALL(5U, 0, (int32_T)*
        c3_TX_clk_signal == 1 != 0U, chartInstance->c3_sfEvent)) != 0);
      guard1 = false;
      if (c3_c_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 5U, chartInstance->c3_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 3U,
                     chartInstance->c3_sfEvent);
        c3_d_out = (CV_TRANSITION_EVAL(3U, (int32_T)_SFD_CCP_CALL(3U, 0,
          chartInstance->c3_TX_bit_count == 1 != 0U, chartInstance->c3_sfEvent))
                    != 0);
        if (c3_d_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 3U, chartInstance->c3_sfEvent);
          chartInstance->c3_tp_SHIFT_TX = 0U;
          chartInstance->c3_is_c3_UART = c3_IN_NO_ACTIVE_CHILD;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c3_sfEvent);
          _SFD_CT_CALL(TRANSITION_BEFORE_TRANS_ACTION_TAG, 5U,
                       chartInstance->c3_sfEvent);
          c3_make_TX_reg1(chartInstance);
          c3_i11 = chartInstance->c3_TX_bit_count - 1;
          if (CV_SATURATION_EVAL(5, 5, 0, 0, c3_i11 < 0)) {
            c3_i11 = 0;
          } else {
            if (c3_i11 > 65535) {
              c3_i11 = 65535;
            }
          }

          chartInstance->c3_TX_bit_count = (uint16_T)c3_i11;
          _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c3_TX_bit_count, 6U);
          chartInstance->c3_is_c3_UART = c3_IN_STOP_TX;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c3_sfEvent);
          chartInstance->c3_tp_STOP_TX = 1U;
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 6U,
                       chartInstance->c3_sfEvent);
          c3_e_out = (CV_TRANSITION_EVAL(6U, (int32_T)_SFD_CCP_CALL(6U, 0,
            chartInstance->c3_TX_bit_count > 1 != 0U, chartInstance->c3_sfEvent))
                      != 0);
          if (c3_e_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 6U, chartInstance->c3_sfEvent);
            chartInstance->c3_tp_SHIFT_TX = 0U;
            chartInstance->c3_is_c3_UART = c3_IN_NO_ACTIVE_CHILD;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c3_sfEvent);
            _SFD_CT_CALL(TRANSITION_BEFORE_TRANS_ACTION_TAG, 5U,
                         chartInstance->c3_sfEvent);
            c3_make_TX_reg1(chartInstance);
            c3_i12 = chartInstance->c3_TX_bit_count - 1;
            if (CV_SATURATION_EVAL(5, 5, 0, 0, c3_i12 < 0)) {
              c3_i12 = 0;
            } else {
              if (c3_i12 > 65535) {
                c3_i12 = 65535;
              }
            }

            chartInstance->c3_TX_bit_count = (uint16_T)c3_i12;
            _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c3_TX_bit_count, 6U);
            chartInstance->c3_is_c3_UART = c3_IN_SHIFT_TX;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c3_sfEvent);
            chartInstance->c3_tp_SHIFT_TX = 1U;
          } else {
            guard1 = true;
          }
        }
      } else {
        guard1 = true;
      }

      if (guard1 == true) {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U,
                     chartInstance->c3_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c3_sfEvent);
      break;

     case c3_IN_STOP_TX:
      CV_CHART_EVAL(2, 0, 4);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 4U,
                   chartInstance->c3_sfEvent);
      c3_f_out = (CV_TRANSITION_EVAL(4U, (int32_T)_SFD_CCP_CALL(4U, 0, (int32_T)*
        c3_TX_clk_signal == 1 != 0U, chartInstance->c3_sfEvent)) != 0);
      if (c3_f_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 4U, chartInstance->c3_sfEvent);
        chartInstance->c3_tp_STOP_TX = 0U;
        chartInstance->c3_is_c3_UART = c3_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c3_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_TRANS_ACTION_TAG, 4U,
                     chartInstance->c3_sfEvent);
        *c3_TX_empty = true;
        _SFD_DATA_RANGE_CHECK((real_T)*c3_TX_empty, 0U);
        chartInstance->c3_is_c3_UART = c3_IN_IDLE_TX;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
        chartInstance->c3_tp_IDLE_TX = 1U;
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U,
                     chartInstance->c3_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c3_sfEvent);
      break;

     default:
      CV_CHART_EVAL(2, 0, 0);
      chartInstance->c3_is_c3_UART = c3_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
      break;
    }
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c3_sfEvent);
}

static void initSimStructsc3_UART(SFc3_UARTInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c3_machineNumber, uint32_T
  c3_chartNumber, uint32_T c3_instanceNumber)
{
  (void)c3_machineNumber;
  (void)c3_chartNumber;
  (void)c3_instanceNumber;
}

static const mxArray *c3_sf_marshallOut(void *chartInstanceVoid, void *c3_inData)
{
  const mxArray *c3_mxArrayOutData = NULL;
  real_T c3_u;
  const mxArray *c3_y = NULL;
  SFc3_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc3_UARTInstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  c3_u = *(real_T *)c3_inData;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", &c3_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c3_mxArrayOutData, c3_y, false);
  return c3_mxArrayOutData;
}

static real_T c3_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance, const
  mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId)
{
  real_T c3_y;
  real_T c3_d0;
  (void)chartInstance;
  sf_mex_import(c3_parentId, sf_mex_dup(c3_u), &c3_d0, 1, 0, 0U, 0, 0U, 0);
  c3_y = c3_d0;
  sf_mex_destroy(&c3_u);
  return c3_y;
}

static void c3_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData)
{
  const mxArray *c3_nargout;
  const char_T *c3_identifier;
  emlrtMsgIdentifier c3_thisId;
  real_T c3_y;
  SFc3_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc3_UARTInstanceStruct *)chartInstanceVoid;
  c3_nargout = sf_mex_dup(c3_mxArrayInData);
  c3_identifier = c3_varName;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_y = c3_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_nargout), &c3_thisId);
  sf_mex_destroy(&c3_nargout);
  *(real_T *)c3_outData = c3_y;
  sf_mex_destroy(&c3_mxArrayInData);
}

const mxArray *sf_c3_UART_get_eml_resolved_functions_info(void)
{
  const mxArray *c3_nameCaptureInfo = NULL;
  c3_nameCaptureInfo = NULL;
  sf_mex_assign(&c3_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c3_nameCaptureInfo;
}

static void c3_make_TX_reg(SFc3_UARTInstanceStruct *chartInstance)
{
  uint32_T c3_debug_family_var_map[2];
  real_T c3_nargin = 0.0;
  real_T c3_nargout = 0.0;
  int32_T c3_i13;
  boolean_T *c3_txd;
  c3_txd = (boolean_T *)ssGetOutputPortSignal(chartInstance->S, 2);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_debug_family_names,
    c3_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_nargin, 0U, c3_sf_marshallOut,
    c3_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_nargout, 1U, c3_sf_marshallOut,
    c3_sf_marshallIn);
  CV_EML_FCN(4, 0);
  _SFD_EML_CALL(4U, chartInstance->c3_sfEvent, 2);
  chartInstance->c3_TX_reg[0] = 1U;
  for (c3_i13 = 0; c3_i13 < 8; c3_i13++) {
    chartInstance->c3_TX_reg[c3_i13 + 1] = chartInstance->c3_TX_reg_in[7 -
      c3_i13];
  }

  chartInstance->c3_TX_reg[9] = 0U;
  _SFD_EML_CALL(4U, chartInstance->c3_sfEvent, 3);
  *c3_txd = (chartInstance->c3_TX_reg[9] != 0);
  _SFD_EML_CALL(4U, chartInstance->c3_sfEvent, -3);
  _SFD_SYMBOL_SCOPE_POP();
}

static void c3_make_TX_reg1(SFc3_UARTInstanceStruct *chartInstance)
{
  uint32_T c3_debug_family_var_map[2];
  real_T c3_nargin = 0.0;
  real_T c3_nargout = 0.0;
  uint8_T c3_uv2[10];
  int32_T c3_i14;
  int32_T c3_i15;
  boolean_T *c3_txd;
  c3_txd = (boolean_T *)ssGetOutputPortSignal(chartInstance->S, 2);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_b_debug_family_names,
    c3_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_nargin, 0U, c3_sf_marshallOut,
    c3_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_nargout, 1U, c3_sf_marshallOut,
    c3_sf_marshallIn);
  CV_EML_FCN(5, 0);
  _SFD_EML_CALL(5U, chartInstance->c3_sfEvent, 2);
  c3_uv2[0] = 1U;
  for (c3_i14 = 0; c3_i14 < 9; c3_i14++) {
    c3_uv2[c3_i14 + 1] = chartInstance->c3_TX_reg[c3_i14];
  }

  for (c3_i15 = 0; c3_i15 < 10; c3_i15++) {
    chartInstance->c3_TX_reg[c3_i15] = c3_uv2[c3_i15];
  }

  _SFD_EML_CALL(5U, chartInstance->c3_sfEvent, 3);
  *c3_txd = (chartInstance->c3_TX_reg[9] != 0);
  _SFD_EML_CALL(5U, chartInstance->c3_sfEvent, -3);
  _SFD_SYMBOL_SCOPE_POP();
}

static const mxArray *c3_b_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData)
{
  const mxArray *c3_mxArrayOutData = NULL;
  int8_T c3_u;
  const mxArray *c3_y = NULL;
  SFc3_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc3_UARTInstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  c3_u = *(int8_T *)c3_inData;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", &c3_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c3_mxArrayOutData, c3_y, false);
  return c3_mxArrayOutData;
}

static int8_T c3_b_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId)
{
  int8_T c3_y;
  int8_T c3_i16;
  (void)chartInstance;
  sf_mex_import(c3_parentId, sf_mex_dup(c3_u), &c3_i16, 1, 2, 0U, 0, 0U, 0);
  c3_y = c3_i16;
  sf_mex_destroy(&c3_u);
  return c3_y;
}

static void c3_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData)
{
  const mxArray *c3_WakeUp;
  const char_T *c3_identifier;
  emlrtMsgIdentifier c3_thisId;
  int8_T c3_y;
  SFc3_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc3_UARTInstanceStruct *)chartInstanceVoid;
  c3_WakeUp = sf_mex_dup(c3_mxArrayInData);
  c3_identifier = c3_varName;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_y = c3_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_WakeUp), &c3_thisId);
  sf_mex_destroy(&c3_WakeUp);
  *(int8_T *)c3_outData = c3_y;
  sf_mex_destroy(&c3_mxArrayInData);
}

static const mxArray *c3_c_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData)
{
  const mxArray *c3_mxArrayOutData = NULL;
  int32_T c3_u;
  const mxArray *c3_y = NULL;
  SFc3_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc3_UARTInstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  c3_u = *(int32_T *)c3_inData;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", &c3_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c3_mxArrayOutData, c3_y, false);
  return c3_mxArrayOutData;
}

static int32_T c3_c_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId)
{
  int32_T c3_y;
  int32_T c3_i17;
  (void)chartInstance;
  sf_mex_import(c3_parentId, sf_mex_dup(c3_u), &c3_i17, 1, 6, 0U, 0, 0U, 0);
  c3_y = c3_i17;
  sf_mex_destroy(&c3_u);
  return c3_y;
}

static void c3_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData)
{
  const mxArray *c3_b_sfEvent;
  const char_T *c3_identifier;
  emlrtMsgIdentifier c3_thisId;
  int32_T c3_y;
  SFc3_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc3_UARTInstanceStruct *)chartInstanceVoid;
  c3_b_sfEvent = sf_mex_dup(c3_mxArrayInData);
  c3_identifier = c3_varName;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_y = c3_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_b_sfEvent),
    &c3_thisId);
  sf_mex_destroy(&c3_b_sfEvent);
  *(int32_T *)c3_outData = c3_y;
  sf_mex_destroy(&c3_mxArrayInData);
}

static const mxArray *c3_d_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData)
{
  const mxArray *c3_mxArrayOutData = NULL;
  uint8_T c3_u;
  const mxArray *c3_y = NULL;
  SFc3_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc3_UARTInstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  c3_u = *(uint8_T *)c3_inData;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", &c3_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c3_mxArrayOutData, c3_y, false);
  return c3_mxArrayOutData;
}

static uint8_T c3_d_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance,
  const mxArray *c3_b_tp_IDLE_TX, const char_T *c3_identifier)
{
  uint8_T c3_y;
  emlrtMsgIdentifier c3_thisId;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_y = c3_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_b_tp_IDLE_TX),
    &c3_thisId);
  sf_mex_destroy(&c3_b_tp_IDLE_TX);
  return c3_y;
}

static uint8_T c3_e_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId)
{
  uint8_T c3_y;
  uint8_T c3_u0;
  (void)chartInstance;
  sf_mex_import(c3_parentId, sf_mex_dup(c3_u), &c3_u0, 1, 3, 0U, 0, 0U, 0);
  c3_y = c3_u0;
  sf_mex_destroy(&c3_u);
  return c3_y;
}

static void c3_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData)
{
  const mxArray *c3_b_tp_IDLE_TX;
  const char_T *c3_identifier;
  emlrtMsgIdentifier c3_thisId;
  uint8_T c3_y;
  SFc3_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc3_UARTInstanceStruct *)chartInstanceVoid;
  c3_b_tp_IDLE_TX = sf_mex_dup(c3_mxArrayInData);
  c3_identifier = c3_varName;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_y = c3_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_b_tp_IDLE_TX),
    &c3_thisId);
  sf_mex_destroy(&c3_b_tp_IDLE_TX);
  *(uint8_T *)c3_outData = c3_y;
  sf_mex_destroy(&c3_mxArrayInData);
}

static const mxArray *c3_e_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData)
{
  const mxArray *c3_mxArrayOutData = NULL;
  boolean_T c3_u;
  const mxArray *c3_y = NULL;
  SFc3_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc3_UARTInstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  c3_u = *(boolean_T *)c3_inData;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", &c3_u, 11, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c3_mxArrayOutData, c3_y, false);
  return c3_mxArrayOutData;
}

static boolean_T c3_f_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance,
  const mxArray *c3_TX_empty, const char_T *c3_identifier)
{
  boolean_T c3_y;
  emlrtMsgIdentifier c3_thisId;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_y = c3_g_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_TX_empty),
    &c3_thisId);
  sf_mex_destroy(&c3_TX_empty);
  return c3_y;
}

static boolean_T c3_g_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId)
{
  boolean_T c3_y;
  boolean_T c3_b0;
  (void)chartInstance;
  sf_mex_import(c3_parentId, sf_mex_dup(c3_u), &c3_b0, 1, 11, 0U, 0, 0U, 0);
  c3_y = c3_b0;
  sf_mex_destroy(&c3_u);
  return c3_y;
}

static void c3_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData)
{
  const mxArray *c3_TX_empty;
  const char_T *c3_identifier;
  emlrtMsgIdentifier c3_thisId;
  boolean_T c3_y;
  SFc3_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc3_UARTInstanceStruct *)chartInstanceVoid;
  c3_TX_empty = sf_mex_dup(c3_mxArrayInData);
  c3_identifier = c3_varName;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_y = c3_g_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_TX_empty),
    &c3_thisId);
  sf_mex_destroy(&c3_TX_empty);
  *(boolean_T *)c3_outData = c3_y;
  sf_mex_destroy(&c3_mxArrayInData);
}

static const mxArray *c3_f_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData)
{
  const mxArray *c3_mxArrayOutData = NULL;
  int32_T c3_i18;
  uint8_T c3_b_inData[10];
  int32_T c3_i19;
  uint8_T c3_u[10];
  const mxArray *c3_y = NULL;
  SFc3_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc3_UARTInstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  for (c3_i18 = 0; c3_i18 < 10; c3_i18++) {
    c3_b_inData[c3_i18] = (*(uint8_T (*)[10])c3_inData)[c3_i18];
  }

  for (c3_i19 = 0; c3_i19 < 10; c3_i19++) {
    c3_u[c3_i19] = c3_b_inData[c3_i19];
  }

  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", c3_u, 3, 0U, 1U, 0U, 1, 10), false);
  sf_mex_assign(&c3_mxArrayOutData, c3_y, false);
  return c3_mxArrayOutData;
}

static void c3_h_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance, const
  mxArray *c3_b_TX_reg, const char_T *c3_identifier, uint8_T c3_y[10])
{
  emlrtMsgIdentifier c3_thisId;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_b_TX_reg), &c3_thisId, c3_y);
  sf_mex_destroy(&c3_b_TX_reg);
}

static void c3_i_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance, const
  mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, uint8_T c3_y[10])
{
  uint8_T c3_uv3[10];
  int32_T c3_i20;
  (void)chartInstance;
  sf_mex_import(c3_parentId, sf_mex_dup(c3_u), c3_uv3, 1, 3, 0U, 1, 0U, 1, 10);
  for (c3_i20 = 0; c3_i20 < 10; c3_i20++) {
    c3_y[c3_i20] = c3_uv3[c3_i20];
  }

  sf_mex_destroy(&c3_u);
}

static void c3_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData)
{
  const mxArray *c3_b_TX_reg;
  const char_T *c3_identifier;
  emlrtMsgIdentifier c3_thisId;
  uint8_T c3_y[10];
  int32_T c3_i21;
  SFc3_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc3_UARTInstanceStruct *)chartInstanceVoid;
  c3_b_TX_reg = sf_mex_dup(c3_mxArrayInData);
  c3_identifier = c3_varName;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_b_TX_reg), &c3_thisId, c3_y);
  sf_mex_destroy(&c3_b_TX_reg);
  for (c3_i21 = 0; c3_i21 < 10; c3_i21++) {
    (*(uint8_T (*)[10])c3_outData)[c3_i21] = c3_y[c3_i21];
  }

  sf_mex_destroy(&c3_mxArrayInData);
}

static const mxArray *c3_g_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData)
{
  const mxArray *c3_mxArrayOutData = NULL;
  int32_T c3_i22;
  uint8_T c3_b_inData[8];
  int32_T c3_i23;
  uint8_T c3_u[8];
  const mxArray *c3_y = NULL;
  SFc3_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc3_UARTInstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  for (c3_i22 = 0; c3_i22 < 8; c3_i22++) {
    c3_b_inData[c3_i22] = (*(uint8_T (*)[8])c3_inData)[c3_i22];
  }

  for (c3_i23 = 0; c3_i23 < 8; c3_i23++) {
    c3_u[c3_i23] = c3_b_inData[c3_i23];
  }

  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", c3_u, 3, 0U, 1U, 0U, 1, 8), false);
  sf_mex_assign(&c3_mxArrayOutData, c3_y, false);
  return c3_mxArrayOutData;
}

static void c3_j_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance, const
  mxArray *c3_b_TX_reg_in, const char_T *c3_identifier, uint8_T c3_y[8])
{
  emlrtMsgIdentifier c3_thisId;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_b_TX_reg_in), &c3_thisId,
                        c3_y);
  sf_mex_destroy(&c3_b_TX_reg_in);
}

static void c3_k_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance, const
  mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId, uint8_T c3_y[8])
{
  uint8_T c3_uv4[8];
  int32_T c3_i24;
  (void)chartInstance;
  sf_mex_import(c3_parentId, sf_mex_dup(c3_u), c3_uv4, 1, 3, 0U, 1, 0U, 1, 8);
  for (c3_i24 = 0; c3_i24 < 8; c3_i24++) {
    c3_y[c3_i24] = c3_uv4[c3_i24];
  }

  sf_mex_destroy(&c3_u);
}

static void c3_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData)
{
  const mxArray *c3_b_TX_reg_in;
  const char_T *c3_identifier;
  emlrtMsgIdentifier c3_thisId;
  uint8_T c3_y[8];
  int32_T c3_i25;
  SFc3_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc3_UARTInstanceStruct *)chartInstanceVoid;
  c3_b_TX_reg_in = sf_mex_dup(c3_mxArrayInData);
  c3_identifier = c3_varName;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_b_TX_reg_in), &c3_thisId,
                        c3_y);
  sf_mex_destroy(&c3_b_TX_reg_in);
  for (c3_i25 = 0; c3_i25 < 8; c3_i25++) {
    (*(uint8_T (*)[8])c3_outData)[c3_i25] = c3_y[c3_i25];
  }

  sf_mex_destroy(&c3_mxArrayInData);
}

static const mxArray *c3_h_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData)
{
  const mxArray *c3_mxArrayOutData = NULL;
  uint16_T c3_u;
  const mxArray *c3_y = NULL;
  SFc3_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc3_UARTInstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  c3_u = *(uint16_T *)c3_inData;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", &c3_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c3_mxArrayOutData, c3_y, false);
  return c3_mxArrayOutData;
}

static uint16_T c3_l_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance,
  const mxArray *c3_b_TX_bit_count, const char_T *c3_identifier)
{
  uint16_T c3_y;
  emlrtMsgIdentifier c3_thisId;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_y = c3_m_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_b_TX_bit_count),
    &c3_thisId);
  sf_mex_destroy(&c3_b_TX_bit_count);
  return c3_y;
}

static uint16_T c3_m_emlrt_marshallIn(SFc3_UARTInstanceStruct *chartInstance,
  const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId)
{
  uint16_T c3_y;
  uint16_T c3_u1;
  (void)chartInstance;
  sf_mex_import(c3_parentId, sf_mex_dup(c3_u), &c3_u1, 1, 5, 0U, 0, 0U, 0);
  c3_y = c3_u1;
  sf_mex_destroy(&c3_u);
  return c3_y;
}

static void c3_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData)
{
  const mxArray *c3_b_TX_bit_count;
  const char_T *c3_identifier;
  emlrtMsgIdentifier c3_thisId;
  uint16_T c3_y;
  SFc3_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc3_UARTInstanceStruct *)chartInstanceVoid;
  c3_b_TX_bit_count = sf_mex_dup(c3_mxArrayInData);
  c3_identifier = c3_varName;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_y = c3_m_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_b_TX_bit_count),
    &c3_thisId);
  sf_mex_destroy(&c3_b_TX_bit_count);
  *(uint16_T *)c3_outData = c3_y;
  sf_mex_destroy(&c3_mxArrayInData);
}

static const mxArray *c3_n_emlrt_marshallIn(SFc3_UARTInstanceStruct
  *chartInstance, const mxArray *c3_b_setSimStateSideEffectsInfo, const char_T
  *c3_identifier)
{
  const mxArray *c3_y = NULL;
  emlrtMsgIdentifier c3_thisId;
  c3_y = NULL;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  sf_mex_assign(&c3_y, c3_o_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c3_b_setSimStateSideEffectsInfo), &c3_thisId), false);
  sf_mex_destroy(&c3_b_setSimStateSideEffectsInfo);
  return c3_y;
}

static const mxArray *c3_o_emlrt_marshallIn(SFc3_UARTInstanceStruct
  *chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId)
{
  const mxArray *c3_y = NULL;
  (void)chartInstance;
  (void)c3_parentId;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_duplicatearraysafe(&c3_u), false);
  sf_mex_destroy(&c3_u);
  return c3_y;
}

static void init_dsm_address_info(SFc3_UARTInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c3_UART_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(833099658U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2056388869U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(413316792U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2728836482U);
}

mxArray *sf_c3_UART_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1,1,5,
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("eFj9QLhdSSjafKCakXhQD");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,3,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(8);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c3_UART_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c3_UART_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c3_UART(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x7'type','srcId','name','auxInfo'{{M[1],M[2],T\"TX_empty\",},{M[1],M[13],T\"txd\",},{M[3],M[17],T\"TX_bit_count\",},{M[3],M[14],T\"TX_reg\",},{M[3],M[16],T\"TX_reg_in\",},{M[8],M[0],T\"is_active_c3_UART\",},{M[9],M[0],T\"is_c3_UART\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 7, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c3_UART_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc3_UARTInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc3_UARTInstanceStruct *) chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _UARTMachineNumber_,
           3,
           6,
           7,
           0,
           8,
           1,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize ist own list of scripts */
        init_script_number_translation(_UARTMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_UARTMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _UARTMachineNumber_,
            chartInstance->chartNumber,
            1,
            1,
            1);
          _SFD_SET_DATA_PROPS(0,2,0,1,"TX_empty");
          _SFD_SET_DATA_PROPS(1,1,1,0,"TX_data_wr");
          _SFD_SET_DATA_PROPS(2,2,0,1,"txd");
          _SFD_SET_DATA_PROPS(3,0,0,0,"TX_reg");
          _SFD_SET_DATA_PROPS(4,1,1,0,"TX_data");
          _SFD_SET_DATA_PROPS(5,0,0,0,"TX_reg_in");
          _SFD_SET_DATA_PROPS(6,0,0,0,"TX_bit_count");
          _SFD_SET_DATA_PROPS(7,1,1,0,"TX_clk_signal");
          _SFD_EVENT_SCOPE(0,1);
          _SFD_STATE_INFO(0,0,0);
          _SFD_STATE_INFO(1,0,0);
          _SFD_STATE_INFO(2,0,0);
          _SFD_STATE_INFO(3,0,0);
          _SFD_STATE_INFO(4,0,2);
          _SFD_STATE_INFO(5,0,2);
          _SFD_CH_SUBSTATE_COUNT(4);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_CH_SUBSTATE_INDEX(1,1);
          _SFD_CH_SUBSTATE_INDEX(2,2);
          _SFD_CH_SUBSTATE_INDEX(3,3);
          _SFD_ST_SUBSTATE_COUNT(0,0);
          _SFD_ST_SUBSTATE_COUNT(1,0);
          _SFD_ST_SUBSTATE_COUNT(2,0);
          _SFD_ST_SUBSTATE_COUNT(3,0);
        }

        _SFD_CV_INIT_CHART(4,1,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(1,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(2,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(3,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(4,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(5,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 14 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(1,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 17 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(4,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 17 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(2,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 17 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(5,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartSaturateMap[] = { 60 };

          static unsigned int sEndSaturateMap[] = { 61 };

          _SFD_CV_INIT_TRANSITION_SATURATION(5,1,&(sStartSaturateMap[0]),
            &(sEndSaturateMap[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 16 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(3,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 15 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(6,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(4,1,1,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(4,0,"make_TX_reg",0,-1,79);
        _SFD_CV_INIT_EML(5,1,1,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(5,0,"make_TX_reg1",0,-1,72);
        _SFD_SET_DATA_COMPILED_PROPS(0,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c3_e_sf_marshallOut,(MexInFcnForType)c3_e_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c3_e_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c3_e_sf_marshallOut,(MexInFcnForType)c3_e_sf_marshallIn);

        {
          unsigned int dimVector[1];
          dimVector[0]= 10;
          _SFD_SET_DATA_COMPILED_PROPS(3,SF_UINT8,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c3_f_sf_marshallOut,(MexInFcnForType)
            c3_f_sf_marshallIn);
        }

        {
          unsigned int dimVector[1];
          dimVector[0]= 8;
          _SFD_SET_DATA_COMPILED_PROPS(4,SF_UINT8,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c3_g_sf_marshallOut,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[1];
          dimVector[0]= 8;
          _SFD_SET_DATA_COMPILED_PROPS(5,SF_UINT8,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c3_g_sf_marshallOut,(MexInFcnForType)
            c3_g_sf_marshallIn);
        }

        _SFD_SET_DATA_COMPILED_PROPS(6,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c3_h_sf_marshallOut,(MexInFcnForType)c3_h_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(7,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c3_e_sf_marshallOut,(MexInFcnForType)NULL);

        {
          boolean_T *c3_TX_empty;
          boolean_T *c3_TX_data_wr;
          boolean_T *c3_txd;
          boolean_T *c3_TX_clk_signal;
          uint8_T (*c3_TX_data)[8];
          c3_TX_clk_signal = (boolean_T *)ssGetInputPortSignal(chartInstance->S,
            2);
          c3_TX_data = (uint8_T (*)[8])ssGetInputPortSignal(chartInstance->S, 1);
          c3_txd = (boolean_T *)ssGetOutputPortSignal(chartInstance->S, 2);
          c3_TX_data_wr = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 0);
          c3_TX_empty = (boolean_T *)ssGetOutputPortSignal(chartInstance->S, 1);
          _SFD_SET_DATA_VALUE_PTR(0U, c3_TX_empty);
          _SFD_SET_DATA_VALUE_PTR(1U, c3_TX_data_wr);
          _SFD_SET_DATA_VALUE_PTR(2U, c3_txd);
          _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c3_TX_reg);
          _SFD_SET_DATA_VALUE_PTR(4U, *c3_TX_data);
          _SFD_SET_DATA_VALUE_PTR(5U, chartInstance->c3_TX_reg_in);
          _SFD_SET_DATA_VALUE_PTR(6U, &chartInstance->c3_TX_bit_count);
          _SFD_SET_DATA_VALUE_PTR(7U, c3_TX_clk_signal);
        }
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _UARTMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "EBn98nZllBDiI9GbXFyzLF";
}

static void sf_opaque_initialize_c3_UART(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc3_UARTInstanceStruct*) chartInstanceVar)->S,0);
  initialize_params_c3_UART((SFc3_UARTInstanceStruct*) chartInstanceVar);
  initialize_c3_UART((SFc3_UARTInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c3_UART(void *chartInstanceVar)
{
  enable_c3_UART((SFc3_UARTInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c3_UART(void *chartInstanceVar)
{
  disable_c3_UART((SFc3_UARTInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c3_UART(void *chartInstanceVar)
{
  sf_gateway_c3_UART((SFc3_UARTInstanceStruct*) chartInstanceVar);
}

extern const mxArray* sf_internal_get_sim_state_c3_UART(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  mxArray *plhs[1] = { NULL };

  mxArray *prhs[4];
  int mxError = 0;
  prhs[0] = mxCreateString("chart_simctx_raw2high");
  prhs[1] = mxCreateDoubleScalar(ssGetSFuncBlockHandle(S));
  prhs[2] = (mxArray*) get_sim_state_c3_UART((SFc3_UARTInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
  prhs[3] = (mxArray*) sf_get_sim_state_info_c3_UART();/* state var info */
  mxError = sf_mex_call_matlab(1, plhs, 4, prhs, "sfprivate");
  mxDestroyArray(prhs[0]);
  mxDestroyArray(prhs[1]);
  mxDestroyArray(prhs[2]);
  mxDestroyArray(prhs[3]);
  if (mxError || plhs[0] == NULL) {
    sf_mex_error_message("Stateflow Internal Error: \nError calling 'chart_simctx_raw2high'.\n");
  }

  return plhs[0];
}

extern void sf_internal_set_sim_state_c3_UART(SimStruct* S, const mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  mxArray *plhs[1] = { NULL };

  mxArray *prhs[3];
  int mxError = 0;
  prhs[0] = mxCreateString("chart_simctx_high2raw");
  prhs[1] = mxDuplicateArray(st);      /* high level simctx */
  prhs[2] = (mxArray*) sf_get_sim_state_info_c3_UART();/* state var info */
  mxError = sf_mex_call_matlab(1, plhs, 3, prhs, "sfprivate");
  mxDestroyArray(prhs[0]);
  mxDestroyArray(prhs[1]);
  mxDestroyArray(prhs[2]);
  if (mxError || plhs[0] == NULL) {
    sf_mex_error_message("Stateflow Internal Error: \nError calling 'chart_simctx_high2raw'.\n");
  }

  set_sim_state_c3_UART((SFc3_UARTInstanceStruct*)chartInfo->chartInstance,
                        mxDuplicateArray(plhs[0]));
  mxDestroyArray(plhs[0]);
}

static const mxArray* sf_opaque_get_sim_state_c3_UART(SimStruct* S)
{
  return sf_internal_get_sim_state_c3_UART(S);
}

static void sf_opaque_set_sim_state_c3_UART(SimStruct* S, const mxArray *st)
{
  sf_internal_set_sim_state_c3_UART(S, st);
}

static void sf_opaque_terminate_c3_UART(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc3_UARTInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_UART_optimization_info();
    }

    finalize_c3_UART((SFc3_UARTInstanceStruct*) chartInstanceVar);
    utFree((void *)chartInstanceVar);
    if (crtInfo != NULL) {
      utFree((void *)crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc3_UART((SFc3_UARTInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c3_UART(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c3_UART((SFc3_UARTInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c3_UART(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_UART_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,3);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,3,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,3,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,3);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,3,3);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,3,2);
    }

    ssSetInputPortOptimOpts(S, 3, SS_REUSABLE_AND_LOCAL);

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=2; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 3; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,3);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(1530813235U));
  ssSetChecksum1(S,(4127936180U));
  ssSetChecksum2(S,(3260472475U));
  ssSetChecksum3(S,(3962360967U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c3_UART(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c3_UART(SimStruct *S)
{
  SFc3_UARTInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc3_UARTInstanceStruct *)utMalloc(sizeof
    (SFc3_UARTInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc3_UARTInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c3_UART;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c3_UART;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c3_UART;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c3_UART;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c3_UART;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c3_UART;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c3_UART;
  chartInstance->chartInfo.getSimStateInfo = sf_get_sim_state_info_c3_UART;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c3_UART;
  chartInstance->chartInfo.mdlStart = mdlStart_c3_UART;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c3_UART;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c3_UART_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c3_UART(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c3_UART(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c3_UART(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c3_UART_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
