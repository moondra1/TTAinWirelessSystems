#ifndef __c5_UART_h__
#define __c5_UART_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc5_UARTInstanceStruct
#define typedef_SFc5_UARTInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c5_sfEvent;
  uint8_T c5_tp_RX_OVF;
  uint8_T c5_tp_START_RX;
  uint8_T c5_tp_IDLE_RX;
  uint8_T c5_tp_EDGE_RX;
  uint8_T c5_tp_STOP_RX;
  uint8_T c5_tp_SHIFT_RX;
  uint8_T c5_tp_DONE_RX;
  boolean_T c5_isStable;
  uint8_T c5_is_active_c5_UART;
  uint8_T c5_is_c5_UART;
  uint16_T c5_RX_bit_count;
  uint8_T c5_RX_reg[8];
  boolean_T c5_RX_clear_clk_div;
  uint8_T c5_doSetSimStateSideEffects;
  const mxArray *c5_setSimStateSideEffectsInfo;
} SFc5_UARTInstanceStruct;

#endif                                 /*typedef_SFc5_UARTInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c5_UART_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c5_UART_get_check_sum(mxArray *plhs[]);
extern void c5_UART_method_dispatcher(SimStruct *S, int_T method, void *data);

#endif
