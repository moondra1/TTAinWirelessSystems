#ifndef __c1_UART_h__
#define __c1_UART_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc1_UARTInstanceStruct
#define typedef_SFc1_UARTInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c1_sfEvent;
  uint8_T c1_tp_S1_C;
  uint8_T c1_tp_S1;
  uint8_T c1_tp_S1_R;
  uint8_T c1_tp_S1_R1;
  uint8_T c1_tp_S1_T;
  uint8_T c1_b_tp_S1;
  boolean_T c1_isStable;
  uint8_T c1_is_active_c1_UART;
  uint8_T c1_is_S1_C;
  uint8_T c1_is_active_S1_C;
  uint8_T c1_is_S1_R;
  uint8_T c1_is_active_S1_R;
  uint8_T c1_is_S1_T;
  uint8_T c1_is_active_S1_T;
  uint32_T c1_divisor;
  uint32_T c1_divisor_TX;
  uint32_T c1_divisor_RX;
  uint8_T c1_doSetSimStateSideEffects;
  const mxArray *c1_setSimStateSideEffectsInfo;
} SFc1_UARTInstanceStruct;

#endif                                 /*typedef_SFc1_UARTInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c1_UART_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c1_UART_get_check_sum(mxArray *plhs[]);
extern void c1_UART_method_dispatcher(SimStruct *S, int_T method, void *data);

#endif
