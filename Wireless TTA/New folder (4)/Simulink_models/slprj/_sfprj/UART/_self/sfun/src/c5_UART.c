/* Include files */

#include <stddef.h>
#include "blas.h"
#include "UART_sfun.h"
#include "c5_UART.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "UART_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define c5_event_WakeUp                (0)
#define CALL_EVENT                     (-1)
#define c5_IN_NO_ACTIVE_CHILD          ((uint8_T)0U)
#define c5_IN_DONE_RX                  ((uint8_T)1U)
#define c5_IN_EDGE_RX                  ((uint8_T)2U)
#define c5_IN_IDLE_RX                  ((uint8_T)3U)
#define c5_IN_RX_OVF                   ((uint8_T)4U)
#define c5_IN_SHIFT_RX                 ((uint8_T)5U)
#define c5_IN_START_RX                 ((uint8_T)6U)
#define c5_IN_STOP_RX                  ((uint8_T)7U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c5_debug_family_names[2] = { "nargin", "nargout" };

/* Function Declarations */
static void initialize_c5_UART(SFc5_UARTInstanceStruct *chartInstance);
static void initialize_params_c5_UART(SFc5_UARTInstanceStruct *chartInstance);
static void enable_c5_UART(SFc5_UARTInstanceStruct *chartInstance);
static void disable_c5_UART(SFc5_UARTInstanceStruct *chartInstance);
static void c5_update_debugger_state_c5_UART(SFc5_UARTInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c5_UART(SFc5_UARTInstanceStruct
  *chartInstance);
static void set_sim_state_c5_UART(SFc5_UARTInstanceStruct *chartInstance, const
  mxArray *c5_st);
static void c5_set_sim_state_side_effects_c5_UART(SFc5_UARTInstanceStruct
  *chartInstance);
static void finalize_c5_UART(SFc5_UARTInstanceStruct *chartInstance);
static void sf_gateway_c5_UART(SFc5_UARTInstanceStruct *chartInstance);
static void c5_chartstep_c5_UART(SFc5_UARTInstanceStruct *chartInstance);
static void initSimStructsc5_UART(SFc5_UARTInstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c5_machineNumber, uint32_T
  c5_chartNumber, uint32_T c5_instanceNumber);
static const mxArray *c5_sf_marshallOut(void *chartInstanceVoid, void *c5_inData);
static real_T c5_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance, const
  mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId);
static void c5_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData);
static void c5_make_RX_reg(SFc5_UARTInstanceStruct *chartInstance);
static const mxArray *c5_b_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData);
static int8_T c5_b_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId);
static void c5_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData);
static const mxArray *c5_c_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData);
static int32_T c5_c_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId);
static void c5_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData);
static const mxArray *c5_d_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData);
static uint8_T c5_d_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance,
  const mxArray *c5_b_tp_RX_OVF, const char_T *c5_identifier);
static uint8_T c5_e_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId);
static void c5_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData);
static const mxArray *c5_e_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData);
static uint16_T c5_f_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance,
  const mxArray *c5_b_RX_bit_count, const char_T *c5_identifier);
static uint16_T c5_g_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId);
static void c5_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData);
static const mxArray *c5_f_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData);
static void c5_h_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance, const
  mxArray *c5_b_RX_reg, const char_T *c5_identifier, uint8_T c5_y[8]);
static void c5_i_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance, const
  mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId, uint8_T c5_y[8]);
static void c5_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData);
static const mxArray *c5_g_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData);
static boolean_T c5_j_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance,
  const mxArray *c5_b_RX_clear_clk_div, const char_T *c5_identifier);
static boolean_T c5_k_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId);
static void c5_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData);
static const mxArray *c5_l_emlrt_marshallIn(SFc5_UARTInstanceStruct
  *chartInstance, const mxArray *c5_b_setSimStateSideEffectsInfo, const char_T
  *c5_identifier);
static const mxArray *c5_m_emlrt_marshallIn(SFc5_UARTInstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId);
static void init_dsm_address_info(SFc5_UARTInstanceStruct *chartInstance);

/* Function Definitions */
static void initialize_c5_UART(SFc5_UARTInstanceStruct *chartInstance)
{
  int32_T c5_i0;
  int32_T c5_i1;
  boolean_T *c5_RX_avail;
  uint8_T (*c5_RX_data)[8];
  c5_RX_data = (uint8_T (*)[8])ssGetOutputPortSignal(chartInstance->S, 2);
  c5_RX_avail = (boolean_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c5_doSetSimStateSideEffects = 0U;
  chartInstance->c5_setSimStateSideEffectsInfo = NULL;
  chartInstance->c5_tp_DONE_RX = 0U;
  chartInstance->c5_tp_EDGE_RX = 0U;
  chartInstance->c5_tp_IDLE_RX = 0U;
  chartInstance->c5_tp_RX_OVF = 0U;
  chartInstance->c5_tp_SHIFT_RX = 0U;
  chartInstance->c5_tp_START_RX = 0U;
  chartInstance->c5_tp_STOP_RX = 0U;
  chartInstance->c5_is_active_c5_UART = 0U;
  chartInstance->c5_is_c5_UART = c5_IN_NO_ACTIVE_CHILD;
  chartInstance->c5_RX_bit_count = 0U;
  for (c5_i0 = 0; c5_i0 < 8; c5_i0++) {
    chartInstance->c5_RX_reg[c5_i0] = 0U;
  }

  chartInstance->c5_RX_clear_clk_div = false;
  if (!(sf_get_output_port_reusable(chartInstance->S, 1) != 0)) {
    *c5_RX_avail = false;
  }

  if (!(sf_get_output_port_reusable(chartInstance->S, 2) != 0)) {
    for (c5_i1 = 0; c5_i1 < 8; c5_i1++) {
      (*c5_RX_data)[c5_i1] = 0U;
    }
  }
}

static void initialize_params_c5_UART(SFc5_UARTInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c5_UART(SFc5_UARTInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c5_UART(SFc5_UARTInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c5_update_debugger_state_c5_UART(SFc5_UARTInstanceStruct
  *chartInstance)
{
  uint32_T c5_prevAniVal;
  c5_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c5_is_active_c5_UART == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 4U, chartInstance->c5_sfEvent);
  }

  if (chartInstance->c5_is_c5_UART == c5_IN_RX_OVF) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c5_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c5_sfEvent);
  }

  if (chartInstance->c5_is_c5_UART == c5_IN_START_RX) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c5_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c5_sfEvent);
  }

  if (chartInstance->c5_is_c5_UART == c5_IN_IDLE_RX) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c5_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c5_sfEvent);
  }

  if (chartInstance->c5_is_c5_UART == c5_IN_EDGE_RX) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c5_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c5_sfEvent);
  }

  if (chartInstance->c5_is_c5_UART == c5_IN_STOP_RX) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c5_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c5_sfEvent);
  }

  if (chartInstance->c5_is_c5_UART == c5_IN_SHIFT_RX) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c5_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c5_sfEvent);
  }

  if (chartInstance->c5_is_c5_UART == c5_IN_DONE_RX) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c5_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c5_sfEvent);
  }

  _SFD_SET_ANIMATION(c5_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static const mxArray *get_sim_state_c5_UART(SFc5_UARTInstanceStruct
  *chartInstance)
{
  const mxArray *c5_st;
  const mxArray *c5_y = NULL;
  boolean_T c5_hoistedGlobal;
  boolean_T c5_u;
  const mxArray *c5_b_y = NULL;
  int32_T c5_i2;
  uint8_T c5_b_u[8];
  const mxArray *c5_c_y = NULL;
  uint16_T c5_b_hoistedGlobal;
  uint16_T c5_c_u;
  const mxArray *c5_d_y = NULL;
  boolean_T c5_c_hoistedGlobal;
  boolean_T c5_d_u;
  const mxArray *c5_e_y = NULL;
  int32_T c5_i3;
  uint8_T c5_e_u[8];
  const mxArray *c5_f_y = NULL;
  uint8_T c5_d_hoistedGlobal;
  uint8_T c5_f_u;
  const mxArray *c5_g_y = NULL;
  uint8_T c5_e_hoistedGlobal;
  uint8_T c5_g_u;
  const mxArray *c5_h_y = NULL;
  boolean_T *c5_RX_avail;
  uint8_T (*c5_RX_data)[8];
  c5_RX_data = (uint8_T (*)[8])ssGetOutputPortSignal(chartInstance->S, 2);
  c5_RX_avail = (boolean_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c5_st = NULL;
  c5_st = NULL;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_createcellmatrix(7, 1), false);
  c5_hoistedGlobal = *c5_RX_avail;
  c5_u = c5_hoistedGlobal;
  c5_b_y = NULL;
  sf_mex_assign(&c5_b_y, sf_mex_create("y", &c5_u, 11, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c5_y, 0, c5_b_y);
  for (c5_i2 = 0; c5_i2 < 8; c5_i2++) {
    c5_b_u[c5_i2] = (*c5_RX_data)[c5_i2];
  }

  c5_c_y = NULL;
  sf_mex_assign(&c5_c_y, sf_mex_create("y", c5_b_u, 3, 0U, 1U, 0U, 1, 8), false);
  sf_mex_setcell(c5_y, 1, c5_c_y);
  c5_b_hoistedGlobal = chartInstance->c5_RX_bit_count;
  c5_c_u = c5_b_hoistedGlobal;
  c5_d_y = NULL;
  sf_mex_assign(&c5_d_y, sf_mex_create("y", &c5_c_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c5_y, 2, c5_d_y);
  c5_c_hoistedGlobal = chartInstance->c5_RX_clear_clk_div;
  c5_d_u = c5_c_hoistedGlobal;
  c5_e_y = NULL;
  sf_mex_assign(&c5_e_y, sf_mex_create("y", &c5_d_u, 11, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c5_y, 3, c5_e_y);
  for (c5_i3 = 0; c5_i3 < 8; c5_i3++) {
    c5_e_u[c5_i3] = chartInstance->c5_RX_reg[c5_i3];
  }

  c5_f_y = NULL;
  sf_mex_assign(&c5_f_y, sf_mex_create("y", c5_e_u, 3, 0U, 1U, 0U, 1, 8), false);
  sf_mex_setcell(c5_y, 4, c5_f_y);
  c5_d_hoistedGlobal = chartInstance->c5_is_active_c5_UART;
  c5_f_u = c5_d_hoistedGlobal;
  c5_g_y = NULL;
  sf_mex_assign(&c5_g_y, sf_mex_create("y", &c5_f_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c5_y, 5, c5_g_y);
  c5_e_hoistedGlobal = chartInstance->c5_is_c5_UART;
  c5_g_u = c5_e_hoistedGlobal;
  c5_h_y = NULL;
  sf_mex_assign(&c5_h_y, sf_mex_create("y", &c5_g_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c5_y, 6, c5_h_y);
  sf_mex_assign(&c5_st, c5_y, false);
  return c5_st;
}

static void set_sim_state_c5_UART(SFc5_UARTInstanceStruct *chartInstance, const
  mxArray *c5_st)
{
  const mxArray *c5_u;
  uint8_T c5_uv0[8];
  int32_T c5_i4;
  uint8_T c5_uv1[8];
  int32_T c5_i5;
  boolean_T *c5_RX_avail;
  uint8_T (*c5_RX_data)[8];
  c5_RX_data = (uint8_T (*)[8])ssGetOutputPortSignal(chartInstance->S, 2);
  c5_RX_avail = (boolean_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c5_u = sf_mex_dup(c5_st);
  *c5_RX_avail = c5_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c5_u, 0)), "RX_avail");
  c5_h_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c5_u, 1)),
                        "RX_data", c5_uv0);
  for (c5_i4 = 0; c5_i4 < 8; c5_i4++) {
    (*c5_RX_data)[c5_i4] = c5_uv0[c5_i4];
  }

  chartInstance->c5_RX_bit_count = c5_f_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c5_u, 2)), "RX_bit_count");
  chartInstance->c5_RX_clear_clk_div = c5_j_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c5_u, 3)), "RX_clear_clk_div");
  c5_h_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c5_u, 4)),
                        "RX_reg", c5_uv1);
  for (c5_i5 = 0; c5_i5 < 8; c5_i5++) {
    chartInstance->c5_RX_reg[c5_i5] = c5_uv1[c5_i5];
  }

  chartInstance->c5_is_active_c5_UART = c5_d_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c5_u, 5)), "is_active_c5_UART");
  chartInstance->c5_is_c5_UART = c5_d_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c5_u, 6)), "is_c5_UART");
  sf_mex_assign(&chartInstance->c5_setSimStateSideEffectsInfo,
                c5_l_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c5_u, 7)), "setSimStateSideEffectsInfo"), true);
  sf_mex_destroy(&c5_u);
  chartInstance->c5_doSetSimStateSideEffects = 1U;
  c5_update_debugger_state_c5_UART(chartInstance);
  sf_mex_destroy(&c5_st);
}

static void c5_set_sim_state_side_effects_c5_UART(SFc5_UARTInstanceStruct
  *chartInstance)
{
  if (chartInstance->c5_doSetSimStateSideEffects != 0) {
    if (chartInstance->c5_is_c5_UART == c5_IN_DONE_RX) {
      chartInstance->c5_tp_DONE_RX = 1U;
    } else {
      chartInstance->c5_tp_DONE_RX = 0U;
    }

    if (chartInstance->c5_is_c5_UART == c5_IN_EDGE_RX) {
      chartInstance->c5_tp_EDGE_RX = 1U;
    } else {
      chartInstance->c5_tp_EDGE_RX = 0U;
    }

    if (chartInstance->c5_is_c5_UART == c5_IN_IDLE_RX) {
      chartInstance->c5_tp_IDLE_RX = 1U;
    } else {
      chartInstance->c5_tp_IDLE_RX = 0U;
    }

    if (chartInstance->c5_is_c5_UART == c5_IN_RX_OVF) {
      chartInstance->c5_tp_RX_OVF = 1U;
    } else {
      chartInstance->c5_tp_RX_OVF = 0U;
    }

    if (chartInstance->c5_is_c5_UART == c5_IN_SHIFT_RX) {
      chartInstance->c5_tp_SHIFT_RX = 1U;
    } else {
      chartInstance->c5_tp_SHIFT_RX = 0U;
    }

    if (chartInstance->c5_is_c5_UART == c5_IN_START_RX) {
      chartInstance->c5_tp_START_RX = 1U;
    } else {
      chartInstance->c5_tp_START_RX = 0U;
    }

    if (chartInstance->c5_is_c5_UART == c5_IN_STOP_RX) {
      chartInstance->c5_tp_STOP_RX = 1U;
    } else {
      chartInstance->c5_tp_STOP_RX = 0U;
    }

    chartInstance->c5_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c5_UART(SFc5_UARTInstanceStruct *chartInstance)
{
  sf_mex_destroy(&chartInstance->c5_setSimStateSideEffectsInfo);
}

static void sf_gateway_c5_UART(SFc5_UARTInstanceStruct *chartInstance)
{
  int32_T c5_i6;
  int32_T c5_i7;
  boolean_T *c5_rxd;
  boolean_T *c5_common_clk;
  boolean_T *c5_RX_clk_signal;
  boolean_T *c5_RX_avail;
  boolean_T *c5_RX_data_rd;
  uint8_T (*c5_RX_data)[8];
  c5_RX_data_rd = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 3);
  c5_RX_data = (uint8_T (*)[8])ssGetOutputPortSignal(chartInstance->S, 2);
  c5_RX_avail = (boolean_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c5_RX_clk_signal = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 2);
  c5_common_clk = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 1);
  c5_rxd = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 0);
  c5_set_sim_state_side_effects_c5_UART(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 4U, chartInstance->c5_sfEvent);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c5_RX_bit_count, 0U);
  for (c5_i6 = 0; c5_i6 < 8; c5_i6++) {
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c5_RX_reg[c5_i6], 1U);
  }

  _SFD_DATA_RANGE_CHECK((real_T)*c5_rxd, 2U);
  _SFD_DATA_RANGE_CHECK((real_T)*c5_common_clk, 3U);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c5_RX_clear_clk_div, 4U);
  _SFD_DATA_RANGE_CHECK((real_T)*c5_RX_clk_signal, 5U);
  _SFD_DATA_RANGE_CHECK((real_T)*c5_RX_avail, 6U);
  for (c5_i7 = 0; c5_i7 < 8; c5_i7++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*c5_RX_data)[c5_i7], 7U);
  }

  _SFD_DATA_RANGE_CHECK((real_T)*c5_RX_data_rd, 8U);
  chartInstance->c5_sfEvent = c5_event_WakeUp;
  _SFD_CE_CALL(EVENT_BEFORE_BROADCAST_TAG, c5_event_WakeUp,
               chartInstance->c5_sfEvent);
  c5_chartstep_c5_UART(chartInstance);
  _SFD_CE_CALL(EVENT_AFTER_BROADCAST_TAG, c5_event_WakeUp,
               chartInstance->c5_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_UARTMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void c5_chartstep_c5_UART(SFc5_UARTInstanceStruct *chartInstance)
{
  boolean_T c5_out;
  boolean_T c5_b_out;
  boolean_T c5_c_out;
  boolean_T c5_d_out;
  boolean_T c5_temp;
  boolean_T c5_e_out;
  boolean_T c5_f_out;
  boolean_T c5_g_out;
  boolean_T c5_h_out;
  boolean_T c5_i_out;
  boolean_T c5_j_out;
  boolean_T c5_k_out;
  int32_T c5_i8;
  int32_T c5_i9;
  boolean_T *c5_RX_data_rd;
  boolean_T *c5_RX_avail;
  boolean_T *c5_RX_clk_signal;
  boolean_T *c5_common_clk;
  boolean_T *c5_rxd;
  uint8_T (*c5_RX_data)[8];
  boolean_T guard1 = false;
  boolean_T guard2 = false;
  c5_RX_data_rd = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 3);
  c5_RX_data = (uint8_T (*)[8])ssGetOutputPortSignal(chartInstance->S, 2);
  c5_RX_avail = (boolean_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c5_RX_clk_signal = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 2);
  c5_common_clk = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 1);
  c5_rxd = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 0);
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 4U, chartInstance->c5_sfEvent);
  if (chartInstance->c5_is_active_c5_UART == 0U) {
    _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 4U, chartInstance->c5_sfEvent);
    chartInstance->c5_is_active_c5_UART = 1U;
    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, chartInstance->c5_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 11U, chartInstance->c5_sfEvent);
    chartInstance->c5_is_c5_UART = c5_IN_IDLE_RX;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c5_sfEvent);
    chartInstance->c5_tp_IDLE_RX = 1U;
    chartInstance->c5_RX_bit_count = 0U;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c5_RX_bit_count, 0U);
    *c5_RX_avail = false;
    _SFD_DATA_RANGE_CHECK((real_T)*c5_RX_avail, 6U);
  } else {
    switch (chartInstance->c5_is_c5_UART) {
     case c5_IN_DONE_RX:
      CV_CHART_EVAL(4, 0, 1);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 10U,
                   chartInstance->c5_sfEvent);
      c5_out = (CV_TRANSITION_EVAL(10U, (int32_T)_SFD_CCP_CALL(10U, 0, (int32_T)*
                  c5_RX_data_rd == 1 != 0U, chartInstance->c5_sfEvent)) != 0);
      if (c5_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 10U, chartInstance->c5_sfEvent);
        chartInstance->c5_tp_DONE_RX = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c5_sfEvent);
        chartInstance->c5_is_c5_UART = c5_IN_IDLE_RX;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c5_sfEvent);
        chartInstance->c5_tp_IDLE_RX = 1U;
        chartInstance->c5_RX_bit_count = 0U;
        _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c5_RX_bit_count, 0U);
        *c5_RX_avail = false;
        _SFD_DATA_RANGE_CHECK((real_T)*c5_RX_avail, 6U);
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U,
                     chartInstance->c5_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c5_sfEvent);
      break;

     case c5_IN_EDGE_RX:
      CV_CHART_EVAL(4, 0, 2);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 4U,
                   chartInstance->c5_sfEvent);
      c5_b_out = (CV_TRANSITION_EVAL(4U, (int32_T)_SFD_CCP_CALL(4U, 0, (int32_T)*
        c5_RX_clk_signal == 1 != 0U, chartInstance->c5_sfEvent)) != 0);
      guard2 = false;
      if (c5_b_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 4U, chartInstance->c5_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 5U,
                     chartInstance->c5_sfEvent);
        c5_c_out = (CV_TRANSITION_EVAL(5U, (int32_T)_SFD_CCP_CALL(5U, 0,
          chartInstance->c5_RX_bit_count != 8 != 0U, chartInstance->c5_sfEvent))
                    != 0);
        if (c5_c_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 5U, chartInstance->c5_sfEvent);
          chartInstance->c5_tp_EDGE_RX = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c5_sfEvent);
          chartInstance->c5_is_c5_UART = c5_IN_SHIFT_RX;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c5_sfEvent);
          chartInstance->c5_tp_SHIFT_RX = 1U;
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 6U,
                       chartInstance->c5_sfEvent);
          c5_d_out = (CV_TRANSITION_EVAL(6U, (int32_T)_SFD_CCP_CALL(6U, 0,
            chartInstance->c5_RX_bit_count == 8 != 0U, chartInstance->c5_sfEvent))
                      != 0);
          if (c5_d_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 6U, chartInstance->c5_sfEvent);
            chartInstance->c5_tp_EDGE_RX = 0U;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c5_sfEvent);
            chartInstance->c5_is_c5_UART = c5_IN_STOP_RX;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c5_sfEvent);
            chartInstance->c5_tp_STOP_RX = 1U;
          } else {
            guard2 = true;
          }
        }
      } else {
        guard2 = true;
      }

      if (guard2 == true) {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U,
                     chartInstance->c5_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c5_sfEvent);
      break;

     case c5_IN_IDLE_RX:
      CV_CHART_EVAL(4, 0, 3);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 0U,
                   chartInstance->c5_sfEvent);
      c5_temp = (_SFD_CCP_CALL(0U, 0, (int32_T)*c5_common_clk == 1 != 0U,
                  chartInstance->c5_sfEvent) != 0);
      if (c5_temp) {
        c5_temp = (_SFD_CCP_CALL(0U, 1, (int32_T)*c5_rxd != 1 != 0U,
                    chartInstance->c5_sfEvent) != 0);
      }

      c5_e_out = (CV_TRANSITION_EVAL(0U, (int32_T)c5_temp) != 0);
      if (c5_e_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c5_sfEvent);
        chartInstance->c5_tp_IDLE_RX = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c5_sfEvent);
        chartInstance->c5_is_c5_UART = c5_IN_START_RX;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c5_sfEvent);
        chartInstance->c5_tp_START_RX = 1U;
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U,
                     chartInstance->c5_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c5_sfEvent);
      break;

     case c5_IN_RX_OVF:
      CV_CHART_EVAL(4, 0, 4);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 9U,
                   chartInstance->c5_sfEvent);
      c5_f_out = (CV_TRANSITION_EVAL(9U, (int32_T)_SFD_CCP_CALL(9U, 0, (int32_T)*
        c5_rxd == 1 != 0U, chartInstance->c5_sfEvent)) != 0);
      if (c5_f_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 9U, chartInstance->c5_sfEvent);
        chartInstance->c5_tp_RX_OVF = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c5_sfEvent);
        chartInstance->c5_is_c5_UART = c5_IN_IDLE_RX;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c5_sfEvent);
        chartInstance->c5_tp_IDLE_RX = 1U;
        chartInstance->c5_RX_bit_count = 0U;
        _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c5_RX_bit_count, 0U);
        *c5_RX_avail = false;
        _SFD_DATA_RANGE_CHECK((real_T)*c5_RX_avail, 6U);
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U,
                     chartInstance->c5_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c5_sfEvent);
      break;

     case c5_IN_SHIFT_RX:
      CV_CHART_EVAL(4, 0, 5);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 7U,
                   chartInstance->c5_sfEvent);
      c5_g_out = (CV_TRANSITION_EVAL(7U, (int32_T)_SFD_CCP_CALL(7U, 0, (int32_T)*
        c5_RX_clk_signal == 1 != 0U, chartInstance->c5_sfEvent)) != 0);
      if (c5_g_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 7U, chartInstance->c5_sfEvent);
        chartInstance->c5_tp_SHIFT_RX = 0U;
        chartInstance->c5_is_c5_UART = c5_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c5_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_TRANS_ACTION_TAG, 7U,
                     chartInstance->c5_sfEvent);
        c5_make_RX_reg(chartInstance);
        chartInstance->c5_is_c5_UART = c5_IN_EDGE_RX;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c5_sfEvent);
        chartInstance->c5_tp_EDGE_RX = 1U;
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 4U,
                     chartInstance->c5_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, chartInstance->c5_sfEvent);
      break;

     case c5_IN_START_RX:
      CV_CHART_EVAL(4, 0, 6);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 1U,
                   chartInstance->c5_sfEvent);
      c5_h_out = (CV_TRANSITION_EVAL(1U, (int32_T)_SFD_CCP_CALL(1U, 0, (int32_T)*
        c5_RX_clk_signal == 1 != 0U, chartInstance->c5_sfEvent)) != 0);
      guard1 = false;
      if (c5_h_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, chartInstance->c5_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 2U,
                     chartInstance->c5_sfEvent);
        c5_i_out = (CV_TRANSITION_EVAL(2U, (int32_T)_SFD_CCP_CALL(2U, 0,
          (int32_T)*c5_rxd == 1 != 0U, chartInstance->c5_sfEvent)) != 0);
        if (c5_i_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c5_sfEvent);
          chartInstance->c5_tp_START_RX = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c5_sfEvent);
          chartInstance->c5_is_c5_UART = c5_IN_RX_OVF;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c5_sfEvent);
          chartInstance->c5_tp_RX_OVF = 1U;
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 3U,
                       chartInstance->c5_sfEvent);
          c5_j_out = (CV_TRANSITION_EVAL(3U, (int32_T)_SFD_CCP_CALL(3U, 0,
            (int32_T)*c5_rxd == 0 != 0U, chartInstance->c5_sfEvent)) != 0);
          if (c5_j_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 3U, chartInstance->c5_sfEvent);
            chartInstance->c5_tp_START_RX = 0U;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c5_sfEvent);
            chartInstance->c5_is_c5_UART = c5_IN_EDGE_RX;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c5_sfEvent);
            chartInstance->c5_tp_EDGE_RX = 1U;
          } else {
            guard1 = true;
          }
        }
      } else {
        guard1 = true;
      }

      if (guard1 == true) {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 5U,
                     chartInstance->c5_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U, chartInstance->c5_sfEvent);
      break;

     case c5_IN_STOP_RX:
      CV_CHART_EVAL(4, 0, 7);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 8U,
                   chartInstance->c5_sfEvent);
      c5_k_out = (CV_TRANSITION_EVAL(8U, (int32_T)_SFD_CCP_CALL(8U, 0, (int32_T)*
        c5_RX_clk_signal == 1 != 0U, chartInstance->c5_sfEvent)) != 0);
      if (c5_k_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 8U, chartInstance->c5_sfEvent);
        chartInstance->c5_tp_STOP_RX = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c5_sfEvent);
        chartInstance->c5_is_c5_UART = c5_IN_DONE_RX;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c5_sfEvent);
        chartInstance->c5_tp_DONE_RX = 1U;
        for (c5_i8 = 0; c5_i8 < 8; c5_i8++) {
          (*c5_RX_data)[c5_i8] = chartInstance->c5_RX_reg[c5_i8];
        }

        for (c5_i9 = 0; c5_i9 < 8; c5_i9++) {
          _SFD_DATA_RANGE_CHECK((real_T)(*c5_RX_data)[c5_i9], 7U);
        }

        *c5_RX_avail = true;
        _SFD_DATA_RANGE_CHECK((real_T)*c5_RX_avail, 6U);
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 6U,
                     chartInstance->c5_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 6U, chartInstance->c5_sfEvent);
      break;

     default:
      CV_CHART_EVAL(4, 0, 0);
      chartInstance->c5_is_c5_UART = c5_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c5_sfEvent);
      break;
    }
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, chartInstance->c5_sfEvent);
}

static void initSimStructsc5_UART(SFc5_UARTInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c5_machineNumber, uint32_T
  c5_chartNumber, uint32_T c5_instanceNumber)
{
  (void)c5_machineNumber;
  (void)c5_chartNumber;
  (void)c5_instanceNumber;
}

static const mxArray *c5_sf_marshallOut(void *chartInstanceVoid, void *c5_inData)
{
  const mxArray *c5_mxArrayOutData = NULL;
  real_T c5_u;
  const mxArray *c5_y = NULL;
  SFc5_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc5_UARTInstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  c5_u = *(real_T *)c5_inData;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", &c5_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static real_T c5_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance, const
  mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId)
{
  real_T c5_y;
  real_T c5_d0;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), &c5_d0, 1, 0, 0U, 0, 0U, 0);
  c5_y = c5_d0;
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void c5_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData)
{
  const mxArray *c5_nargout;
  const char_T *c5_identifier;
  emlrtMsgIdentifier c5_thisId;
  real_T c5_y;
  SFc5_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc5_UARTInstanceStruct *)chartInstanceVoid;
  c5_nargout = sf_mex_dup(c5_mxArrayInData);
  c5_identifier = c5_varName;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_y = c5_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_nargout), &c5_thisId);
  sf_mex_destroy(&c5_nargout);
  *(real_T *)c5_outData = c5_y;
  sf_mex_destroy(&c5_mxArrayInData);
}

const mxArray *sf_c5_UART_get_eml_resolved_functions_info(void)
{
  const mxArray *c5_nameCaptureInfo = NULL;
  c5_nameCaptureInfo = NULL;
  sf_mex_assign(&c5_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c5_nameCaptureInfo;
}

static void c5_make_RX_reg(SFc5_UARTInstanceStruct *chartInstance)
{
  uint32_T c5_debug_family_var_map[2];
  real_T c5_nargin = 0.0;
  real_T c5_nargout = 0.0;
  uint32_T c5_u0;
  int32_T c5_i10;
  uint8_T c5_uv2[8];
  int32_T c5_i11;
  boolean_T *c5_rxd;
  c5_rxd = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 0);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c5_debug_family_names,
    c5_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_nargin, 0U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_nargout, 1U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  CV_EML_FCN(7, 0);
  _SFD_EML_CALL(7U, chartInstance->c5_sfEvent, 2);
  c5_u0 = (uint32_T)chartInstance->c5_RX_bit_count + 1U;
  if (CV_SATURATION_EVAL(4, 7, 0, 0, c5_u0 > 65535U)) {
    c5_u0 = 65535U;
  }

  chartInstance->c5_RX_bit_count = (uint16_T)c5_u0;
  _SFD_EML_CALL(7U, chartInstance->c5_sfEvent, 3);
  for (c5_i10 = 0; c5_i10 < 7; c5_i10++) {
    c5_uv2[c5_i10] = chartInstance->c5_RX_reg[c5_i10 + 1];
  }

  c5_uv2[7] = (uint8_T)*c5_rxd;
  for (c5_i11 = 0; c5_i11 < 8; c5_i11++) {
    chartInstance->c5_RX_reg[c5_i11] = c5_uv2[c5_i11];
  }

  _SFD_EML_CALL(7U, chartInstance->c5_sfEvent, -3);
  _SFD_SYMBOL_SCOPE_POP();
}

static const mxArray *c5_b_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData)
{
  const mxArray *c5_mxArrayOutData = NULL;
  int8_T c5_u;
  const mxArray *c5_y = NULL;
  SFc5_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc5_UARTInstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  c5_u = *(int8_T *)c5_inData;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", &c5_u, 2, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static int8_T c5_b_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId)
{
  int8_T c5_y;
  int8_T c5_i12;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), &c5_i12, 1, 2, 0U, 0, 0U, 0);
  c5_y = c5_i12;
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void c5_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData)
{
  const mxArray *c5_WakeUp;
  const char_T *c5_identifier;
  emlrtMsgIdentifier c5_thisId;
  int8_T c5_y;
  SFc5_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc5_UARTInstanceStruct *)chartInstanceVoid;
  c5_WakeUp = sf_mex_dup(c5_mxArrayInData);
  c5_identifier = c5_varName;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_y = c5_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_WakeUp), &c5_thisId);
  sf_mex_destroy(&c5_WakeUp);
  *(int8_T *)c5_outData = c5_y;
  sf_mex_destroy(&c5_mxArrayInData);
}

static const mxArray *c5_c_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData)
{
  const mxArray *c5_mxArrayOutData = NULL;
  int32_T c5_u;
  const mxArray *c5_y = NULL;
  SFc5_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc5_UARTInstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  c5_u = *(int32_T *)c5_inData;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", &c5_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static int32_T c5_c_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId)
{
  int32_T c5_y;
  int32_T c5_i13;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), &c5_i13, 1, 6, 0U, 0, 0U, 0);
  c5_y = c5_i13;
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void c5_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData)
{
  const mxArray *c5_b_sfEvent;
  const char_T *c5_identifier;
  emlrtMsgIdentifier c5_thisId;
  int32_T c5_y;
  SFc5_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc5_UARTInstanceStruct *)chartInstanceVoid;
  c5_b_sfEvent = sf_mex_dup(c5_mxArrayInData);
  c5_identifier = c5_varName;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_y = c5_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_b_sfEvent),
    &c5_thisId);
  sf_mex_destroy(&c5_b_sfEvent);
  *(int32_T *)c5_outData = c5_y;
  sf_mex_destroy(&c5_mxArrayInData);
}

static const mxArray *c5_d_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData)
{
  const mxArray *c5_mxArrayOutData = NULL;
  uint8_T c5_u;
  const mxArray *c5_y = NULL;
  SFc5_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc5_UARTInstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  c5_u = *(uint8_T *)c5_inData;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", &c5_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static uint8_T c5_d_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance,
  const mxArray *c5_b_tp_RX_OVF, const char_T *c5_identifier)
{
  uint8_T c5_y;
  emlrtMsgIdentifier c5_thisId;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_y = c5_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_b_tp_RX_OVF),
    &c5_thisId);
  sf_mex_destroy(&c5_b_tp_RX_OVF);
  return c5_y;
}

static uint8_T c5_e_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId)
{
  uint8_T c5_y;
  uint8_T c5_u1;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), &c5_u1, 1, 3, 0U, 0, 0U, 0);
  c5_y = c5_u1;
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void c5_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData)
{
  const mxArray *c5_b_tp_RX_OVF;
  const char_T *c5_identifier;
  emlrtMsgIdentifier c5_thisId;
  uint8_T c5_y;
  SFc5_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc5_UARTInstanceStruct *)chartInstanceVoid;
  c5_b_tp_RX_OVF = sf_mex_dup(c5_mxArrayInData);
  c5_identifier = c5_varName;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_y = c5_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_b_tp_RX_OVF),
    &c5_thisId);
  sf_mex_destroy(&c5_b_tp_RX_OVF);
  *(uint8_T *)c5_outData = c5_y;
  sf_mex_destroy(&c5_mxArrayInData);
}

static const mxArray *c5_e_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData)
{
  const mxArray *c5_mxArrayOutData = NULL;
  uint16_T c5_u;
  const mxArray *c5_y = NULL;
  SFc5_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc5_UARTInstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  c5_u = *(uint16_T *)c5_inData;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", &c5_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static uint16_T c5_f_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance,
  const mxArray *c5_b_RX_bit_count, const char_T *c5_identifier)
{
  uint16_T c5_y;
  emlrtMsgIdentifier c5_thisId;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_y = c5_g_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_b_RX_bit_count),
    &c5_thisId);
  sf_mex_destroy(&c5_b_RX_bit_count);
  return c5_y;
}

static uint16_T c5_g_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId)
{
  uint16_T c5_y;
  uint16_T c5_u2;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), &c5_u2, 1, 5, 0U, 0, 0U, 0);
  c5_y = c5_u2;
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void c5_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData)
{
  const mxArray *c5_b_RX_bit_count;
  const char_T *c5_identifier;
  emlrtMsgIdentifier c5_thisId;
  uint16_T c5_y;
  SFc5_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc5_UARTInstanceStruct *)chartInstanceVoid;
  c5_b_RX_bit_count = sf_mex_dup(c5_mxArrayInData);
  c5_identifier = c5_varName;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_y = c5_g_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_b_RX_bit_count),
    &c5_thisId);
  sf_mex_destroy(&c5_b_RX_bit_count);
  *(uint16_T *)c5_outData = c5_y;
  sf_mex_destroy(&c5_mxArrayInData);
}

static const mxArray *c5_f_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData)
{
  const mxArray *c5_mxArrayOutData = NULL;
  int32_T c5_i14;
  uint8_T c5_b_inData[8];
  int32_T c5_i15;
  uint8_T c5_u[8];
  const mxArray *c5_y = NULL;
  SFc5_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc5_UARTInstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  for (c5_i14 = 0; c5_i14 < 8; c5_i14++) {
    c5_b_inData[c5_i14] = (*(uint8_T (*)[8])c5_inData)[c5_i14];
  }

  for (c5_i15 = 0; c5_i15 < 8; c5_i15++) {
    c5_u[c5_i15] = c5_b_inData[c5_i15];
  }

  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", c5_u, 3, 0U, 1U, 0U, 1, 8), false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static void c5_h_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance, const
  mxArray *c5_b_RX_reg, const char_T *c5_identifier, uint8_T c5_y[8])
{
  emlrtMsgIdentifier c5_thisId;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_b_RX_reg), &c5_thisId, c5_y);
  sf_mex_destroy(&c5_b_RX_reg);
}

static void c5_i_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance, const
  mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId, uint8_T c5_y[8])
{
  uint8_T c5_uv3[8];
  int32_T c5_i16;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), c5_uv3, 1, 3, 0U, 1, 0U, 1, 8);
  for (c5_i16 = 0; c5_i16 < 8; c5_i16++) {
    c5_y[c5_i16] = c5_uv3[c5_i16];
  }

  sf_mex_destroy(&c5_u);
}

static void c5_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData)
{
  const mxArray *c5_b_RX_reg;
  const char_T *c5_identifier;
  emlrtMsgIdentifier c5_thisId;
  uint8_T c5_y[8];
  int32_T c5_i17;
  SFc5_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc5_UARTInstanceStruct *)chartInstanceVoid;
  c5_b_RX_reg = sf_mex_dup(c5_mxArrayInData);
  c5_identifier = c5_varName;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_b_RX_reg), &c5_thisId, c5_y);
  sf_mex_destroy(&c5_b_RX_reg);
  for (c5_i17 = 0; c5_i17 < 8; c5_i17++) {
    (*(uint8_T (*)[8])c5_outData)[c5_i17] = c5_y[c5_i17];
  }

  sf_mex_destroy(&c5_mxArrayInData);
}

static const mxArray *c5_g_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData)
{
  const mxArray *c5_mxArrayOutData = NULL;
  boolean_T c5_u;
  const mxArray *c5_y = NULL;
  SFc5_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc5_UARTInstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  c5_u = *(boolean_T *)c5_inData;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", &c5_u, 11, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static boolean_T c5_j_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance,
  const mxArray *c5_b_RX_clear_clk_div, const char_T *c5_identifier)
{
  boolean_T c5_y;
  emlrtMsgIdentifier c5_thisId;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_y = c5_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_b_RX_clear_clk_div),
    &c5_thisId);
  sf_mex_destroy(&c5_b_RX_clear_clk_div);
  return c5_y;
}

static boolean_T c5_k_emlrt_marshallIn(SFc5_UARTInstanceStruct *chartInstance,
  const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId)
{
  boolean_T c5_y;
  boolean_T c5_b0;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), &c5_b0, 1, 11, 0U, 0, 0U, 0);
  c5_y = c5_b0;
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void c5_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData)
{
  const mxArray *c5_b_RX_clear_clk_div;
  const char_T *c5_identifier;
  emlrtMsgIdentifier c5_thisId;
  boolean_T c5_y;
  SFc5_UARTInstanceStruct *chartInstance;
  chartInstance = (SFc5_UARTInstanceStruct *)chartInstanceVoid;
  c5_b_RX_clear_clk_div = sf_mex_dup(c5_mxArrayInData);
  c5_identifier = c5_varName;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_y = c5_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_b_RX_clear_clk_div),
    &c5_thisId);
  sf_mex_destroy(&c5_b_RX_clear_clk_div);
  *(boolean_T *)c5_outData = c5_y;
  sf_mex_destroy(&c5_mxArrayInData);
}

static const mxArray *c5_l_emlrt_marshallIn(SFc5_UARTInstanceStruct
  *chartInstance, const mxArray *c5_b_setSimStateSideEffectsInfo, const char_T
  *c5_identifier)
{
  const mxArray *c5_y = NULL;
  emlrtMsgIdentifier c5_thisId;
  c5_y = NULL;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  sf_mex_assign(&c5_y, c5_m_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c5_b_setSimStateSideEffectsInfo), &c5_thisId), false);
  sf_mex_destroy(&c5_b_setSimStateSideEffectsInfo);
  return c5_y;
}

static const mxArray *c5_m_emlrt_marshallIn(SFc5_UARTInstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId)
{
  const mxArray *c5_y = NULL;
  (void)chartInstance;
  (void)c5_parentId;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_duplicatearraysafe(&c5_u), false);
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void init_dsm_address_info(SFc5_UARTInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c5_UART_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1157485160U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3158804641U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(280481055U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2046214828U);
}

mxArray *sf_c5_UART_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1,1,5,
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("1UYRGVAaUX7j90Sp4E7ijD");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,4,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(8);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c5_UART_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c5_UART_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c5_UART(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x7'type','srcId','name','auxInfo'{{M[1],M[26],T\"RX_avail\",},{M[1],M[27],T\"RX_data\",},{M[3],M[8],T\"RX_bit_count\",},{M[3],M[14],T\"RX_clear_clk_div\",},{M[3],M[9],T\"RX_reg\",},{M[8],M[0],T\"is_active_c5_UART\",},{M[9],M[0],T\"is_c5_UART\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 7, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c5_UART_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc5_UARTInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc5_UARTInstanceStruct *) chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _UARTMachineNumber_,
           5,
           8,
           12,
           0,
           9,
           1,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize ist own list of scripts */
        init_script_number_translation(_UARTMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_UARTMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _UARTMachineNumber_,
            chartInstance->chartNumber,
            1,
            1,
            1);
          _SFD_SET_DATA_PROPS(0,0,0,0,"RX_bit_count");
          _SFD_SET_DATA_PROPS(1,0,0,0,"RX_reg");
          _SFD_SET_DATA_PROPS(2,1,1,0,"rxd");
          _SFD_SET_DATA_PROPS(3,1,1,0,"common_clk");
          _SFD_SET_DATA_PROPS(4,0,0,0,"RX_clear_clk_div");
          _SFD_SET_DATA_PROPS(5,1,1,0,"RX_clk_signal");
          _SFD_SET_DATA_PROPS(6,2,0,1,"RX_avail");
          _SFD_SET_DATA_PROPS(7,2,0,1,"RX_data");
          _SFD_SET_DATA_PROPS(8,1,1,0,"RX_data_rd");
          _SFD_EVENT_SCOPE(0,1);
          _SFD_STATE_INFO(0,0,0);
          _SFD_STATE_INFO(1,0,0);
          _SFD_STATE_INFO(2,0,0);
          _SFD_STATE_INFO(3,0,0);
          _SFD_STATE_INFO(4,0,0);
          _SFD_STATE_INFO(5,0,0);
          _SFD_STATE_INFO(6,0,0);
          _SFD_STATE_INFO(7,0,2);
          _SFD_CH_SUBSTATE_COUNT(7);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_CH_SUBSTATE_INDEX(1,1);
          _SFD_CH_SUBSTATE_INDEX(2,2);
          _SFD_CH_SUBSTATE_INDEX(3,3);
          _SFD_CH_SUBSTATE_INDEX(4,4);
          _SFD_CH_SUBSTATE_INDEX(5,5);
          _SFD_CH_SUBSTATE_INDEX(6,6);
          _SFD_ST_SUBSTATE_COUNT(0,0);
          _SFD_ST_SUBSTATE_COUNT(1,0);
          _SFD_ST_SUBSTATE_COUNT(2,0);
          _SFD_ST_SUBSTATE_COUNT(3,0);
          _SFD_ST_SUBSTATE_COUNT(4,0);
          _SFD_ST_SUBSTATE_COUNT(5,0);
          _SFD_ST_SUBSTATE_COUNT(6,0);
        }

        _SFD_CV_INIT_CHART(7,1,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(1,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(2,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(3,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(4,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(5,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(6,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(7,0,0,0,0,0,NULL,NULL);
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 7 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(9,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 8 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(2,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1, 18 };

          static unsigned int sEndGuardMap[] = { 14, 25 };

          static int sPostFixPredicateTree[] = { 0, 1, -3 };

          _SFD_CV_INIT_TRANS(0,2,&(sStartGuardMap[0]),&(sEndGuardMap[0]),3,
                             &(sPostFixPredicateTree[0]));
        }

        _SFD_CV_INIT_TRANS(11,0,NULL,NULL,0,NULL);

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 17 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(1,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 8 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(3,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 14 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(10,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 17 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(7,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 17 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(4,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 16 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(6,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 16 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(5,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 17 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(8,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(7,1,1,0,0,1,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(7,0,"make_RX_reg",0,-1,78);
        _SFD_CV_INIT_EML_SATURATION(7,1,0,36,-1,50);
        _SFD_SET_DATA_COMPILED_PROPS(0,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_e_sf_marshallOut,(MexInFcnForType)c5_e_sf_marshallIn);

        {
          unsigned int dimVector[1];
          dimVector[0]= 8;
          _SFD_SET_DATA_COMPILED_PROPS(1,SF_UINT8,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c5_f_sf_marshallOut,(MexInFcnForType)
            c5_f_sf_marshallIn);
        }

        _SFD_SET_DATA_COMPILED_PROPS(2,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_g_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_g_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_g_sf_marshallOut,(MexInFcnForType)c5_g_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(5,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_g_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(6,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_g_sf_marshallOut,(MexInFcnForType)c5_g_sf_marshallIn);

        {
          unsigned int dimVector[1];
          dimVector[0]= 8;
          _SFD_SET_DATA_COMPILED_PROPS(7,SF_UINT8,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c5_f_sf_marshallOut,(MexInFcnForType)
            c5_f_sf_marshallIn);
        }

        _SFD_SET_DATA_COMPILED_PROPS(8,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_g_sf_marshallOut,(MexInFcnForType)NULL);

        {
          boolean_T *c5_rxd;
          boolean_T *c5_common_clk;
          boolean_T *c5_RX_clk_signal;
          boolean_T *c5_RX_avail;
          boolean_T *c5_RX_data_rd;
          uint8_T (*c5_RX_data)[8];
          c5_RX_data_rd = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 3);
          c5_RX_data = (uint8_T (*)[8])ssGetOutputPortSignal(chartInstance->S, 2);
          c5_RX_avail = (boolean_T *)ssGetOutputPortSignal(chartInstance->S, 1);
          c5_RX_clk_signal = (boolean_T *)ssGetInputPortSignal(chartInstance->S,
            2);
          c5_common_clk = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 1);
          c5_rxd = (boolean_T *)ssGetInputPortSignal(chartInstance->S, 0);
          _SFD_SET_DATA_VALUE_PTR(0U, &chartInstance->c5_RX_bit_count);
          _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c5_RX_reg);
          _SFD_SET_DATA_VALUE_PTR(2U, c5_rxd);
          _SFD_SET_DATA_VALUE_PTR(3U, c5_common_clk);
          _SFD_SET_DATA_VALUE_PTR(4U, &chartInstance->c5_RX_clear_clk_div);
          _SFD_SET_DATA_VALUE_PTR(5U, c5_RX_clk_signal);
          _SFD_SET_DATA_VALUE_PTR(6U, c5_RX_avail);
          _SFD_SET_DATA_VALUE_PTR(7U, *c5_RX_data);
          _SFD_SET_DATA_VALUE_PTR(8U, c5_RX_data_rd);
        }
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _UARTMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "k9qbVNEBmvxnSN7nUnAeED";
}

static void sf_opaque_initialize_c5_UART(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc5_UARTInstanceStruct*) chartInstanceVar)->S,0);
  initialize_params_c5_UART((SFc5_UARTInstanceStruct*) chartInstanceVar);
  initialize_c5_UART((SFc5_UARTInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c5_UART(void *chartInstanceVar)
{
  enable_c5_UART((SFc5_UARTInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c5_UART(void *chartInstanceVar)
{
  disable_c5_UART((SFc5_UARTInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c5_UART(void *chartInstanceVar)
{
  sf_gateway_c5_UART((SFc5_UARTInstanceStruct*) chartInstanceVar);
}

extern const mxArray* sf_internal_get_sim_state_c5_UART(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  mxArray *plhs[1] = { NULL };

  mxArray *prhs[4];
  int mxError = 0;
  prhs[0] = mxCreateString("chart_simctx_raw2high");
  prhs[1] = mxCreateDoubleScalar(ssGetSFuncBlockHandle(S));
  prhs[2] = (mxArray*) get_sim_state_c5_UART((SFc5_UARTInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
  prhs[3] = (mxArray*) sf_get_sim_state_info_c5_UART();/* state var info */
  mxError = sf_mex_call_matlab(1, plhs, 4, prhs, "sfprivate");
  mxDestroyArray(prhs[0]);
  mxDestroyArray(prhs[1]);
  mxDestroyArray(prhs[2]);
  mxDestroyArray(prhs[3]);
  if (mxError || plhs[0] == NULL) {
    sf_mex_error_message("Stateflow Internal Error: \nError calling 'chart_simctx_raw2high'.\n");
  }

  return plhs[0];
}

extern void sf_internal_set_sim_state_c5_UART(SimStruct* S, const mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  mxArray *plhs[1] = { NULL };

  mxArray *prhs[3];
  int mxError = 0;
  prhs[0] = mxCreateString("chart_simctx_high2raw");
  prhs[1] = mxDuplicateArray(st);      /* high level simctx */
  prhs[2] = (mxArray*) sf_get_sim_state_info_c5_UART();/* state var info */
  mxError = sf_mex_call_matlab(1, plhs, 3, prhs, "sfprivate");
  mxDestroyArray(prhs[0]);
  mxDestroyArray(prhs[1]);
  mxDestroyArray(prhs[2]);
  if (mxError || plhs[0] == NULL) {
    sf_mex_error_message("Stateflow Internal Error: \nError calling 'chart_simctx_high2raw'.\n");
  }

  set_sim_state_c5_UART((SFc5_UARTInstanceStruct*)chartInfo->chartInstance,
                        mxDuplicateArray(plhs[0]));
  mxDestroyArray(plhs[0]);
}

static const mxArray* sf_opaque_get_sim_state_c5_UART(SimStruct* S)
{
  return sf_internal_get_sim_state_c5_UART(S);
}

static void sf_opaque_set_sim_state_c5_UART(SimStruct* S, const mxArray *st)
{
  sf_internal_set_sim_state_c5_UART(S, st);
}

static void sf_opaque_terminate_c5_UART(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc5_UARTInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_UART_optimization_info();
    }

    finalize_c5_UART((SFc5_UARTInstanceStruct*) chartInstanceVar);
    utFree((void *)chartInstanceVar);
    if (crtInfo != NULL) {
      utFree((void *)crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc5_UART((SFc5_UARTInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c5_UART(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c5_UART((SFc5_UARTInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c5_UART(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_UART_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,5);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,5,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,5,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,5);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 3, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,5,4);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,5,2);
    }

    ssSetInputPortOptimOpts(S, 4, SS_REUSABLE_AND_LOCAL);

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=2; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 4; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,5);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(3414948995U));
  ssSetChecksum1(S,(1797625994U));
  ssSetChecksum2(S,(1282528833U));
  ssSetChecksum3(S,(530984660U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c5_UART(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c5_UART(SimStruct *S)
{
  SFc5_UARTInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc5_UARTInstanceStruct *)utMalloc(sizeof
    (SFc5_UARTInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc5_UARTInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c5_UART;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c5_UART;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c5_UART;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c5_UART;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c5_UART;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c5_UART;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c5_UART;
  chartInstance->chartInfo.getSimStateInfo = sf_get_sim_state_info_c5_UART;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c5_UART;
  chartInstance->chartInfo.mdlStart = mdlStart_c5_UART;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c5_UART;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c5_UART_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c5_UART(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c5_UART(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c5_UART(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c5_UART_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
