#ifndef __c2_UART_h__
#define __c2_UART_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc2_UARTInstanceStruct
#define typedef_SFc2_UARTInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c2_sfEvent;
  uint8_T c2_tp_S1_C;
  uint8_T c2_tp_S1;
  uint8_T c2_tp_S1_R;
  uint8_T c2_tp_S1_R1;
  uint8_T c2_tp_S1_T;
  uint8_T c2_b_tp_S1;
  boolean_T c2_isStable;
  uint8_T c2_is_active_c2_UART;
  uint8_T c2_is_S1_C;
  uint8_T c2_is_active_S1_C;
  uint8_T c2_is_S1_R;
  uint8_T c2_is_active_S1_R;
  uint8_T c2_is_S1_T;
  uint8_T c2_is_active_S1_T;
  uint32_T c2_divisor;
  uint32_T c2_divisor_TX;
  uint32_T c2_divisor_RX;
  uint8_T c2_doSetSimStateSideEffects;
  const mxArray *c2_setSimStateSideEffectsInfo;
} SFc2_UARTInstanceStruct;

#endif                                 /*typedef_SFc2_UARTInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c2_UART_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c2_UART_get_check_sum(mxArray *plhs[]);
extern void c2_UART_method_dispatcher(SimStruct *S, int_T method, void *data);

#endif
