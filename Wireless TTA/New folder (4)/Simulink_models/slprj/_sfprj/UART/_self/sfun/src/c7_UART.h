#ifndef __c7_UART_h__
#define __c7_UART_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc7_UARTInstanceStruct
#define typedef_SFc7_UARTInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c7_sfEvent;
  uint8_T c7_tp_RX_OVF;
  uint8_T c7_tp_START_RX;
  uint8_T c7_tp_IDLE_RX;
  uint8_T c7_tp_EDGE_RX;
  uint8_T c7_tp_STOP_RX;
  uint8_T c7_tp_SHIFT_RX;
  uint8_T c7_tp_DONE_RX;
  boolean_T c7_isStable;
  uint8_T c7_is_active_c7_UART;
  uint8_T c7_is_c7_UART;
  uint16_T c7_RX_bit_count;
  uint8_T c7_RX_reg[8];
  boolean_T c7_RX_clear_clk_div;
  uint8_T c7_doSetSimStateSideEffects;
  const mxArray *c7_setSimStateSideEffectsInfo;
} SFc7_UARTInstanceStruct;

#endif                                 /*typedef_SFc7_UARTInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c7_UART_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c7_UART_get_check_sum(mxArray *plhs[]);
extern void c7_UART_method_dispatcher(SimStruct *S, int_T method, void *data);

#endif
