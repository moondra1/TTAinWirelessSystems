#ifndef __c3_UART_h__
#define __c3_UART_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc3_UARTInstanceStruct
#define typedef_SFc3_UARTInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c3_sfEvent;
  uint8_T c3_tp_IDLE_TX;
  uint8_T c3_tp_LOAD_TX;
  uint8_T c3_tp_SHIFT_TX;
  uint8_T c3_tp_STOP_TX;
  boolean_T c3_isStable;
  uint8_T c3_is_active_c3_UART;
  uint8_T c3_is_c3_UART;
  uint8_T c3_TX_reg[10];
  uint8_T c3_TX_reg_in[8];
  uint16_T c3_TX_bit_count;
  uint8_T c3_doSetSimStateSideEffects;
  const mxArray *c3_setSimStateSideEffectsInfo;
} SFc3_UARTInstanceStruct;

#endif                                 /*typedef_SFc3_UARTInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c3_UART_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c3_UART_get_check_sum(mxArray *plhs[]);
extern void c3_UART_method_dispatcher(SimStruct *S, int_T method, void *data);

#endif
