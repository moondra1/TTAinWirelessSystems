% $Id: solution.tex 29221 2013-05-14 22:26:42Z kyoungho $
\section{Our Solution Using the OMG DDS-based Query Implementation}

\begin{figure}[htb]
\centering
\includegraphics[width=1.0\linewidth,keepaspectratio]{highlevel_arch}
\caption{High Level Data Flow Architecture}
\label{fig:highlevel_arch}
\end{figure}
\label{sec:solution}

This section describes our OMG DDS-based solution to the DEBS 2013 Challenge
Problem.  We provide an overview of DDS and an overview of
the solution with the DDS features used in our system.  Finally, we present a
detailed description of each query implementation. 

\subsection{Overview of OMG DDS}
The OMG Data Distribution Service (DDS) specification defines a distributed
communications middleware standard that is useful in a wide variety of
environments~\cite{dds}.
The core DDS specification defines a data-centric publish-subscribe architecture for connecting anonymous information providers
with information consumers. DDS promotes loose coupling between system
components. The information consumers and providers are decoupled with respect
to time (\textit{i.e.,} they may not be present at the same time), space (\textit{i.e.,} they 
may be anywhere), flow (\textit{i.e.,} information providers must offer equivalent or better 
quality-of-service (QoS) than required by the consumers), behavior (\textit{i.e.,} business 
logic independent), platforms, and programming languages. A data provider publishes
typed data-flows, identified by names called Topics. The coupling is expressed
only in terms of topic name, data type schema, and the required and offered QoS
attributes of consumers and producers respectively. In the following subsection,
DDS concepts used in our solution are briefly introduced.  

\subsubsection{Data-Centric Architecture}
Using DDS, applications share a
\textit{global data space} (or \textit{domain}) governed by schema specified
using the XTypes~\cite{types} standard. Each topic schema is described as a structured datatype (an 
Interface Definition Language \textit{struct}). The datatype can be keyed on one 
or more fields. Each key identifies an instance (similar to a primary key in a 
database table) and DDS provides mechanisms to control the lifecycle of instances. 
Instance lifecycle supports CRUD (create, read, update, delete) operations. Complex 
delivery models can be associated with data flows by simply configuring the topic QoS. 

\subsubsection{DataWriter and DataReader}
\textit{DataWriters} and \textit{DataReaders} are end-points applications used
to write and read typed data messages (samples) from the global data space. DDS ensures that 
the end-points are compatible with respect to the topic name, datatype, and the
QoS. Creating a DataReader with a known topic and datatype implicitly crates a
\textit{subscription}, which may or may not match with a DataWriter depending upon the
QoS.

\subsubsection{Data Caching}
Data received by the DataReader is stored in a local cache. The application
accesses this data using one of two methods: \textit{read()} and
\textit{take()}; \textit{read()} leaves the data in middleware cache until it is
removed by either calling \textit{take()} or it is overwritten by the subsequent
data samples. The \textit{Resource Limits} QoS prevents the middleware caches
from growing out of bounds. 
Finally, \textit{query conditions} provide a powerful mechanism to write SQL-like
expressions on the datatype members and retrieve data samples that 
satisfy the predicates.  

\subsubsection{Content-Filtered Topics}
Content-filters refine topic subscription and filter samples that do
not match the defined application-specified predicate. The predicate is a string
encoded SQL-like expression based on the fields of the datatype. The query expression and the
parameters may change at run-time. Data filtering may be performed by the
DataWriter or DataReader.

\subsubsection{DDS Quality-of-Service}
DDS supports multiple QoS policies.  Most QoS policies have requested/offered semantics 
and can configure the dataflow between each pair of DataReader and DataWriter. We used 
the following QoS policies in our solution.  

\begin{itemize}
\item \textbf {Reliability}: controls the reliability of the data flow
between DataWriters and DataReaders. BEST\_EFFORT and RELIABLE are
two possible alternatives. BEST\_EFFORT reliability does not use any cpu/memory
resource to ensure delivery of samples. RELIABLE, on the other hand, uses an
ack/nack based protocol to provide a spectrum of reliability guarantees from
\textit{strict} (\textit{i.e.,} fully reliable) to BEST\_EFFORT. The reliability can 
be tuned using the History QoS policy. 

\item \textbf {History}: This QoS policy specifies how much data must be stored
(in DDS caches) by a DataWriter or DataReader. It controls whether DDS should deliver 
only the most recent value (\textit{i.e.,} history-depth=1), attempt to deliver all
intermediate values (\textit{i.e.,} history depth=\textit{unlimited}), or anything in
between.

\item \textbf {Time-based Filter}: High rate data flows can be throttled using
the time-based filter QoS policy. A \textit{minimum separation} configuration parameter
specifies the minimum period between two successive arrivals of data
samples. When configured, the DataReader receives only a subset of data. 


\iffalse
\item \textbf {Deadline}: This QoS policy specifies the \textit{maximum separation}
(deadline) between two successive updates for each instance. When specified on a
DataWriter, the application updates each (previously written) instance at least 
once every period. When specified on a DataReader, the middleware will notify the
application (via callback) if the DataWriter breaks the deadline contract. Note 
that DDS makes no effort to meet the deadline; it only notifies if the deadline 
is missed.
\fi

\item \textbf {Resource Limits}: This QoS policy controls the memory used to cache
samples. Maximum number of samples, maximum number of samples-per-instance and maximum 
number of instances can be specified. 
\end{itemize}

\subsection{DDS-based Architecture}

Our implementation of the four queries uses a DDS-based solution.  The key
architectural pieces of our solution, illustrated in
Figure~\ref{fig:highlevel_arch}, comprise processing blocks and a high-level
data flow.
Each processing block consists of one or more DDS DataWriters
and DataReaders to publish or subscribe to data, respectively, where the data
flow in the network is logically partitioned  by Topics. Using DDS,
application developers do not need to consider the network connections of each
data flow between DataWriters and DataReaders because it is handled by the
underlying middleware. DataReaders and DataWriters and their QoS configurations
in our system are defined in XML files that make it convenient to manage them
and separate implementation concerns of the middleware and business logic of
processing blocks.  The architectural setup is geared towards solving all four queries of the DEBS challenge problem.

We defined data structures in IDL for each dataflow as well as its
point-to-point segments.  Key attributes, such as
\textit{sensor\_id} and \textit{player\_id}, define unique \textit{instances} 
to group the updates for the same physical entity (\textit{e.g.,} a sensor).  
Through the grouping mechanisms enabled by defining a key in DDS, a processor
can easily distinguish between multiple players. The keys also help in partitioning the dataflow(s)
across multiple processors to exploit inherent parallelism in the \textit{per-player}
data streams. Each DataReader in the processing blocks has a (in-memory) cache to store 
and access the samples when needed. For instance, we utilized the cache data to 
implement the historical statistics required in some queries, such as
aggregated data for the specified number of minutes. Cached
data was also used for calculating distances between the ball and players
to find the closest player to the ball for query 2 at a certain time.


In our solution, DDS QoS has been utilized for obtaining benefits of
efficient implementation as well as satisfying the real-time requirements of the
DEBS Challenge Problem.  Since the queries have different characteristics in
terms of the required update rates and processing data sizes, we needed to apply
the appropriate QoS configurations for each query according to its
characteristics.  We set BEST\_EFFORT as \textit{Reliability} QoS for periodic
queries like query 1 and 3, and RELIABILITY for aperiodic queries like query 2
and 4.  Consequently, our system can guarantee faster update rates for periodic
queries while assuring accuracy for aperiodic queries.  Additionally, we
utilized \textit{Time-based Filter} QoS to manage resources efficiently and guarantee a
query's requirement by configuring different minimum separation values.  The
details on how each query is implemented is presented in the rest of this
section.

\subsection{Implementing Query 1}
The \textit{SensorDataGenerator} processing block publishes sensor data in a
real-time manner.  The published data is sent to multiple processing blocks to
be processed in parallel.  In the case of query 1, a processor named
\textit{AverageProcessor} merges the data of two sensors attached to
a player by averaging the sensor values.  In the processing block, ball
data is filtered because only the player's information is needed for query 1.
We utilized \textit{Time-based Filter} QoS supported by DDS to efficiently
manage constrained resources and follow the required update interval of
50Hz. The next processing block for Query 1 is \textit{CurrentRunningAnalyzer}
in which a player's running state, speed and distance are calculated, and the
results are published to clients and the \textit{AggregateRunningAnalyzer}.
Finally, in the \textit{AggregateRunningAnalyzer}, the accumulated values of distance and
speed categorized by a player's running states are streamed as a result, which
is divided into multiple streams for the given time windows.

When a new sample arrives at the DataReader of a processor, a callback
function, \textit{on\_data\_available()} provided by the DataReader's listener interface is
invoked.  Since the processor of the query computes required statistics when a
new sample arrives, we implement our business logic in the call back function.

By using \textit{x} and \textit{y} coordinates of a previous sample kept in a
processor, distance is calculated.  The timestamp of the previous sample is
designated as the starting time of the calculation and the timestamp of the
current sample is assigned as the ending time.  The speed is obtained by the
velocity value of the  current sample and the unit of speed needs to be
converted from $\mu$m/s to km/h.

However, the raw sensor data obtained from a real soccer game is not always accurate.
Erroneous distance or speed
values are possibly attained based on incorrect sensor data.  To avoid the
problem, we compare three speed values at the current timestamp obtained using
three different ways: velocity of the current sample, velocity of the previous
sample, and distance and time of the current sample.  If outliers exist, these
are removed by a filtering process.

\begin{figure}[htb]
\centering
\includegraphics[width=0.8\linewidth,keepaspectratio]{q1_filtering}
\caption{Comparing Player Distance with Filtering Outliers}
\label{fig:q1_filtering}
\end{figure}

Figure~\ref{fig:q1_filtering} presents distance in millimeters between
each timestamp of a player and shows improvement of data accuracy by applying
the filtering process.   If differential between the speed values determined by
different sources of sensor data at the same timestamp is larger than 20 km/h,
which is configurable for different physical context, it can be marked as a
sensor data error and needs to be fixed to a proper value from a correct
source.   Once speed and distance values are accurately evaluated, a player's
intensity is classified by the speed value.   If all of the required information
is successfully calculated and stored, we call the write operation to let the
middleware publish the outcome for clients.  

For aggregate running analysis, we maintain a separate processor called
\textit{AggregateRunningAnalyzer}, which subscribes to the result stream of the
current running analysis. The aim of the query for aggregate running
analysis is to accumulate the time duration of classified intensities of players
for 4 different time windows.  Since the query incorporates the four time
windows, the processor publishes four result streams by different data writers.  

The main challenge of this query is managing historical sample data
effectively.   We applied an incremental algorithm which avoids redundant
computation that will occur when a new sample arrives.  The algorithm keeps
adding duration values of a new sample to a cache in the processor, and
subtracting the time duration values of samples from the cache which are no
longer included in the specified time windows because of newly arrived samples. 


\subsection{Implementing Query 2}
% query 2
The statistics needed for query 2 are ball possession of players and teams. To
acquire these statistics, we created two processing blocks:
\textit{PlayerBallPossessionProcessor} and
\textit{TeamBallPossessionProcessor}.  In the
\textit{PlayerBallPossessionProcessor}, the sensor data of players and balls are
subscribed from \textit{SensorDataGenerator} to find a ball possessing player by
computing the distance between players and the ball, and the ball's
acceleration.  The detailed state diagram and algorithm for the player ball
possession is described below and shown in Figure~\ref{fig:q2_state_chart}.

\begin{figure}[htb]
\centering
\includegraphics[width=0.8\linewidth,keepaspectratio]{q2_state_chart}
\caption{State Diagram for PlayerBallPossessionProcessor}
\label{fig:q2_state_chart}
\end{figure}

According to the state diagram
described in Figure~\ref{fig:q2_state_chart}, possession for each player is
calculated as follows.  First, all the players are in the \textit{initial state}
which indicates the player does not possess the ball. The player closest to the ball
within a 0.3 meter radius at a certain timestamp is calculated using the
\textit{x} and \textit{y} coordinates of the ball and the players' data. This
player has possession of the ball starting at that timestamp and is in
\textit{possession state}.
% 04/20: Kyoungho, what DDS feature helps in this?

The player gets his last hit while entering the  states below as follows:
\begin{enumerate}
\item \textbf{Hit}: when the distance to the ball is over two
meters. When the distance to the ball from the player who possessed the ball is
over the limit, it can be assumed that the player passed or shot the ball with
the last touch.   
\item \textbf{Intercepted}: if the player that possessed the ball previously is
still in the \textit{possession state} and is not able to reach the
\textit{hit state}, and the closest player to the ball is changed, we can
interpret this to mean that the ball has been intercepted by another player.   
\item \textbf{GameStopped}: the game is \textit{stopped} (including when the
ball leaves the field) when the \textit{x} and \textit{y} coordinates of the
ball are not in the field or when the ball's sensor timestamp is not in the time
range of first or second half.
\end{enumerate}

Moreover, if the ball's height exceeds 1 meter, then subsequent players that may
be closest are ignored, i.e, if the ball is in flight, no one has possession of
the ball till its height is below 1 meter.  For a team possession, we have a
separate  processor called \textit{TeamBallPossession}, which subscribes to the
result stream of the player ball possession. We are calculating it by storing
the possession time of each player at every timestamp into a double-ended
queue. To display the team possession at the current timestamp, the 
possession for each player on the team is aggregated from the current timestamp,
which is located at the end of the appropriate queue  with the previous
timestamp.A team's possession
percentage is calculated accordingly.
As the query incorporates the four time windows, the processor publishes
four result streams by different data writers. 

\subsection{Implementing Query 3}
% query 3
\iffalse
The aim of query 3 is to obtain heat map statistics which shows how long a
player stays in a specific section of the field.  Because each player's foot has
a sensor, averaged \textit{x} and \textit{y} coordinate values for a player
are required to to find a center of mass.  Therefore, the output stream of
\textit{AverageProcessor} is used as the input stream of the query 3
processor.  The update interval of query 3 is one second. We use the
DDS \textit{Time-based Filter} QoS is used to control the interval of events.   
\fi

Query 3 is perhaps the most straightforward query in its logic. However, the
amount of data required to be kept in the system to accurately calculate the
results quickly grows and needs to be handled properly. To do this, we have a
receiving node, called  \textit{AccumulatingHeatMapProcesser} (Accumulator),
processing player positions from the \textit{AverageProcessor} at 50Hz. This
Accumulator reads in the location data for each player and builds a logical data
structure of 6,401 integers: the total
number of samples that were recorded for that second, and then values for each
of the 64x100 row/column cell value.

\iffalse
The proper cell value is
determined by the following two formulas.  These formulas allow the appropriate
64x100 cell to be determined from the sensor data.   

\begin{eqnarray}
  \label{eq:1}
X-Row&=&(SensorXdata \% 530),\nonumber \\ 
Y-Row&=&(SensorData-Y-value + 33940) \% 678 \nonumber
\end{eqnarray}
% 04/20: Kyoungho, in the above is SensorData-Y-value a variable name or Y and
% value are subtracted from sensordata?
\fi

Our solution incurs slightly inaccurate approximations on the 8x13 grid of
cells because the 100 rows of the 64x100 cell matrix cannot be divided evenly
into a 8x13 matrix. The result is that our 8x13 matrix cells are shorter
by 1/2 of a 64x100 cell for each of the 8 cells. This was made to simplify the data processing and
decrease the data transmission requirements. This can be easily updated if exact
precision is required. This solution allows the same
algorithm to determine the appropriate result stream for all 4 grid sizes by
accumulating the 4 cells from the next more accurate data for each required cell
of the result stream. 

Sample data per player is then subscribed to by the \textit{HeatMapProcessor} 
processor block every second. This processor block acts as an accumulator and maintains the data which will be used for
producing the 16 result streams for each player.  It uses the 64x100 grid to
mathematically generate, by adding the appropriate  2x2 cells from the next
larger cell matrix to obtain the 32x50, 16x25, and 8x13 grid of cells when that
information is needed. This node could be configured in a production system to
actually be a single node per player, but we have found that it works well as a
single node for the data provided.    

The processor block keeps track of this data by building a
running tally of all 16 desired output streams: 4 time windows * 4 grid
sizes. By doing this, the node has to compute only two functions per
stream. First, it has to subtract the older data that no longer fits into the
appropriate time window from the running tallies in the 12 streams where the
entire game data is not desired.  Then, the processor block has to add the
newest one second worth of accumulated data to each of the streams' tallies.

There are two ways to implement different time windows utilizing DDS
middleware. First, each processor keeps history values in the processor's cache
and calculates a proper value using the history values according to the size of
the time window. Second, the history values can be kept and managed in the
middleware cache by making use of \textit{History} QoS and \textit{Resource
  Limits} QoS. In query 3, we adopted the latter approach. We approximated the
size of middleware cache in our configuration of the QoS such as history depth
and maximum number of samples according to arrival rate of input streams. In the
case of query 3, the arrival rate is 1 second and requires a minimum cache
memory storage of 60 samples for 1 minute, 300 samples for 5 minutes, and 600 samples for 10 minutes.
However, history samples for 1 minute and 5 minutes
are actually overlapped with history samples for 10 minutes, so we allocate
memory for history samples for 20 minutes and other time windows reuse the same
memory, which is a tradeoff and an optimization.  

\subsection{Implementing Query 4}
% query 4
The query 4's goal is to detect events of a player shooting in the direction of
the goal. Shots on goal occur when a player hits the ball (i.e. his
foot is close to the ball and the ball's acceleration is greater than 55
m/$s^2$) and the projection of the ball's movement would lead it within one of the two
goal areas.
Since detection of hit events is already carried out to compute query 2,
\textit{ShotsOnGoalProcessor} subscribes to a data-stream published by by
\textit{PlayerBallPossessionProcessor}. Similarly, the filtered data-stream
reporting data about the ball currently used in the game is used instead of
raw sensor data.

\begin{figure}[htb]
\centering
\includegraphics[width=0.8\linewidth,keepaspectratio]{q4_state_chart}
\caption{State Diagram for ShotsOnGoalProcessor}
\label{fig:q4_state_chart}
\end{figure}

Detection of shots on goal is implemented using a simple finite-state machine
represented in Figure~\ref{fig:q4_state_chart}. The processor uses asynchronous
notifications from DDS to wait for input data to be available. The latest
hit event and ball position samples received are stored by the processor between
successive notifications. Whenever inputs are updated, the state-machine is
updated. Once a hit event is notified, the processor must wait for the ball to
have moved sufficiently farther from the initial location of the hit so that its
motion can be projected more accurately using the equations for projectile motion.

Whenever the processor detects a valid projection on the goal, it will publish a
result sample containing the shooting player's id and the ball's position at the
time of detection. In this implementation we used simple 2-dimensional
equations for these computations, which seemed to yield accurate enough results. 
Nevertheless, it is trivial to replace the projection calculating algorithm if results are found
to be not satisfactory. The results are produced until the projection is found
to be missing the goal with updated ball position information, in which case
the processor resets its state.

The state-machine is also transitioned back to the initial state every
time the ball ends up outside the field boundaries or a new hit event from a
different player is received. Depending on the current values of the inputs,
multiple state transitions may occur with a single state update. This enables the processor to
consume available input more efficiently and avoid unneccessary waiting when the
ball is already in a valid position to compute its projection toward the
goal. For example, in the case of a new hit event, the processor may execute
a maximum of 4 consecutive state transitions within a single state update, if
the ball is immediately found within valid range for computing the projection of
the new hit event and the processor determines the ball will not hit the goal.


