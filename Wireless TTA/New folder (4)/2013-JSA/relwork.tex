% $Id: relwork.tex 29999 2013-10-22 02:50:26Z kyoungho $
\section{Related Work}
\label{sec:relwork}

Prior work in the literature of high availability solutions, VM placement strategies, and resource monitoring are related to the three research contributions we offer in this paper. In this section, we present a comparative analysis of the literature and how our solutions fit in this body of knowledge.

% folks, I reorganized the sections.

\subsection{High Availability Solutions for Virtual Machines}
To ensure high-availability, we propose a fault-tolerant solution that is based on the 
continuous checkpointing technique developed for the Xen hypervisor called
Remus~\cite{Remus_OSDI:08}.  We discuss the details and shortcomings of Remus in Section~\ref{sec:remus_overview}.  

Several other high availability solutions for virtual machines are reported in the literature. VMware fault-tolerance~\cite{DBLP:journals/sigops/ScalesNV10} runs primary and backup VMs in
lock-step using deterministic replay.
This keeps both the VMs in sync but it requires execution at both the VMs and
needs high quality network connections.  In contrast, our model focuses on a
primary-backup scheme for VM replication that does not require execution on all
replica VMs.

Kemari~\cite{kemari2008validating} is another approach that uses
both lock-stepping and continuous check-pointing.  It synchronizes
primary and secondary VMs just before the primary VM has to send an event to
devices, such as storage and networks.  At this point, the primary VM pauses and
Kemari updates the state of the secondary VM to the current state of primary VM.
Thus, VMs are synchronized with lower complexity than lock-stepping.
External buffering mechanisms are used to improve the output latency over continuous check-pointing. 
However, we opted for Remus since it is a mature solution compared to 
Kemari.

Another important work on high availability is HydraVM~\cite{HPL-2011-24}. It is
a storage-based, memory-efficient high availability solution which does not need
passive memory reservation for backups.  It uses incremental check-pointing like
Remus~\cite{Remus_OSDI:08}, but it maintains a complete recent image of VM in shared storage instead
of memory replication.  Thus, it claims to reduce hardware costs for providing high
availability support and provide greater flexibility as recovery can happen on
any physical host having access to shared storage. However, the software is not 
open-source or commercially available.

\subsection{Aproaches to Virtual Machine Placement}



Virtual machine placement on physical hosts in the cloud critically affects the 
performance of the application hosted on the VMs. Even when the individual VMs have
a share of the physical resources, effects of context switching, network performance and other systemic effects~\cite{Zhang:2013:CCP:2465351.2465388, pu2010understanding,
tickoo2010modeling, nathuji2010q} can adversely impact the performance
of the VM.
This is particularly important when high availability solutions based on
replication must also consider performance as is the case in our research.
% SS: Not clear what autonomy means here - does it mean that VM can autonomously change its own placement when it detects a performance degradation below a threshold? Looks like what we mean is "flexibility" to allow users to specify placement policies and algorithms.
Naturally, more autonomy in VM placement is desirable. 

The approach proposed in~\cite{hyser2007autonomic} is closely related to the scheme we propose in this paper. 
The authors present  an autonomic controller that dynamically assigns VMs to physical hosts according
to policies specified by the user.  While the scheme we propose also allows users to specify placement policies and algorithms, 
we dynamically allocate the VMs in the context of a fault-tolerant cloud computing architecture that ensures high-availability solutions. 

Lee et al.~\cite{lee2011validating} investigated VM consolidation heuristics
to understand how VMs perform when they are
co-located on the same host machine.  They also explored how the resource 
demands such as CPU, memory, and network bandwidth are handled when consolidated. The work
in~\cite{beloglazov2010energy} proposed a modified Best Fit Decreasing (BFD)
algorithm as a VM reallocation heuristic for efficient resource management.
The evaluation in the paper showed that the suggested heuristics minimize energy
consumption while providing improved QoS.  Our work
may benefit from these prior works and we are additionally concerned with placing
replicas in a way that  applications continues to obtain
the desired QoS after a failover.

\subsection{Resource Monitoring in Large Distributed Systems}
Contemporary compute clusters and grids have provided  special capabilities to
monitor the distributed systems via frameworks, such as
Ganglia~\cite{massie2004ganglia} and Nagios~\cite{barth2008nagios}.  
According to~\cite{foster2008cloud}, one of the distinctions between grids and
cloud is that cloud resources also include virtualized resources.   Thus, the
grid- and cluster-based frameworks are structured primarily to monitor physical
resources only, and not a mix of virtualized and physical resources.   Even
though some of these tools have been enhanced to work in the cloud, \emph{e.g.},
virtual machine monitoring in
Nagios\footnote[1]{\url{http://people.redhat.com/~rjones/nagios-virt}} and
customized scripts used in Ganglia, they still do not focus on the timeliness
and reliability of the dissemination of monitored data that is essential to
support application QoS in the cloud.  A work that comes closest to
ours,~\cite{huang2007study}, provides a comparative study of publish/subscribe
middleware for real-time grid monitoring in terms of real-time performance and
scalability.  While this work also uses publish/subscribe for resource
monitoring, it is done in the context of grid and hence incurs the same
limitations.

In other recent works, ~\cite{han2011virtual} presents a virtual resource
monitoring model and ~\cite{de2011toward} discusses a cloud monitoring
architecture for private clouds.  Although these prior works describe cloud
monitoring systems and architectures, they do not provide experimental
performance results of their models for properties such as system overhead and
response time.  Consequently, we are unable to determine their relevance to
support timely dissemination of resource information and hence their ability to
host mission-critical applications in the cloud.   Latency results using RESTful
services for resource monitoring are described in~\cite{guinard2010resource},
however, they are not able to support diverse and differentiated service levels
for cloud clients we are able to provide.

\subsection{Comparative Analysis}
Although there are several findings in the literature that relate to our three contributions, 
none of these approaches offer a holistic framework that can be used in a cloud infrastructure.
Consequently, the combined effect of individual solutions has not been
investigated.  Our work is a step in the direction of fulfilling this void.
Integrating the three different approaches is not straightforward and requires
good design decisions, which we have demonstrated with our work and presented in
the remainder of this paper.
