% $Id: experiment.tex 30013 2013-10-23 18:32:59Z shekhars $
\section{Experimental Results and Case Study}
\label{sec:experiment}

In this section we present results  to show that our
solution can seamlessly and effectively leverage existing solutions
for fault tolerance in the cloud.   

\subsection{Rationale for Experiments}
\label{sec:exp_rationale}
Our high-availability solution for cloud-hosted soft real-time
applications leverages existing VM-based solutions, such as the one
provided by Remus.  Moreover, it is also possible that the application
running inside the VM itself may provide its own application-level
fault tolerance.  Thus, it is important for us to validate that our
high availability solution can work seamlessly and effectively in the
context of existing solutions.

Moreover, since we provide a pluggable framework for replica
placement, we must validate our approach in the context of a concrete
placement algorithm that can be plugged into our framework.  To that
end, we have developed a concrete placement algorithm, which we
describe below and used it in the evaluations.  

\subsection{Representative Applications and Evaluation Testbed}

To validate both the claims: (a) support for high-availability soft
real-time applications, and (b) seamless co-existence with other
cloud-based solutions, we have used two representative soft real-time
applications.  For the first set of validations, we have used an
existing benchmark application that has the characteristics of a real-time
application~\cite{dds_benchmark}.   To demonstrate how our
solution can co-exist with other solutions, we used a \textit{word
  count} application that provides its own application-level fault-tolerance.
  We show how our solution can co-exist with
different fault-tolerance solutions.

Our private cloud infrastructure for both the experiments we conducted comprises
a cluster of 20 rack servers, and Gigabit switches.  The cloud infrastructure is
operated using OpenNebula 3.0 with shared file systems using NFS (Network File
System) for distributing virtual machine images. Table~\ref{table:nodedetails} provides 
the configuration of each rack server used as a clustered node.
\begin{table}[htb]
\caption{Hardware and Software specification of Cluster Nodes}
\begin{center}
    \begin{tabular}{  c ||  p{6cm} } 
    \hline
    Processor &  2.1 GHz Opteron  \\ \hline
    Number of CPU cores & 12   \\ \hline
    Memory & 32 GB  \\ \hline
    Hard disk & 8 TB  \\ \hline
    Operating System & Ubuntu 10.04 64-bit  \\ \hline
    Hypervisor & Xen 4.1.2  \\ \hline
    Guest virtualization mode & Para  \\ \hline
    \end{tabular}
\label{table:nodedetails}
\end{center}
\end{table}

Our guest domains run Ubuntu 11.10 32-bit as operating
systems, and each guest domain has 4 virtual CPUs and 4GB of RAM. 

\subsection{A Concrete VM Placement Algorithm}
\label{sec:ilp}
Our solution provides a framework that enables plugging in different
user-supplied VM placement algorithms.  We expect that our framework
will compute replica placement decisions in an online manner in
contrast to making offline decisions.  We now present an instance of
VM replica placement algorithm we have developed.  We have formulated
it as an Integer Linear Programming (ILP) problem.

In our ILP formulation we assume that a data center comprises multiple hosts.
Each host can in turn consist of multiple VMs.  We also account for the
resource utilizations of the physical host as well as the VMs on each
host.  Furthermore, not only do we account for CPU utilizations but
also memory and network bandwidth usage.  All of these resources are
accounted for in determining the placement of the replicas because on
a failover we expect our applications to continue to receive their
desired QoS properties.  Table~\ref{table:notation} describes the
variables used in our ILP formulation.   

\begin{table}[htb]
\caption{Notation and Definition of the ILP Formulation}
\begin{center}
    \begin{tabular}{  c ||  p{10cm} } 
    \hline
    \textbf{Notation} & \multicolumn{1}{c}{\textbf{Definition}} \\ \hline\hline
    $x_{ij}$ & Boolean value to determine the $i^{\text{th}}$ VM to the $j^{\text{th}}$ physical host mapping  \\ \hline
    $x^\prime_{ij}$ & Boolean value to determine the replication of the $i^{\text{th}}$ VM to the $j^{\text{th}}$ physical host mapping \\ \hline
    $y_{j}$ & Boolean value to determine usage of the physical host $j$ \\ \hline
    $c_{i}$ & CPU usage of the $i^{\text{th}}$ VM \\ \hline
    $c^\prime_{i}$ & CPU usage of the $i^{\text{th}}$ VM's replica \\ \hline
    $m_{i}$ & Memory usage of the $i^{\text{th}}$ VM \\ \hline
    $m^\prime_{i}$ & Memory usage of the $i^{\text{th}}$ VM's replica \\ \hline
    $b_{i}$ & Network bandwidth usage of the $i^{\text{th}}$ VM \\ \hline
    $b^\prime_{i}$ & Network bandwidth usage of the $i^{\text{th}}$ VM's replica  \\ \hline
    $C_{j}$ & CPU capacity of the $j^{\text{th}}$ physical host\\ \hline
    $M_{j}$ & Memory capacity of the $j^{\text{th}}$ physical host\\ \hline
    $B_{j}$ & Network bandwidth of the $j^{\text{th}}$ physical host\\ \hline
    \end{tabular}
\label{table:notation}
\end{center}
\end{table}

We now present the ILP problem formulation shown below with the defined
constraints that need to be satisfied to find an optimal allocation of VM
replicas.  The objective function of the problem is to minimize the number of
physical hosts by satisfying the requested resource requirements of VMs and
their replicas.   Constraints (2) and (3) ensure every VM and VM's replica is
deployed in a physical host.  Constraints (4), (5), (6) guarantee that
the total capacity of CPU, memory, and network bandwidth of deployed
VMs and VMs' replicas are packed into an assigned physical host,
respectively.  Constraint (7) checks that a VM and its replica is not
deployed in the same physical host since the physical host may become
a single point of failure, which must be prevented. 

\begin{alignat}{3}
	\text{minimize }   & \sum_{j=1}^m y_j  \\ 
	\text{subject to } 
	& \sum_{j=1}^m x_{ij} = 1 & \forall i  \\
	& \sum_{j=1}^m x^\prime_{ij} = 1 & \forall i\\
	& \sum_{i=1}^n c_{i} x_{ij} + \sum_{i=1}^n c^\prime_{i} x^\prime_{ij} \leq C_j y_j  &  \forall j \\
	& \sum_{i=1}^n m_{i} x_{ij} + \sum_{i=1}^n m^\prime_{i} x^\prime_{ij} \leq M_j y_j  &  \forall j\\
	& \sum_{i=1}^n b_{i} x_{ij} + \sum_{i=1}^n b^\prime_{i} x^\prime_{ij} \leq B_j y_j  &  \forall j\\
	& \sum_{i=1}^n x_{ij} + \sum_{i=1}^n x^\prime_{ij} = 1 & \forall j\\
	& x_{ij} = \lbrace0, 1\rbrace, \ x^\prime_{ij} = \lbrace0, 1\rbrace, \ y_{j} = \lbrace0, 1\rbrace
\end{alignat}

\subsection{Experiment 1: Measuring the Impact on Latency for Soft Real-time Applications}

To validate our high-availability solution including the VM replica placement
algorithm, we used the RTI DDS Connext latency benchmark.  \footnote{
For the experiment, our application is using DDS and is not to be confused with
our DDS-based resource usage dissemination solution.  In our solution,
the DDS approach is part of the middleware whereas the application
resides in a VM.}  RTI Connext is an implementation of the OMG DDS
standard~\cite{OMG-DDS:07}.  The RTI Connext benchmark comprises code
to evaluate the latency of DDS applications, and the test code contains 
both the publisher and the subscriber.

Our purpose in using this benchmark was to validate the impact of our high-
availability solution and replica VM placement decisions on the latency of DDS
applications.  For this purpose, the DDS application was deployed inside a VM.
We compare the performance between an optimally placed VM
replica using our algorithm described in Section~\ref{sec:ilp} and a
potentially worse case scenario resulting from a randomly deployed VM.
In the experiment, average latency and standard deviation of latency,
which is a measure of the jitter, are compared for different settings
of Remus and VM placement.  Since a DDS application is a one
directional flow from a publisher to a subscriber, the latency
measurement is estimated as half of the round-trip time which is
measured at a publisher.  In each experimental run, 10,000 samples of
stored data in the defined byte sizes in the table are sent from a
publisher to a subscriber.  We also compare the performance when no
high-availability solution is used.  The rationale is to gain insights
into the overhead imposed by the high-availability solution.

Figure~\ref{fig:stdev_mean} shows how our Remus-based high-availability solution
along with the effective VM placement affects latency of real-time applications.
The measurements from the experimental results for the case of \textit{Without
  Remus}, where VM is not replicated, shows consistent range of standard
deviation and average of latency compared to the case of \textit{Remus with
  Efficient Placement}.  When Remus is used, average latency does not increase
significantly, however, a higher fluctuation of latency is observed by measuring
standard deviation values between both cases.  From the results we can
conclude that the state replication overhead from Remus incurs a wider
range of latency fluctuations.

\begin{figure}[htb] 
\centering
\includegraphics[width=0.8\linewidth,keepaspectratio]{Stdev_Mean}
\caption{Latency Performance Test for Remus and Effective Placement}
\label{fig:stdev_mean}
\end{figure}

\iffalse
\begin{table}[htb]
\begin{center}
\caption{Latency Performance Test for Remus and Effective Placement} % title of Table
\label{table:latency} % is used to refer this table in the text

\begin{tabular}{ | >{\centering} p{1cm} || >{\centering} p{1.6cm}  |  >{\centering}p{1.6cm}  || >{\centering}p{1.6cm}  | >{\centering}p{1.6cm} || >{\centering}p{1.6cm} | p{1.6cm} <{\centering}| }
  \hline
  \multicolumn{1}{ |p{1cm}|| } { } &
  \multicolumn{2}{ >{\centering}p{3.5cm}|| } { \textbf{Without Remus}} &
  \multicolumn{2}{ >{\centering}p{3.5cm}|| } { \textbf{Remus with     
Efficient Placement}} &
  \multicolumn{2}{ >{\centering}p{3.5cm}| }{ \textbf{Remus with Worse Case 
Placement}} \\
  \hline
  bytes & stdev($\mu$s) &  avg($\mu$s) & stdev($\mu$s)& avg($\mu$s) & stdev($\mu$s) & avg($\mu$s) \\
  \hline
  16 & 21.6 &  282.2 & 244.4 & 315.5 & 877.5 & 372.9 \\
  32 & 16.4 &  280.4 & 140.7 & 310.2  & 1531.9 & 393.9 \\
  64 & 17.7 &  291.4 & 157.3 & 338.7 & 619.5 & 369.9 \\
  128 & 21.6 &  309.0 & 121.4 & 326.2 & 803.6 & 345.5 \\
  256 & 16.6 &  323.2 & 124.2 & 361.0 & 934.5 & 377.2 \\
  512 & 7.5 &  331.6 & 173.4 & 367.1 & 982.8 & 388.0 \\
  1024 & 8.6 &  348.1 & 127.6 & 367.2 & 731.4 & 399.4 \\
  2048 & 39.6 & 376.5  & 144.1 & 426.6 & 922.7 & 496.6 \\
  4096 & 30.7 &  608.1 & 151.2 & 616.2 & 2249.7 & 719.3 \\
  8192 & 28.7 & 758.4  & 169.0 & 779.7 & 1489.2 & 904.8 \\

  \hline
\end{tabular}

\end{center}
\end{table}
\fi

However, the key observation is that significantly wider range of
latency fluctuations are observed in the standard deviation of latency
in \textit{Remus with Worst Case Placement}.  On the contrary, the
jitter is much more bounded using our placement algorithm.  our
framework guarantees that the appropriate number of VMs are deployed
in physical machines by following the defined resource constraints so
that contention for resources between VMs does not occur even though a VM
or a physical machine has crashed.  However, if a VM and its replica
is randomly placed without any constraints, unexpected latency
increases for applications running on the VM could occur.  The resulting
values of latency's standard deviation in \textit{Remus with Worst
  Case Placement} demonstrate how the random VM placement negatively
influences timeliness properties of applications.

\subsection{Experiment 2: Validating Co-existence of High Availability Solutions}
Often times the applications or their software platforms support their
own fault-tolerance and high-availability solutions.  The purpose of
this experiment is to test whether it is possible for both our
Remus-based high availability solution and the third party solution could
co-exist.  

To ascertain these claims, we developed a \textit{word count} example
implemented in C++ using OMG DDS.  The application supports its own
fault tolerance using OMG DDS QoS configurations as follows.  OMG DDS
supports a QoS configuration called \emph{Ownership Strength,} which
can be used as a fault tolerance solution by a DDS pub/sub
application.  For example, the application can create redundant
publishers in the form of multiple data writers that publish the same
topic that a subscriber is interested in.  Using the
\textit{OWNERSHIP\_STRENGTH} configuration, the DDS
application can dictate who the primary and backup publishers are.
Thus, a subscriber receives the topics only from the publisher with
the highest strength.  When a failure occurs, a data reader (which is
a DDS entity belonging to a subscriber) automatically fails over to
receive its subscription from a data writer having the next highest
strength among the replica data writers.  

Although such a fault-tolerant solution can be realized using the
ownership QoS, there is no equivalent method in DDS if a failure
occurs at the source of events such as a node that aggregates multiple
sensors data and a node reading a local file stream as a source of
events.  In other words, although the DDS ownership QoS takes care of
replicating the data writers and organizing them according to the
ownership strength, if these data writers are deployed in VMs of a
cloud data center, they will benefit from the replica VM placement
strategy provided by our approach thereby requiring the two solutions
to co-exist.

\begin{figure}[htb] 
\centering
\includegraphics[width=0.7\linewidth,keepaspectratio]{Word_Count}
\caption{Example of Real-time Data Processing: Word Count}
\label{fig:word_count}
\end{figure}

To experiment with such a scenario and examine the performance
overhead as well as message missed ratio (\emph{i.e.}, lost messages
during failover), we developed a DDS-based ``word count'' real-time
streaming application.  The system integrates both the high availability
solutions.  Figure~\ref{fig:word_count} shows the deployment of the
\textit{word count} application running on the highly available
system.  Four VMs are employed to execute the example application.
VM1 runs a process to read input sentences and publishes a sentence to
the next processes.  We call the process running on the VM1 as the
\textit{WordReader}.  In the next set of processes, a sentence is
split into words.  These processes are called \textit{WordNormalizer}.
We place two VMs for the normalizing process and each data writer's
Ownership QoS is configured with the exclusive connection to a data
reader and the data writer in VM3 is set to the primary with higher
strength.  Once the sentences get split, words are published to the
next process called the \textit{WordCounter}, where finally the words
are counted.  In the example, we can duplicate processes for
\textit{WordNormalizer} and \textit{WordCounter} as they process
incoming events, but a process for \textit{WordReader} cannot be
replicated by having multiple data writers in different physical nodes
as the process uses a local storage as a input source.  In this case,
our VM-based high availability solution is adopted.

\begin{table}[htb]
\begin{center}
\caption{DDS QoS Configurations for the Word Count Example} % title of Table
\label{table:qos} % is used to refer this table in the text
{\scriptsize
\begin{tabulary}{\columnwidth}{|L|L|}\hline
\multicolumn{1}{|c|}{\textbf{DDS QoS Policy}} & 
\multicolumn{1}{|c|}{\textbf{Value}}\\ \hline \hline
\textbf{Data Reader} &  \\
Reliability & Reliable  \\ 
History & Keep All  \\ 
Ownership   & Exclusive  \\ 
Deadline  & 10 milliseconds  \\ 
\hline
\textbf{Data Writer} &  \\
Reliability & Reliable  \\ 
Reliability - Max Blocking Time & 5 seconds  \\ 
History & Keep All  \\ 
Resource Limits - Max Samples & 32 \\ 
Ownership   & Exclusive  \\ 
Deadline  & 10 milliseconds  \\ 
\hline
\textbf{RTPS Reliable Reader} &  \\
MIN Heartbeat Response Delay & 0 seconds \\ 
MAX Heartbeat Response Delay & 0 seconds  \\ 
\hline
\textbf{RTPS Reliable Writer} &  \\
Low Watermark & 5  \\ 
High Watermark & 15  \\ 
Heartbeat Period & 10 milliseconds \\ 
Fast Heartbeat Period   & 10 milliseconds  \\ 
Late Joiner Heartbeat Period   & 10 milliseconds  \\ 
MIN NACK Response Delay   & 0 seconds  \\ 
MIN Send Window Size   & 32  \\ 
MAX Send Window Size   & 32  \\ 

% [0.5ex] % [1ex] adds vertical space
\hline
\end{tabulary}
}
\end{center}
\end{table}

Table~\ref{table:qos} describes the DDS QoS configurations used for our
\textit{word count} application. The throughput and latency of an application
can be varied by different DDS QoS configurations.  Therefore,  our
configurations in the table can provide a reasonable understanding of our
performance of experiments described below.  In the \textit{word count}
application, since consistent word counting information is critical,
\textit{reliable} rather than \textit{best effort} is designated as the
Reliability QoS.  For reliable communication, history samples are all kept in
the reader's and writer's queues.  As the Ownership QoS is set to
\textit{exclusive}, only one primary data writer among multiple data
writers can publish samples to a data reader.  If a sample has not
arrived in 10 milliseconds, a deadline missing event occurs and the
primary data writer is changed to the one which has the next highest
ownership strength.  

The results of experimental evaluations are presented to verify performance and
failover overhead of our Remus-based solution in conjunction with DDS
Ownership QoS.  We experimented six cases shown in the
Figure~\ref{fig:experiments} to understand latency and failover
overhead of running Remus and DDS Ownership QoS for the word count
real-time application.  The experimental cases represent the combinatorial
fail over cases in an environment selectively exploiting Remus and
DDS Ownership QoS.

\begin{figure}[htb] 
\centering
\includegraphics[width=0.9\linewidth,keepaspectratio]{Experiments}
\caption{Experiments for the Case Study}
\label{fig:experiments}
\end{figure}

Figure~\ref{fig:remus_latency_overhead} depicts the results of Experiment E1 and
E2 from Figure~\ref{fig:experiments}.  Both the experiments have Ownership QoS
setup as described above.  Experiment E2 additionally has VM1 running the
\textit{WordReader} process, which is replicated to VM1' whose placement
decision is made by our algorithm.  The virtual machine VM1 is replicated using
Remus high availability solution with the replication interval set to
40 milliseconds for all the experiments.  This interval is also
visibly the lowest possible latency for all the experiments, which has
ongoing Remus replication.  All the experiments depicted in
Figure~\ref{fig:experiments} involved a transfer of 8,000 samples from
\textit{WordReader} process on VM1 to \textit{WordCount} process
running on VM4.  In the experiments E1 and E2, \textit{WordNormalizer}
processes run on VM2 and VM3 and incur the overhead of DDS Ownership
QoS. In addition, experiment E2 has the overhead of Remus replication.


\begin{figure}[htb] 
\centering
\includegraphics[width=0.8\linewidth,keepaspectratio]{Remus_Latency_Overhead}
\caption{Latency Performance Impact of Remus Replication}
\label{fig:remus_latency_overhead}
\end{figure}

The graph in Figure~\ref{fig:remus_latency_overhead} is a plot of average
latency for each of the 80 samples set for a total of 8,000 samples
transfer.  For experiment E1 with no Remus replication, it was
observed that the latency fluctuated within a range depending upon the
queue size of \textit{WordCounter} and each of \textit{WordNormalizer}
processes.  For experiment E2 with Remus replication, the average
latency for sample transfer did not have much deviation except for a
few jitters.  This is because of the fact that Remus replicates at a
stable, predefined rate (here 40 ms), however, due to network delays
or delay in checkpoint commit, we observed jitters.  These jitters can
be avoided by setting stricter deadline policies in which case, some
samples might get dropped and they might need to be resent. Hence, in
case of no failure, there is very little overhead for this soft real-time
application.  

Figure~\ref{fig:remus_failover_overhead} is the result for experiment E3 where
\textit{WordReader} process on VM1 is replicated using Remus and it experienced
a failure condition.  Before the failure, it can be observed that the latencies
were stable with few jitters due to the same reasons explained above.  When the
failure occurred, it took around 2 seconds for the failover to complete during
which a few samples got lost.  After the failover, no jitters were
observed since Remus replication has not yet started for VM1', but the
latency showed more variation as the system was still stabilizing from
the last failure.  Thus, the high availability solution works for real-time 
applications even though a minor perturbation is present during
the failover.

\begin{figure}[htb] 
\centering
\includegraphics[width=0.8\linewidth,keepaspectratio]{Remus_Failover_Overhead}
\caption{DDS Ownership QoS with Remus Failover}
\label{fig:remus_failover_overhead}
\end{figure}

Table~\ref{table:failover_overhead} represents the missed ratio for different
failover experiments performed.  In experiments E4 and E5, VM2 failed and the
WordNormalizer process failed over to VM3.  Since the DDS failover relied on
publisher/subscriber mechanism, the number of lost samples is low.  The
presence of Remus replication process on WordReader process node VM1 did not
have any adverse effect on the reliability of the system.  However, in case of
experiments E3 and E6, where Remus failover took place, the number of lost
samples was higher since the failover duration is higher in case of Remus
replication than DDS failover.  These experiments show that ongoing Remus
replication does not affect the performance of DDS failover, even though Remus
failover is slower than DDS failover.  However, since DDS does not provide any
high availability for the source, infrastructure-level high
availability provided by our Remus-based solution must be used.  

\begin{table}[htb]
\begin{center}
\caption{Failover Impact on Sample Missed Ratio} % title of Table
\label{table:failover_overhead} % is used to refer this table in the text

    \begin{tabular}{ | >{\centering}p{3.1cm} | >{\centering}p{4cm} | p{4cm}<{\centering}|}
    \hline
    & Missed Samples 

(total of 8000) 
& Missed Samples 
Percentage (\%) \\ \hline
    Experiment 3 & 221 & 2.76\\ \hline
    Experiment 4 &  33 & 0.41  \\ \hline
    Experiment 5 &  14 & 0.18\\ \hline
    Experiment 6 &  549 & 6.86\\
    \hline
    \end{tabular}
\end{center}
\end{table}

