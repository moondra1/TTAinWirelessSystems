% $Id: architecture.tex 30015 2013-10-23 19:15:48Z caglarf $
\section{Middleware for Highly Available VM-hosted Soft Real-time Applications} 
\label{sec:architecture}

This section presents our three contributions that collectively offer a high availability
middleware architecture for soft real-time applications
deployed in virtual machines in cloud data centers.  We
first describe the architecture and then describe the three contributions in detail.

\subsection{Architectural Overview}
\label{sec:sys_arch}
The architecture of our high-availability middleware, as illustrated in
Figure~\ref{fig:sys_design},
comprises a Local Fault Manager (LFM) for each physical host, and a replicated Global Fault
Manager (GFM) to manage the cluster of physical machines.  The inputs
to the LFMs are the resource information of physical hosts and VMs gathered
directly from the hypervisor.
We collect information for resources such as the CPU, memory, network, storage, and processes.  

\begin{figure}[htb] 
\centering
\includegraphics[width=0.7\linewidth,keepaspectratio]{ArchDiagram}
\caption{Conceptual System Design Illustrating Three Contributions}
\label{fig:sys_design}
\end{figure}

The GFM is responsible for making decisions on VM replica management
including the decisions to place a
replica VM. It needs timely resource
utilization information from the LFMs.  Our DDS-based framework
enables scalable and timely dissemination of resource information from
LFMs (the publishers) to the GFM (the subscriber).  Since no
one-size-fits-all replica placement strategy is appropriate for all
applications, our GFM supports a pluggable replica placement
framework. 

\subsection{Roles and Responsibilities}

Before delving into the design rationale and solution details, we
describe how the system will be used in the cloud.
Figure~\ref{fig:use_case} shows a use case diagram for our system in
which roles and responsibilities of the different software components
are defined.  A user in the role of a system administrator will
configure and run a GFM service and the several LFM services.  A user
in the role of a system developer can implement deployment algorithms 
to find and use a better deployment solution.  The LFM services
periodically update resource information of VMs and hosts as
configured by the user.  The GFM service uses the deployment
algorithms and the resource information to create a deployment plan
for replicas of VMs.  Then, the GFM sends messages to LFMs to run a
backup process via high-availability solutions that leverages Remus. 

\begin{figure}[htb] 
\centering
\includegraphics[width=0.8\linewidth,keepaspectratio]{Use_Case}
\caption{Roles and Responsibilities}
\label{fig:use_case}
\end{figure}

\subsection{Contribution 1: High-Availability Solution}
\label{sec:ha}
This section presents our first contribution that deals with providing
a high availability middleware solution for VMs running soft real-time
applications.  Our solution assumes that a VM-level fault recovery is
already available via solutions, such as Remus~\cite{Remus_OSDI:08}.

\subsubsection{Rationale: Why a Hierarchical Model?}

Following the strategy in Remus, we host the primary and backup VMs on different physical servers to
support the fault tolerance. 

In a data center with hundreds of thousands of physical servers, a
Remus-based solution managing fault tolerance for different
applications may be deployed on every server. Remus
makes no effort to determine the effective placement of replica VMs;
it just assumes that a replica pair exists.  For our solution,
however, assuring the QoS of the soft real-time systems requires
effective placement of replica VMs.  In turn, this requires real-time
monitoring of the resource usage on the physical servers to make efficient
placement decisions.

A centralized solution that manages faults across an entire data
center is infeasible.  Moreover, it is not feasible for some central
entity to poll every server in the data center for resource
availabilities and their usage.  Thus, an appropriate choice is to
develop a hierarchical solution based on the principles of separation
of concerns.  At the local level (\emph{i.e.}, host level), a fault
management logic can interact with its local Remus software while also
being responsible for collecting the local resource usage information.
At the global level, a fault management logic can decide effective
replica placement based on the timely resource usage information
acquired from the local entities. 

Although a two-level solution is described, for scalability reasons,
multiple levels can be introduced in the hierarchy where a large data
center can be compartmentalized into smaller regions.

\subsubsection{Design and Operation}

Our hierarchical solution is to utilize several Local Fault Managers (LFMs)
associated with a single Global Fault Manager (GFM) in adjacent levels of the hierarchy. The GFM
coordinates deployment plans of VMs and their replicas by
communicating with the LFMs.  Every LFM retrieves resource
information from a VM that is deployed in the same physical machine as
the LFM, and sends the information periodically to a GFM.
We focus on addressing the deployment issue because existing
solutions such as Remus delegates the responsibility of placing the
replica VM onto the user.  An arbitrary choice may result in severe
performance degradation for the applications running in the VMs.

The replica manager is the core component of the GFM and is
responsible for running the deployment algorithm provided by a
user of the framework. This component determines the
physical host machine where the replica of a VM should be replicated as
a backup. The location of the backup is then supplied to  the LFM running on the
host machine where the VM is located to take the required actions,
such as informing the local Remus of its backup copy.  

The LFM runs a High-Availability Service (HAS) that is based on the Strategy
pattern~\cite{Vlissides:94}. This interface includes starting and stopping 
replica operations, and automatic failover from a primary VM to a backup VM
in case of a failure.  The use of the strategy pattern enables us to
use a solution different from Remus, if one were to be available.
This way we are not tightly coupled with Remus.  Once the HAS is
started and while it is operational, it keeps synchronizing the state
of a primary VM to a backup VM.  If a failure occurs during this
period, it switches to the backup VM making it the active primary VM.
When the HAS is stopped, it stops the synchronization process and
high-availability is discontinued.  

In the context of the HAS, the job of the GFM is to provide each LFM with backup VMs
that can be used when the HAS is executed. In the  event of
failure of a primary VM, the HAS ensures that the processing switches to the
backup VM and it becomes the primary VM.  This event is triggered when the LFM
informs GFM of the failure event and requests additional backup VMs on which a
replica can start.  It is the GFM's responsibility to provide resources to the
LFM in a timely manner so that the latter can move from a crash consistent state
to seamless recovery fault tolerant state as soon as possible thereby assuring
average response times of performance-sensitive soft real-time applications.

In the architecture shown in Figure~\ref{fig:sys_arch}, replicas of VMs are
automatically deployed in hosts assigned by a GFM and LFMs.  The following are
the steps of the system described in the figure.  

\begin{figure}[htb] 
\centering
\includegraphics[width=0.8\linewidth,keepaspectratio]{System_Architecture}
\caption{System Architecture}
\label{fig:sys_arch}
\end{figure}

\begin{enumerate} 
\item A GFM service is started, and the service waits for connections from LFMs.
\item LFMs will join the system by connecting to the GFM service.
\item The joined LFMs periodically send their individual resource usage
information of VMs hosted on their nodes as well as that of the physical host,
such as CPU, memory, and network bandwidth to the GFM using the DDS
solution described in Section~\ref{sec:disseminate}.
\item Based on the resource information, the GFM determines an optimal
deployment plan for the joined physical hosts and VMs by running a deployment
algorithm, which can be supplied and parametrized by users as
described in Section~\ref{sec:placement}.
\item The GFM will notify LFMs to execute HAS in LFMs with information of source
VMs and destination hosts. 
\end{enumerate}

A GFM service can be deployed on a physical host machine or inside a virtual
machine.   In our system design, to avoid a single point of failure of a GFM
service, a GFM is deployed in a VM and a GFM's VM replica is located in another
physical host. When the physical host where the GFM is located fails, the
backup VM containing the GFM service is promoted to primary and the GFM service
continues to its execution via the high availability solution.  

On the other hand, LFMs are placed in physical hosts used to run VMs in data
centers.  LFMs work with a hypervisor and a high availability solution (Remus in our case) 
to collect resource information of VMs and hosts and to replicate
VMs to other backup hosts, respectively.  Through the high availability
solution, a VM's disk, memory, and network connections are actively replicated
to other hosts and a replication of the VM in a backup host is instantiated when
the primary VM is failed.

\subsection{Contribution 2: Pluggable Framework for Virtual Machine Replica Placement}
\label{sec:placement}
This section presents our second contribution that deals with
providing a pluggable framework for determining VM replica placement.

\subsubsection{Rationale: Why a Pluggable Framework?}
Existing solutions for VM high availability, such as Remus, delegate the task of
choosing the physical host for the replica VM to the user.  This is a
significant challenge since a bad choice of a heavily loaded physical host may
result in performance degradation.  Moreover, a static decision is also not
appropriate since a cloud environment is highly dynamic.  To provide maximal
autonomy in this process requires online deployment algorithms that make
decisions on VM and replica VM placement.

Deployment algorithms determine which host machine should store a VM and its
replica in the context of fault management.  There are different types 
of algorithms to make this decision.  Optimization algorithms, such as bin
packing, genetic algorithms, multiple knapsack, and simulated annealing are some
of the choices used to solve similar problems in a large number of industrial applications today.
Moreover, different heuristics of the bin packing algorithm are commonly
utilized techniques for VM replica placement optimization, in particular.  

Solutions generated by such algorithms and heuristics have different
properties.  Similarly, the runtime complexity of these algorithms is
different.  Since different applications may require different
placement decisions and may also impose different constraints on the
allowed runtime complexity of the placement algorithm, a
one-size-fits-all solution is not acceptable.  Thus, we needed a
pluggable framework to decide VM replica placement.

\subsubsection{Design of a Pluggable Framework for Replica VM Placement}
In bin packing algorithms~\cite{berkey1987two}, the goal is to use minimum
number of bins to pack the items of different sizes.  Best-Fit, First-Fit,
First-Fit-Decreasing, Worst-Fit, Next-Fit, and Next-Fit-Decreasing are the
different heuristics of this algorithm.  All these heuristics will be part
of the middleware we are designing, and will be provided to the 
framework user to run the bin packing algorithm. 

In our framework, we view VMs as items and the host machines as the bins.
Resource information from the VMs, are utilized as weights to employ the bin packing
algorithm.  Resource information is aggregated  into one single scalar value,
and one dimensional bin packing is employed to find the best host machine where
the replica of a VM will be stored.  Our framework uses the Strategy pattern
to enable plugging in different VM replica placement algorithms.  A concrete
problem we have developed and used in our replication manager is described in
Section~\ref{sec:experiment}. 

\subsection{Contribution 3: Scalable and Real-time Dissemination of Resource Usage}
\label{sec:disseminate}

This section presents our third contribution that deals with using a
pub/sub communication model for real-time resource monitoring.  Before
delving into the solution, we first provide a rationale for using a
pub/sub solution.


\subsubsection{Rationale: Why Pub/Sub for Cloud Resource Monitoring and Dissemination?}
\label{sec:why_pubsub}

Predictable response times are important to host soft real-time
applications in the cloud.  This implies that even after a failure and
recovery from failures, applications should continue to receive
acceptable response times.  In turn this requirement requires that the
backup replica VMs be placed on physical hosts that will deliver the
application-expected response times.  

A typical data center comprises hundreds of thousands of commodity
servers that host virtual machines.  Workloads on these servers (and
hence the VMs) demonstrates significant variability due to newly
arriving customer jobs and the varying number of resources they
require.  Since these servers are commodity machines and due to the
very large number of such servers in the data center, failures are
quite common.  

Accordingly, any high-availability solution for virtual machines that
supports real-time applications must ensure that primary and backup
replicas must be hosted on servers that have enough available
resources to meet the QoS requirements of the soft real-time
applications.  Since the cloud environment is a highly dynamic
environment with fluctuating loads and availabilities of resources, it
is important that real-time information of the large number of cloud
resources be available to the GFM to make timely decisions on
replica placement. 

It is not feasible to expect the GFM to pull the resource information
from every physical server in the data center.  First, this will
entail maintaining a TCP/IP connection.  Second, failures of these
physical servers will disrupt the operation of the GFM.  A better
approach is for resource information to be asynchronously pushed to
the GFM.  We surmise therefore that the pub/sub~\cite{ManyPubSubFaces:03}
paradigm has a vital role to play in addressing these requirements.
A solution based on the "push" model, where information is pushed
to the GFM from the LFMs asynchronously, is a scalable alternative.
Since performance, scalability, and timeliness in information
dissemination are key objectives, the OMG Data Distribution Service
(DDS)~\cite{OMG-DDS:07} for data-centric pub/sub is a promising
technology that can  be adopted to disseminate resource monitoring
data in cloud platforms. 

\subsubsection{Design of a DDS-based Cloud Resource Monitoring Framework}
A solution based on
the ``push'' model where information can be pushed to the GFM
asynchronously lends itself to a more scalable alternative.

The GFM is consuming information from the LFMs and is a subscriber. 
The resources themselves are the publishers of
information. Since LFMs are hosted on the physical hosts from which
the resource utilization information is collected, the LFMs can also play
the role of a publisher.  The roles are reversed when a decision from
GFM is pushed to the LFMs.

Figure~\ref{fig:monitoring} depicts the DDS entities used for our
framework. Each LFM node has its domain participant containing a
DataWriter and a DataReader.  A DataWriter in LFM is configured to
periodically disseminate resource information of VMs to a
DataReader in GFM via a LFM Topic.  The LFM obtains this information
via the libvirt APIs.  A DataReader in LFM is to receive command
messages from a DataWriter in GFM to start and stop a HAS via GFM
Topic when a decision is made by algorithms in GFM.   

\begin{figure}[htb] 
\centering
\includegraphics[width=0.8\linewidth,keepaspectratio]{JSAMonitoringDesign}
\caption{DDS Entities}
\label{fig:monitoring}
\end{figure} 

\iffalse
\label{sec:design_goals}

\subsection{Implementation Details}

In this section, details of the implementation of our framework are described
with class diagrams.  The class diagram of GFM is presented in
Figure~\ref{fig:gfm_class_diagram}.  We used the ACE Service Configurator
framework to make a GFM service flexible and extensible by supporting interfaces
to configure the service at any point in time.  The ACE Service Configurator is
a framework that allows seamless start, stop, suspend and resume of services
that it manages.  To enable this activity, every application instance
implemented as an object oriented class instance must inherit from the
\textit{ACE\_Service\_Object} class.  In our case, therefore, 
\textit{GFMServiceObject} inherits from \textit{ACE\_Service\_Object} and is the
initial point of executing the service.  The GFM service is initialized and
started by calling \textit{init()} with the creation of an object of the
\textit{GFMAcceptor}.

\begin{figure}[htb] 
\centering
\includegraphics[width=0.8\linewidth,keepaspectratio]{GFM_Class_Diagram}
\caption{GFM Class Diagram}
\label{fig:gfm_class_diagram}
\end{figure}

The ACE Acceptor-Connector framework is used to decouple the connection and
initialization of network services from the data processing after connection
establishment and initialization.  The \textit{GFMAcceptor} implements the
Acceptor interface of ACE because the GFM service behaves as an acceptor
(\emph{i.e.}, the role of a server) to LFM services that connect to it via
TCP/IP.  Once the connection and initialization is established with a LFM
service, an object of the GFMAcceptorHandler for processing data communications
with the connected LFM is created.  The objects of the
\textit{GFMAcceptorHandler} is implemented by inheriting interfaces for the ACE
Reactor framework.  The ACE Reactor framework helps the service to detect and
dispatch diverse events for communicating with LFM services.  For encoding the
messages exchanged between the GFM and LFMs, we use a binary format called the
Common Data Representation (CDR) supported by the ACE CDR functionality.  

The GFM framework provides a Strategy class interface,
\textit{DeploymentStrategy}, for a deployment algorithm.  Through the framework
interfaces, system developers can implement their own algorithms and examine
performance of the algorithms comparing to other existing algorithms.  The
Strategy pattern is used to provide a more flexible way for system developers to
implement the algorithms and change the deployment algorithm at run-time for
system administrators.  In our current implementations, bin packing strategies
by ordering of  random, first-fit, and best-fit are provided. The deployment
strategy is strategically configurable by a parameter defined in the
GFMServiceObject.

Figure~\ref{fig:lfm_class_diagram} depicts the class diagram of LFM.  Like GFM,
to implement the LFM framework, the ACE Service Configurator,
Acceptor-Connector, and Reactor frameworks are used.  In particular, since the
LFM is the client to the GFM, it implements the ACE\_Connector interface.  The
ACE CDR is also used to process a stream of data from a GFM according to the
protocol.  Additionally, LFM contains a wrapper interface, \textit{VirConnect}
to connect to a hypervisor for collecting resource  information of VMs, and
interfaces for controlling Remus services, \textit{RemusManager}.

\begin{figure}[htb] 
\centering
\includegraphics[width=0.8\linewidth,keepaspectratio]{LFM_Class_Diagram}
\caption{LFM Class Diagram}
\label{fig:lfm_class_diagram}
\end{figure}

\fi

