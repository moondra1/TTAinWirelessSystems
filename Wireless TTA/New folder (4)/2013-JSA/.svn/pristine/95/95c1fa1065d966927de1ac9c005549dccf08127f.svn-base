% $Id$
\section{Introduction}
\label{sec:introduction}

Cloud computing is a large-scale distributed computing platform 
based on the principles of utility computing that offers resources 
such as CPU and storage, systems software, and applications as services 
over the Internet~\cite{Armbrust_CACM:10}.  The driving force 
behind the success of cloud computing is economy of scale.  Traditionally, cloud
computing has focused on enterprise applications.  Lately, however, a class of
soft real-time applications that demand both high availability and predictable
response times are moving towards cloud-based
hosting~\cite{Corradi_DDS_ISCC:12,DoD_Cloud_Strategy:12,SKS_MEDCPS:13}.

To support soft real-time applications in the cloud, it is necessary to satisfy
the response time, reliability and high availability demands of such
applications.  Although the current cloud-based offerings can adequately address
the performance and reliability requirements of enterprise applications, new 
algorithms and techniques are necessary to address the Quality of Service (QoS)
needs, \emph{e.g.}, low-latency needed for good response times and high
availability, of performance-sensitive, real-time applications.  

For example, in a cloud-hosted platform for personalized wellness
management~\cite{SKS_MEDCPS:13}, high-availability, scalability and timeliness
is important for providing on-the-fly guidance to wellness participants to
adjust their  exercise or physical activity.  Such guidance is based on
real-time tracking of the participant's response to current activity that is
obtained from a suite of novel sensors.  Assured performance and high
availability is important because the wellness management cloud infrastructure
integrates and interacts with the exercise machines both to collect data about
participant performance and to adjust the intensity and duration of the
activities.

Prior research in cloud computing has seldom addressed the need for supporting
real-time applications in the cloud.\footnote{In this research we focus on soft
  real-time applications since it is unlikely that hard real-time and
  safety-critical applications will be hosted in the cloud.}   However, there is
a growing interest in addressing these challenges as evidenced by recent
efforts~\cite{RTXen_EMSOFT:11}.  Since applications hosted in the cloud often
are deployed in virtual machines (VMs), there is a need to assure the real-time
properties of the VMs.  A recent effort on real-time extensions to the Xen
hypervisor~\cite{RTXen_EMSOFT:11} has focused on improving the scheduling
strategies in the Xen hypervisor to assure real-time properties of the VMs.
While timeliness is a key requirement, reliability and high availability are
also equally important requirements that must be satisfied.

Fault tolerance based on redundancy is one of the fundamental principles for
supporting high availability in distributed systems.  In the context of cloud
computing, the Remus~\cite{Remus_OSDI:08} project has demonstrated an effective
technique for VM failover using one primary and one backup VM solution that also
includes periodic state synchronization among the redundant VM replicas.  The
Remus failover solution, however, incurs shortcomings in the context of
providing high availability for soft real-time systems hosted in the cloud.

For instance, Remus does not focus on effective replica placement.
Consequently, it cannot assure real-time performance after a failover decision
because it is likely that the backup VM may be on a physical server that is
highly loaded.  The decision to effectively place the replica is left to the
application developer.  Unfortunately, any replica placement decisions made
offline are not attractive for a cloud platform because of the substantially 
changing dynamics of the cloud platform in terms of workloads and failures.
This requirement adds an inherent complexity for the developers who are
responsible for choosing the right physical host with enough capacity to host
the replica VM such that the real-time performance of applications is met.  It
is not feasible for application developers to provide these solutions, which
calls for a cloud platform-based solution that can shield the application
developers from these complexities.

To address these requirements, this paper makes the following three
contributions: 

\begin{enumerate}
  \item We present a fault tolerant architecture in the cloud geared to provide
  high availability and reliability for soft real-time applications.  Our
  solution is provided as a middleware that extends the Remus VM failover
  solution~\cite{Remus_OSDI:08} and is integrated with the OpenNebula
  cloud infrastructure software~\cite{fontan2008opennebula} and the Xen
  hypervisor~\cite{Xen_SOSP:03}.  Specifically, we present a hierarchical
  architecture motivated by the need for separation of concerns and
  scalability. 
 
  \item In the context of our fault tolerance architecture, we present the
  design of a pluggable framework that enables application developers to provide
  their strategies for choosing physical hosts for replica VM placement.  Our
  solution is motivated by the fact that not all applications will impose
  exactly the same requirements for timeliness, reliability and high
  availability, and hence a ``one-size-fits-all'' solution is unlikely to be
  acceptable to all classes of soft real-time applications.  Moreover,
  developers may also want to fine tune their choice by trading off resource
  usage and QoS properties with the cost incurred by them to use the cloud
  resources.

  \item For the first two contributions to work effectively, there is a need for
  a low-overhead, and real-time messaging between the infrastructure components
  of the cloud infrastructure middleware.  The messaging capability is needed to
  reliably inform real-time resource utilization information from the cloud data
  center servers to the controllers that perform resource allocation and
  management decisions.  To that end we present a solution based on real-time
  publish/subscribe (pub/sub) that extends the OMG Data Distribution Service
  (DDS)~\cite{OMG-DDS:07} with additional architectural elements that fit within
  our fault tolerance middleware.  

\end{enumerate}

To evaluate the effectiveness of our solution, we use a ``word count'' real-time
streaming application as a representative soft real-time application hosted in
the cloud and requiring high availability.   For replica VM placement, we have
developed an Integer Linear Programming (ILP) formulation that can be plugged
into our framework.  Our concrete placement algorithm allocates VMs and their
replicas to physical resources in a data center that satisfies the QoS
requirements of the applications.   We present results of experimentation
focusing on critical metrics for real-time applications such as end-to-end
latency and deadline miss ratio.  Through the experiments we show how VM based
high availability solution degrades  the performance of real-time applications, but 
still meets the QoS requirements.

The rest of this paper is organized as follows: Section~\ref{sec:relwork}
describes relevant related work comparing it with our contributions;
Section~\ref{sec:background} provides background information on the
underlying technologies we have leveraged in our solution;
Section~\ref{sec:architecture} describes the system architecture that
provides high availability solutions to performance-sensitive
applications hosted in the cloud; Section~\ref{sec:experiment}
presents experimental results with a case  study of highly available
real-time application using DDS; and Section~\ref{sec:conclusion}
presents concluding remarks alluding to future work.

