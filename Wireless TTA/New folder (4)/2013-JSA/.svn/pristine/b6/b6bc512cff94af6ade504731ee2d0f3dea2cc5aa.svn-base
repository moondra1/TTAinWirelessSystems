% $Id$
\documentclass[preprint,12pt]{elsarticle}

\usepackage{mathtools}
\usepackage{tabulary}
\usepackage{amssymb,amsmath}
\usepackage{url}
\usepackage{graphicx}
% declare the path(s) where your graphic files are
\graphicspath{{pdf/}}
% and their extensions so you won't have to specify these with
% every instance of \includegraphics
\DeclareGraphicsExtensions{.pdf,.jpeg,.png}

\journal{Elsevier Journal of Systems Architecture}

\begin{document}

\begin{frontmatter}

\title{A Middleware Design for Assuring Performance and High Availability
  for Cloud-hosted Soft Real-time Applications\tnoteref{sponsor}} 
\tnotetext[sponsor]{This work was supported in part by NSF awards CAREER/CNS
  0845789 and SHF/CNS 0915976. Any opinions, findings, and conclusions or
  recommendations expressed in this material are those of the author(s) and do
  not necessarily reflect the views of the National Science Foundation.}

\author[vandy]{Kyoungho An, Shashank Shekhar, Faruk Caglar, Aniruddha Gokhale}
\author[sks]{Shivakumar Sastry}

\address[vandy]{
Institute for Software Integrated Systems (ISIS)\\
Department of Electrical Engineering and Computer Science\\
Vanderbilt University, Nashville, TN 37235, USA\\
Email: \{kyoungho.an, shashank.shekhar, faruk.caglar, a.gokhale\}@vanderbilt.edu
}

\address[sks]{
Complex Engineered Systems Lab\\
Department of Electrical and Computer Engineering\\
The University of Akron, Akron, OH 44325, USA\\
Email: ssastry@uakron.edu
}

\begin{abstract}
Applications are increasingly being deployed in the Cloud due to benefits
stemming from economy of scale, scalability, flexibility and utility-based
pricing model.  Although most cloud-based applications have hitherto been
enterprise-style, there is an emerging need for hosting real-time streaming
applications in the cloud that demand both high availability and low latency.  
Contemporary cloud computing research has seldom focused on solutions that
provide both high availability and real-time assurance to these applications in
a way that also optimizes resource consumption in data centers, which is a key
consideration for cloud providers.  This paper makes three contributions to
address this dual challenge.   First, it describes an architecture for a
fault-tolerance framework that can be used to automatically deploy replicas of 
virtual machines in data centers in a way that optimizes resources while
assuring availability and responsiveness.  Second, it describes the design of a
pluggable framework within the fault tolerance architecture that enables
plugging in different placement algorithms for VM replica deployment.  Third, it
illustrates the design of a framework for real-time dissemination of resource
utilization information using a real-time publish/subscribe framework, which is
required by the replica selection and placement framework.  Experimental results
using a case study that involves a specific replica placement algorithm are
presented to evaluate the effectiveness of our architecture.
\end{abstract}

\begin{keyword}
high availability, real-time, quality of service, cloud
computing, middleware, framework.
\end{keyword}

\end{frontmatter}

\input{introduction}
\input{relwork}
\input{background}
\input{architecture}
\input{experiment}
\input{conclusion}


%% References with bibTeX database:

\bibliographystyle{elsarticle-num}
\bibliography{cloud,networks}

\end{document}

