% $Id$
\section{Related Work}
\label{sec:relwork}

This section presents related work and compares it with our contributions.  In
particular, we organize the related work along three dimensions of our research
contributions.  We then present a comparative analysis comparing the presented
related work holistically in the context of our three contributions.

% folks, I reorganized the sections.

\subsection{High Availability Solutions for Virtual Machines}
This section focuses on high availability solutions for virtual machines and
presents related work in that area.  Our fault tolerance solution leverages the continuous checkpointing-based, high
availability solution developed for the Xen hypervisor called
Remus~\cite{Remus_OSDI:08}.  Details on Remus including its shortcomings are
provided in Section~\ref{sec:remus_overview}.  

Several  other high availability solutions exist for virtual machines. VMware fault
tolerance~\cite{DBLP:journals/sigops/ScalesNV10} runs primary and backup VMs in
lock-step~\cite{DBLP:journals/tocs/BressoudS96} using deterministic replay.
This keeps both the VMs in sync but it requires execution at both the VMs and
needs network connections of high quality.  In contrast, our model focuses on a
primary-backup scheme for VM replication which does not require execution on all
replica VMs.

Kemari~\cite{kemari2008validating} is another approach which takes advantage of
both lock-stepping and continuous check-pointing approaches.  It synchronizes
primary and secondary VMs just before the primary VM has to send an event to
devices, such as storage and networks.  At this point, the primary VM pauses and
Kemari updates the state of secondary VM to the current state of primary VM.
Thus, VMs are synchronized with less complexity compared to lock-stepping and
output latency of continuous check-pointing due to external buffering mechanisms
is also avoided. However, we opted for Remus since it is a mature solution compared to 
Kemari.

Another important work on high availability is HydraVM~\cite{HPL-2011-24}. It is
a storage-based, memory-efficient high availability solution which does not need
passive memory reservation for backups.  It uses incremental check-pointing like
Remus~\cite{Remus_OSDI:08}, but it maintains a complete recent image of VM in shared storage instead
of memory replication.  Thus, it claims to reduce hardware costs for providing high
availability support and provide greater flexibility as recovery can happen on
any physical host having access to shared storage. However, the software is not 
open-sourced or commercially available, thus cannot be applied in our solution.


\subsection{Aproaches to Virtual Machine Placement}
Virtual machine placement on physical hosts is an important criteria in
assuring performance to applications hosted in VMs.  Although individual VMs may
be provided a share of the physical resources, if the physical host is heavily
loaded, then effects of context switching and other systemic affects including
performance interference~\cite{Zhang:2013:CCP:2465351.2465388, pu2010understanding,
tickoo2010modeling, nathuji2010q}, will adversely impact the performance
delivered to the VM and hence the applications that are hosted within the VM.
This is particularly important when high availability solutions based on
replication must also consider performance as is the case in our research.
Naturally, more autonomy in VM placement is desirable. 

A related work closest to ours appears in~\cite{hyser2007autonomic}.  The
authors present a prototype of a VM placement system where an autonomic
controller dynamically manages the mapping of VMs onto physical hosts according
to policies specified by the user.  As in our case, they too suggest a system
architecture for autonomic virtual machine placement in accordance with
algorithms defined by users.  However, our research additionally considers
dynamic allocation of VM replicas with high-availability solutions to
accomplish a fault-tolerant cloud computing architecture.  Therefore, our work
addresses a more complex problem.  

Lee et al.~\cite{lee2011validating} investigate the VM consolidation heuristics
to discover the assumptions on how virtual machines operate when they are
co-located on the same host machine.  Additionally, they explore how the resource 
demand such as CPU, memory, and network bandwidth are handled with respect to 
resource availability in the context of VM consolidation.   The work
in~\cite{beloglazov2010energy} proposes a modified Best Fit Decreasing (BFD)
algorithm as VM reallocation heuristics for efficient resource management.
The evaluation in the paper shows that the suggested heuristics minimize energy
consumption while providing improved Quality of Service (QoS).  While our work
may benefit from these prior works, we are additionally concerned with placing
replicas in a way that after fail over, the applications will continue to obtain
the desired QoS.

\subsection{Resource Monitoring in Large Distributed Systems}
In this section we compare our work on real-time resource monitoring to prior
work on resource monitoring in cloud and related distributed systems.
Contemporary compute clusters and grids have provided  special capabilities to
monitor the distributed systems via frameworks, such as
Ganglia~\cite{massie2004ganglia} and Nagios~\cite{barth2008nagios}.  
According to~\cite{foster2008cloud}, one of the distinctions between grids and
cloud is that cloud resources also include virtualized resources.   Thus, the
grid- and cluster-based frameworks are structured primarily to monitor physical
resources only, and not a mix of virtualized and physical resources.   Even
though some of these tools have been enhanced to work in the cloud, \emph{e.g.},
virtual machine monitoring in
Nagios\footnote[1]{\url{http://people.redhat.com/~rjones/nagios-virt}} and
customized scripts used in Ganglia, they still do not focus on the timeliness
and reliability of the dissemination of monitored data that is essential to
support application QoS in the cloud.  A work that comes closest to
ours~\cite{huang2007study} provides a comparative study of publish/subscribe
middleware for real-time grid monitoring in terms of real-time performance and
scalability.  While this work also uses publish/subscribe for resource
monitoring, it is done in the context of grid and hence incurs the same
limitations.

In other recent works, ~\cite{han2011virtual} presents a virtual resource
monitoring model while~\cite{de2011toward} discusses cloud monitoring
architecture for private clouds.  Although these prior works describe cloud
monitoring systems and architectures, they do not provide experimental
performance results of their models for properties, such as system overhead and
response time.  Consequently, we are unable to determine their relevance to
support timely dissemination of resource information and hence their ability to
host mission-critical applications in the cloud.   Latency results using RESTful
services for resource monitoring are described in~\cite{guinard2010resource},
however, they are not able to support diverse and differentiated service levels
for cloud clients as we are able to provide.

\subsection{Comparative Analysis}
Although related work along individual dimensions of our research, \emph{i.e.},
high availability for VMs, placement of VMs, and resource monitoring exist to
some extent, to the best of our knowledge all the known solutions exist in
isolation and we are not aware of any work that brings these concepts together
holistically in a framework that can be used in a cloud infrastructure.
Consequently, the combined effect of individual solutions has not been
investigated.  Our work is a step in the direction of fulfilling this void.
Integrating the three different approaches is not straightforward and requires
good design decisions, which we have demonstrated with our work and presented in
the remainder of this paper.
