% $Id$
\section{Experimental Results and Case Study}
\label{sec:experiment}

The purpose of this section is to validate our approach and to show that our
solution can coexist with other middleware-provided fault tolerance solutions.
Moreover, we also demonstrate that it is feasible for users to leverage only
parts of our solution as needed.  The validation part of this section
demonstrates the impact of our high availability and replica placement approach
on end-to-end latencies experiences by real-time applications.  To that end we
have used the performance benchmarks supplied by RTI Connext Data Distribution
Service (DDS) version 5.0~\cite{dds_benchmark}.   To demonstrate how our
solution can coexist with other solutions, we used a \textit{word count}
application as a case study of real-time data processing in highly available
virtualized environment.

\subsection{Testbed Environment}
Our private cloud infrastructure for both the experiments we conducted comprises
a cluster of 20 rack servers, and Gigabit switches. The cloud infrastructure is
operated using OpenNebula 3.0 with shared file systems using NFS (Network File
System) for distributing virtual machine images. The following is the hardware
specification of a rack server used as a clustered node: 12-core 2.1 GHz Opteron
CPUs, 32GB of RAM, 8TB of HDD, and 4 Gigabit Ethernet cards.  

Ubuntu 10.04 64-bit was installed as host operating systems for the cluster
nodes.  Xen 4.1.2 hypervisor was installed to operate virtual machines, and our
system adopted para-virtualization for guest domains. Our guest domains run
Ubuntu 11.10 32-bit as operating systems, and each guest domain has 4 virtual
CPUs and 4GB of RAM. 

\subsection{Experiment 1: Measuring the Impact on Latency of Real-time Applications}

To validate our high availability solution including the VM replica placement
framework, we have used the RTI DDS Connext latency benchmark.  RTI Connext is
an implementation of the OMG Data Distribution Service (DDS)~\cite{OMG-DDS:07}
standard.  DDS supports a data-centric publish/subscribe communication model
between peers and provides different configurations to realizing end-to-end
quality of service (QoS) between the communicating peers. The RTI Connext
benchmark comprises code to evaluate latency of DDS applications.

Our purpose in using this benchmark was to validate the impact of our high
availability solution and replica VM placement decisions on the latency of DDS
applications.  For this purpose the DDS application was deployed inside a VM.
We make a performance comparison between an optimally placed VM replica by a
bin-packing strategy of our framework and a potentially worse case scenario
resulting from a randomly deployed VM.  In the experiment, average latency
and standard deviation of latency, which is a measure of the jitter, are
compared for different settings of Remus and VM placement.  Since a DDS
application is a one directional flow from a publisher to a subscriber, the
latency measurement is estimated as half of the round-trip time which is
measured at a publisher.  In each experimental run, 10,000 samples of stored
data in the defined byte sizes in the table are sent from a publisher to a
subscriber.  We also compare the performance when no high availability solution
is used.  The rationale is to gain insights into the overhead imposed by the
high availability solution.

Table~\ref{table:latency} shows how our Remus-based high availability solution
along with the effective VM placement affects latency of real-time applications.
The measurements from the experimental results for the case of \textit{Without
  Remus}, where VM is not replicated, shows consistent range of standard
deviation and average of latency compared to the case of \textit{Remus with
  Efficient Placement}.  When Remus is used, average latency does not increase
significantly, however, a higher fluctuation of latency is observed by measuring
standard deviation values between both cases.  We can reach a conclusion that
the state replication overhead of Remus incurs a wider range of latency
fluctuations.

\begin{table}[htb]
\begin{center}
\caption{Latency Performance Test for Remus and Effective Placement} % title of Table
\label{table:latency} % is used to refer this table in the text

\begin{tabular}{ | >{\centering} p{1cm} || >{\centering} p{1.6cm}  |  >{\centering}p{1.6cm}  || >{\centering}p{1.6cm}  | >{\centering}p{1.6cm} || >{\centering}p{1.6cm} | p{1.6cm} <{\centering}| }
  \hline
  \multicolumn{1}{ |p{1cm}|| } { } &
  \multicolumn{2}{ >{\centering}p{3.5cm}|| } { \textbf{Without Remus}} &
  \multicolumn{2}{ >{\centering}p{3.5cm}|| } { \textbf{Remus with     
Efficient Placement}} &
  \multicolumn{2}{ >{\centering}p{3.5cm}| }{ \textbf{Remus with Worse Case 
Placement}} \\
  \hline
  bytes & stdev($\mu$s) &  avg($\mu$s) & stdev($\mu$s)& avg($\mu$s) & stdev($\mu$s) & avg($\mu$s) \\
  \hline
  16 & 21.6 &  282.2 & 244.4 & 315.5 & 877.5 & 372.9 \\
  32 & 16.4 &  280.4 & 140.7 & 310.2  & 1531.9 & 393.9 \\
  64 & 17.7 &  291.4 & 157.3 & 338.7 & 619.5 & 369.9 \\
  128 & 21.6 &  309.0 & 121.4 & 326.2 & 803.6 & 345.5 \\
  256 & 16.6 &  323.2 & 124.2 & 361.0 & 934.5 & 377.2 \\
  512 & 7.5 &  331.6 & 173.4 & 367.1 & 982.8 & 388.0 \\
  1024 & 8.6 &  348.1 & 127.6 & 367.2 & 731.4 & 399.4 \\
  2048 & 39.6 & 376.5  & 144.1 & 426.6 & 922.7 & 496.6 \\
  4096 & 30.7 &  608.1 & 151.2 & 616.2 & 2249.7 & 719.3 \\
  8192 & 28.7 & 758.4  & 169.0 & 779.7 & 1489.2 & 904.8 \\

  \hline
\end{tabular}

\end{center}
\end{table}

The significantly wider range of latency fluctuations are observed in the
standard deviation of latency in \textit{Remus with Worst Case Placement}. Our
framework guarantees that the appropriate number of VMs are deployed in physical
machines by following defined resource constraints so that contention of
resources between VMs does not happen even though a VM or a physical machines is
crashed.  However, if a VM and its replica is randomly placed without any
constraints, unexpected latency increases for applications running on the VM can
occur.  The resulting values of latency's standard deviation in \textit{Remus
  with Worst Case Placement} demonstrate how the random VM placement negatively
influences timeliness properties of applications.  

\subsection{Experiment 2: Support for Co-hosting High Availability Solutions}
Often times the applications or their software platforms support fault tolerance
and high availability solutions.  The purpose of this this experiment is to test
whether it is possible to co-host both our Remus-based high availability
solution and the third party solution.  Moreover, we also wanted to test if it
is possible to leverage only a subset of our solution when needed.

To ascertain these claims, we developed a case study implemented in C++ using
OMG DDS.  OMG DDS supports a QoS configuration called \emph{Ownership Strength,}
which can be used as a fault tolerance solutions by some publisher/subscribers
of a DDS application.  In particular, high availability of a DDS application can
be realized by making use of replicated multiple data writers (which are
DDS entities belonging to a publisher) and the DDS Ownership QoS which
controls the number of data writers and their priorities by defining the
\textit{OWNERSHIP\_STRENGTH} configuration in conjunction with the exclusive
option of the DDS Ownership QoS.  When a failure occurs, a data reader (which is
a DDS entity belonging to a subscriber) automatically fails over to receive its
subscription from a data writer having the next highest strength among the
replica data writers.  

Note that although such a fault tolerance solution can be realized in the above
case using the ownership QoS, there is no equivalent method in DDS if
a failure occurs at the source of events such as a node that aggregates multiple
sensor data and a node reading a local file stream as a source of events.  In
such circumstances, it may be necessary to use a solution such as ours.  In
effect we therefore design a high availability solution for the DDS application
that requires co-existence of both our solution and the DDS-supplied solution.
Note that even though the DDS ownership QoS takes care of replicating the data
writers and organizing them according to the ownership strength, if these data
writers are deployed in VMs of a cloud data center, they will benefit from the
replica VM placement strategy provided by our approach.  Indeed, our solution
allows using a subset of the solution when needed. 

\begin{figure}[htb] 
\centering
\includegraphics[width=0.8\linewidth,keepaspectratio]{Word_Count}
\caption{Example of Real-time Data Processing: Word Count}
\label{fig:word_count}
\end{figure}

To experiment with such a scenario and examine the performance overhead as well
as message missed ratio (lost messages during failover), we developed a
DDS-based ``word count'' real-time streaming application.  The system integrates
both the high availability solutions.  Figure~\ref{fig:word_count} shows the
structure of the \textit{word count} application running on the highly available
system.  Four VMs are employed to execute the example application.  VM1 runs a
process to read input sentences and publish a sentence to the next processes. We
call the process running on the VM1 as the \textit{WordReader}. In the next
processes, a sentence is split into words and the processes are called
\textit{WordNormalizer}.  We place two VMs for the normalizing process and each
data writer's Ownership QoS is configured with the exclusive connection to a
data reader and the data writer in VM3 is set to the primary with higher
strength.  Once the sentences get split, words are published to the next
process called the \textit{WordCounter}, where finally the words are counted.
In the example, we can duplicate processes for \textit{WordNormalizer} and
\textit{WordCounter} as they process incoming events, but a process for
\textit{WordReader} cannot be replicated by having multiple data writers in
different physical nodes as the process uses a local storage as a input
source.  In this case, our VM-based high availability solution is adopted. 

\begin{table}[htb]
\begin{center}
\caption{DDS QoS Configurations for the Word Count Example} % title of Table
\label{table:qos} % is used to refer this table in the text
{\small
\begin{tabulary}{\columnwidth}{|L|L|}\hline
\multicolumn{1}{|c|}{\textbf{DDS QoS Policy}} & 
\multicolumn{1}{|c|}{\textbf{Value}}\\ \hline \hline
\textbf{Data Reader} &  \\
Reliability & Reliable  \\ 
History & Keep All  \\ 
Ownership   & Exclusive  \\ 
Deadline  & 10 milliseconds  \\ 
\hline
\textbf{Data Writer} &  \\
Reliability & Reliable  \\ 
Reliability - Max Blocking Time & 5 seconds  \\ 
History & Keep All  \\ 
Resource Limits - Max Samples & 32 \\ 
Ownership   & Exclusive  \\ 
Deadline  & 10 milliseconds  \\ 
\hline
\textbf{RTPS Reliable Reader} &  \\
MIN Heartbeat Response Delay & 0 seconds \\ 
MAX Heartbeat Response Delay & 0 seconds  \\ 
\hline
\textbf{RTPS Reliable Writer} &  \\
Low Watermark & 5  \\ 
High Watermark & 15  \\ 
Heartbeat Period & 10 milliseconds \\ 
Fast Heartbeat Period   & 10 milliseconds  \\ 
Late Joiner Heartbeat Period   & 10 milliseconds  \\ 
MIN NACK Response Delay   & 0 seconds  \\ 
MIN Send Window Size   & 32  \\ 
MAX Send Window Size   & 32  \\ 

% [0.5ex] % [1ex] adds vertical space
\hline
\end{tabulary}
}
\end{center}
\end{table}

Table~\ref{table:qos} describes the DDS QoS configurations used for our
\textit{word count} application. The throughput and latency of an application
can be varied by different DDS QoS configurations.  Therefore,  our
configurations in the table can provide a reasonable understanding of our
performance of experiments described below.  In the \textit{word count}
application, since consistent word counting information is critical,
\textit{reliable} rather than \textit{best effort} is designated as the
Reliability QoS.  For reliable communication, history samples are all kept in
the reader's and writer's queues. As the Ownership QoS is set to
\textit{exclusive}, only one primary data writer among multiple data writers can
publish samples to a data reader.  If a sample has not arrived in 10
milliseconds, a deadline missing event occurs and the primary data writer is
changed to the one which has the next highest ownership strength. 
\iffalse
% Kyoungho, I commented this out since RTPS was not introduced
We use the same configurations for RTPS Reliable Reader and RTPS as the ones
defined in the Reliable QoS example provided by RTI Connext DDS.  
\fi

The results of experimental evaluations are presented to verify performance and
failover overhead of Remus and DDS Ownership QoS.  We experimented six cases
shown in the Figure~\ref{fig:experiments} to understand latency and failover
overhead of running Remus and DDS Ownership QoS for the word count real-time
application.  The experimental cases represent combinatorial fail over testings
in an environment selectively exploiting Remus and DDS Ownership QoS.   

\begin{figure}[htb] 
\centering
\includegraphics[width=0.8\linewidth,keepaspectratio]{Experiments}
\caption{Experiments for the Case Study}
\label{fig:experiments}
\end{figure}

Figure~\ref{fig:remus_latency_overhead} depicts the results of Experiment E1 and
E2 from figure~\ref{fig:experiments}. Both the experiments have Ownership QoS
setup as described above.  Experiment E2 additionally has VM1 running
\textit{WordReader} process, which is replicated to VM1' whose placement
decision is made by our algorithm.  The virtual machine VM1 is replicated using
Remus high availability solution with the replication interval set to 40
milliseconds for all the experiments.  This interval is also visibly, the lowest
possible latency for all the experiments, which has ongoing Remus
replication.  All the experiments depicted in Figure~\ref{fig:experiments} 
involved a transfer of 8,000 samples from \textit{WordReader} process on VM1 to
\textit{WordCount} process running on VM4.  In the experiments E1 and E2,
\textit{WordNormalizer} processes run on VM2 and VM3 and incur the overhead of
DDS Ownership QoS. In addition, experiment E2 has the overhead of Remus
replication.


\begin{figure}[htb] 
\centering
\includegraphics[width=0.8\linewidth,keepaspectratio]{Remus_Latency_Overhead}
\caption{Latency Performance Impact of Remus Replication}
\label{fig:remus_latency_overhead}
\end{figure}

The graph in figure~\ref{fig:remus_latency_overhead} is a plot of average
latency for each of the 80 samples set for a total of 8,000 samples
transfer. For experiment E1 with no Remus replication, it was observed that the
latency fluctuated within a range depending upon the queue size of
\textit{WordCounter} and each of \textit{WordNormalizer} processes. For
experiment E2 with Remus replication, the average latency for sample transfer
did not have much deviation except for a few jitters.  This is because of the
fact that Remus replicates at a stable, predefined rate (here 40 ms), however,
due to network delays or delay in checkpoint commit, we observed jitters. These
jitters can be avoided by setting stricter deadline policies in which case, some
samples might get dropped and they might need to be resent. Hence, in case of no
failure, there is very little overhead for this real-time application. 

Figure~\ref{fig:remus_failover_overhead} is the result for experiment E3 where
\textit{WordReader} process on VM1 is replicated using Remus and it experienced
a failure condition. Before the failure, it can be observed that the latencies
were stable with few jitters due to the same reasons explained above.  When the
failure occurred, it took around 2 seconds for the failover to complete, during
which a few samples got lost. After the failover, no jitters were observed since
Remus replication has not yet started for VM1', but the latency showed more
variation as the system was still stabilizing from the last failure.  Thus, the
high availability solution works for real time applications even though a minor
impact is present during the failover. 

\begin{figure}[htb] 
\centering
\includegraphics[width=0.8\linewidth,keepaspectratio]{Remus_Failover_Overhead}
\caption{DDS Ownership QoS with Remus Failover}
\label{fig:remus_failover_overhead}
\end{figure}

Table~\ref{table:failover_overhead} represents the missed ratio for different
failover experiments performed. In experiments E4 and E5, VM2 failed and the
WordNormalizer process failed over to VM3. Since the DDS failover relied on
publisher/subscriber mechanism, the number of lost samples is low. The
presence of Remus replication process on WordReader process node -- VM1 did not
have any adverse effect on the reliability of the system.  However, in case of
experiments E3 and E6, where Remus failover took place, the number of lost
samples was higher since the failover duration is higher in case of Remus
replication than DDS failover. These experiments show that ongoing Remus
replication does not affect the performance of DDS failover, even though Remus
failover is slower than DDS failover. However, since DDS does not provide any
quality of service for source high availability, infrastructure-level high
availability provided by Remus must be used. 

\begin{table}[htb]
\begin{center}
\caption{Failover Impact on Sample Missed Ratio} % title of Table
\label{table:failover_overhead} % is used to refer this table in the text

    \begin{tabular}{ | >{\centering}p{3.1cm} | >{\centering}p{4cm} | p{4cm}<{\centering}|}
    \hline
    & Missed Samples 

(total of 8000) 
& Missed Samples 
Percentage (\%) \\ \hline
    Experiment 3 & 221 & 2.76\\ \hline
    Experiment 4 &  33 & 0.41  \\ \hline
    Experiment 5 &  14 & 0.18\\ \hline
    Experiment 6 &  549 & 6.86\\
    \hline
    \end{tabular}
\end{center}
\end{table}

