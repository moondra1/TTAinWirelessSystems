/////////////////////////////////////////////////////////////////////
////                                                             ////
////  Simple NJL5501R (+patient) Test Bench					     ////
////                                                             ////
////  SystemC Version: 2.3.0                                     ////
////  Author: Peter Volgyesi, MetaMorph, Inc.                    ////
////          pvolgyesi@metamorphsoftware.com                    ////
////                                                             ////
////                                                             ////
/////////////////////////////////////////////////////////////////////
#ifndef TEST_NJL5501R_H
#define TEST_NJL5501R_H

#include <systemc.h>
#include "njl5501r.h"

SC_MODULE(test_njl5501r) {

	sc_in<bool>    clk;

	// DUT ports
	sc_out<sc_uint<10> >	red_led;
	sc_out<sc_uint<10> >	ir_led;
	int ctr;
	int tem;
	int tem2;
	void init(void) {
		ctr=ctr+1;
		if(ctr==1)
		{
			red_led.write(0);
			ir_led.write(1023);
		}
		if((ctr<=20)&&(ctr!=1)){
			tem=ir_led.read();
			tem2=red_led.read();
		red_led.write(tem/2);
		ir_led.write(tem2/2);
		}
		if(!(tem+tem2))
		{
			ctr=0;
			sc_stop();
		}
	}

	SC_CTOR(test_njl5501r) {
		ctr=0;
		SC_METHOD(init);
		sensitive << clk;
		
	}
};

#endif 