

#ifndef NJL5501R_H
#define NJL5501R_H
#include <systemc.h>


SC_MODULE(njl5501r) {


	// ports
	sc_in<sc_uint<10> >	red_led;
	sc_in<sc_uint<10> >	ir_led;
	sc_out<sc_uint<10> > signal;
	sc_in<bool> clk;
	int		phase;
	//private:
	//sc_clock clk;


	void sig_gen()
	{
		
		int	RED_OFFSET=5 ;
		int	RED_AMPLITUDE=5;
		int	IR_OFFSET=8;
		int	IR_AMPLITUDE=2;
		int N_PATTERN=30;
		int PATTERN1[30] ={0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -10, -20, 10, 50, 90, 90, 90, 90, 40, 5, 0, 0};
		sc_uint<10> out1;
		int temp4= (100*RED_OFFSET + RED_AMPLITUDE * PATTERN1[phase])/100;
		int temp3= (100*IR_OFFSET + IR_AMPLITUDE * PATTERN1[phase])/100;
		int temp5 =(ir_led.read()*temp3)/10;
		int temp6 =(red_led.read()*temp4)/10;
		int temp7 =temp5 + temp6;
		int temp= (ir_led.read() *(100*IR_OFFSET + IR_AMPLITUDE * PATTERN1[phase]));
		int temp2 = (red_led.read() *(100*RED_OFFSET + RED_AMPLITUDE * PATTERN1[phase]));
		out1=temp7;
		phase = (phase + 1) % N_PATTERN;
		signal.write(out1);

	}



	/*SC_HAS_PROCESS(njl5501r);

	njl5501r(sc_module_name mname, int hr, int num, int p, int red_offset, int red_ampl, int ir_offset, int ir_ampl) : sc_module(mname),phase(p),RED_OFFSET(red_offset),IR_OFFSET(ir_offset),RED_AMPLITUDE(red_ampl),IR_AMPLITUDE(ir_ampl),N_PATTERN(num)
		// NOTE: TODO:      SC_MS should be here ---^ but simulation would be slow*/
	SC_CTOR(njl5501r)
	{
		phase=0;
		SC_METHOD(sig_gen);
		sensitive << clk;
	}

};

#endif // NJL5501R_H