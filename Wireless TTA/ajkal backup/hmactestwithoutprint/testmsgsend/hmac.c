#include <linux/module.h> /* Needed by all modules */ 
#include <linux/kernel.h> /* Needed for KERN_INFO */ 
#include <linux/init.h> /* Needed for the macros */ 
#include <linux/crypto.h> 
#include <linux/err.h> 
#include <linux/scatterlist.h> 
#include <linux/gfp.h> 
#include <crypto/hash.h> 
/* * Desired output: 694c7f0b84c0a8f0043c20aea9355a7eb51647f3 */ 
static char hmac[40]; 
static char *key = "passphrase"; 
//static char *mystring = "22"; 
struct hmac_sha1_result 
{ struct completion completion; 
int err; 
};
 static void hmac_sha1_complete(struct crypto_async_request *req, int err) { 
struct hmac_sha1_result *r=req->data; 
if(err==-EINPROGRESS) 
return; 
r->err=err;
 complete(&r->completion);
 }

 static int hmac_sha1(char *key, size_t klen, // key and key length 
			char *data_in, size_t dlen, // data in and length 
char *hash_out, size_t outlen)// hash buffer and length) 
{  
int rc=0; 
struct crypto_ahash *tfm; 
struct scatterlist sg; 
struct ahash_request *req;
 struct hmac_sha1_result tresult;
 void *hash_buf;
 int len = 20;
 char hash_tmp[20]; 
char *hash_res = hash_tmp; /* Set hash output to 0 initially */ 
memset(hash_out, 0, outlen); 
init_completion(&tresult.completion);
 tfm=crypto_alloc_ahash("hmac(sha1)",0,0);
 if(IS_ERR(tfm)) { printk(KERN_ERR "hmac_sha1: crypto_alloc_ahash failed.\n"); rc=PTR_ERR(tfm); goto err_tfm;
 }
 if(!(req=ahash_request_alloc(tfm,GFP_KERNEL))) 
{ printk(KERN_ERR "hmac_sha1: failed to allocate request for hmac(sha1)\n"); rc=-ENOMEM; goto err_req;
 }

 if(crypto_ahash_digestsize(tfm)>len) 
{ printk(KERN_ERR "hmac_sha1: tfm size > result buffer.\n");
 rc=-EINVAL; goto err_req; 
}

 ahash_request_set_callback(req,CRYPTO_TFM_REQ_MAY_BACKLOG, hmac_sha1_complete,&tresult); 

if(!(hash_buf=kzalloc(dlen,GFP_KERNEL))) 
{ printk(KERN_ERR "hmac_sha1: failed to kzalloc hash_buf");
 rc=-ENOMEM; goto err_hash_buf; 
} 
memcpy(hash_buf,data_in,dlen); 
sg_init_one(&sg,hash_buf,dlen); 
crypto_ahash_clear_flags(tfm,-0); 
if((rc=crypto_ahash_setkey(tfm,key,klen)))
{ printk(KERN_ERR "hmac_sha1: crypto_ahash_setkey failed\n");
 goto err_setkey;
 }
 ahash_request_set_crypt(req,&sg,hash_res,dlen);
 rc=crypto_ahash_digest(req);
 switch(rc)
 { 
case 0: 
while (len--) 
{ snprintf(hash_out, outlen, "%02x", (*hash_res++ & 0x0FF)); 
hash_out += 2;
 } break; 

case -EINPROGRESS: 
case -EBUSY:
 rc=wait_for_completion_interruptible(&tresult.completion); 
if(!rc && !(rc=tresult.err)) 
{ 
INIT_COMPLETION(tresult.completion); 
break; 
}
 else 
{ printk(KERN_ERR "hmac_sha1: wait_for_completion_interruptible failed\n"); 
goto out; 
} 

default: goto out; 
} 

out: 
err_setkey: 
kfree(hash_buf); 
err_hash_buf:
 ahash_request_free(req); 
err_req:
 crypto_free_ahash(tfm); 
err_tfm: return rc; 
}

 //static int __init init_main(void)

 

//{ printk(KERN_INFO "[CRYPTO] -> Successfully loaded crypto module.\n");
 //hmac_sha1(key, strlen(key), mystring, strlen(mystring), hmac, sizeof(hmac));
 //printk(KERN_INFO "FINAL MAC:%s", hmac);
 //return 0;
 //}

 //static void __exit cleanup_main(void) 
//{
 //#if DEBUG > 0
 //printk(KERN_INFO "[CRYPTO] < - Successfully unloaded crypto module.\n");
 //#endif 
//} 
//module_init(init_main); 
//module_exit(cleanup_main); 
/* *	Declaring code as GPL. */ 
MODULE_LICENSE("GPL");
 MODULE_AUTHOR("Andrei Sambra - andrei.sambra@telecom-sudparis.eu"); /* Who wrote this module? */ 
MODULE_DESCRIPTION("SHA1-HMAC"); /* What does this module do */
