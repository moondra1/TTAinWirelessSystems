#include <linux/module.h>
#include <linux/init.h>
#include <linux/in.h>
#include <linux/inet.h>
#include <net/sock.h>
#include <linux/string.h>
#include <linux/moduleparam.h>
#include <linux/timer.h>
#include <linux/jiffies.h>
#include <linux/gpio.h>
//#include "hmac.c"

#define SERVERPORT 5555
int gpio=26;
int err;
ktime_t currtime , interval;
static struct hrtimer hr_timer;
ktime_t ktime;
static struct socket *clientsocket=NULL;
int j=0;
int len;
//char buf[64];
char outmessage[51]="helloworld";
struct msghdr msg;
  struct iovec iov;
  mm_segment_t oldfs;
  struct sockaddr_in to;


//static char hmac[40];
//static char *key = "passphrase";

enum hrtimer_restart my_hrtimer_callback( struct hrtimer *timer )
{

//printk(KERN_ALERT"Inside Timer Routine count-> %d",j++);
	j++;
  	oldfs = get_fs();
  	set_fs( KERNEL_DS );
	len = sock_sendmsg( clientsocket, &msg, 51 );
	gpio_set_value(gpio,j%2);
	set_fs( oldfs );
	currtime  = ktime_get();
  	interval = ktime_set(0,10000000); 
  	hrtimer_forward(timer, currtime , interval);
  //printk( KERN_ERR "sock_sendmsg returned: %d\n", len);
	
//hrtimer_start( &hr_timer, ktime, HRTIMER_MODE_REL );

  
  return HRTIMER_RESTART;
}


static int __init client_init( void )
{
 err=gpio_request_one(gpio ,GPIOF_OUT_INIT_LOW ,"LED");
   if (err!=0)
{
  printk(KERN_ALERT "GPIO ACQ failed");
   return -EIO;
    }

  ktime = ktime_set(0, 10000000);

  hrtimer_init( &hr_timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL );
  
  hr_timer.function = &my_hrtimer_callback;
	hrtimer_start( &hr_timer, ktime, HRTIMER_MODE_REL );
  printk(KERN_ERR "sendthread initialized\n");
  if( sock_create( PF_INET,SOCK_DGRAM,IPPROTO_UDP,&clientsocket)<0 )
	{
 	   printk( KERN_ERR "server: Error creating clientsocket.n" );
    	   return -EIO;
   	}
  
  /*HMAC*/
        /*printk(KERN_INFO "[CRYPTO] -> Successfully loaded crypto module.\n");
        memset(&outmessage[0], 0, sizeof(outmessage));
       
        printk("message len: %i message: %s\n", strlen(message), message); 
           
        strncpy(outmessage,message,11);
        printk("message len: %i inmessage: %s\n", strlen(outmessage), outmessage);    

        hmac_sha1(key, strlen(key), outmessage, 11, hmac, sizeof(hmac));
        printk("message len: %i  FINAL MAC:%s \n",strlen(hmac), hmac);
        
        int temp=0;
        int ptr = 11;
        while(temp<40)
        {   
            outmessage[ptr++]=  hmac[temp++];
        } */
	//memset(&buf[0],0,sizeof(buf));
        //memset(&outmessage[0], 0, sizeof(outmessage));

       // printk("message len: %i hmacmessage: %s\n", strlen(outmessage), outmessage); 

  memset(&to,0, sizeof(to));
  to.sin_family = AF_INET;
  to.sin_addr.s_addr = in_aton( "192.168.0.1");
   /* destination address */
  to.sin_port = htons( (unsigned short) SERVERPORT );
  memset(&msg,0,sizeof(msg));
  msg.msg_name = &to;
  msg.msg_namelen = sizeof(to);
iov.iov_base = &outmessage;
  	iov.iov_len  = 51;
  	msg.msg_control = NULL;
  	msg.msg_controllen = 0;
 	 msg.msg_iov    = &iov;
  	msg.msg_iovlen = 1;
  //Sending message
  
  return 0;
}

static void __exit client_exit( void )
{
  if( clientsocket )
    sock_release( clientsocket );
   int ret;

  ret = hrtimer_cancel( &hr_timer );
  if (ret) printk("The timer was still in use...\n");
gpio_free(gpio);

    printk( KERN_ERR "send exit" );
}

 module_init( client_init );
 module_exit( client_exit );
 MODULE_LICENSE("GPL");
