
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/kthread.h>

#include <linux/errno.h>
#include <linux/types.h>

#include <linux/netdevice.h>
#include <linux/ip.h>
#include <linux/in.h>

#include <linux/mutex.h>

#include <linux/delay.h>
#include <linux/gpio.h>

#include "ISIS_Crypto.h"

#define DEFAULT_PORT 5555
#define CONNECT_PORT 6666
#define MODULE_NAME "ksocket"



struct msghdr msg;
struct iovec iov;
mm_segment_t oldfs;
int err;
int gpio=26;
int gpio2=221;
int j=1;
int i=1;
int tag_flag=0;
struct msghdr msgR;
struct iovec iovR;
mm_segment_t oldfsR;



char inmessage[1250];
char message[25];
char outmessage[51]="Id no 3 at your service master";

int len=51;
unsigned long delay_frame=4*2000;

int k=0;
int l=0;
int text_len = 10;

unsigned char *key = "isis";
int key_len = 4;
unsigned char digest[64];
unsigned char aux_tag[64];







struct kthread_t
{
        struct task_struct *thread;
        struct socket *sock;
        struct sockaddr_in addr;
        struct socket *sock_send;
        struct sockaddr_in addr_send;
        int running;
};

struct kthread_t *kthread = NULL;

/* function prototypes */
int ksocket_receive(struct socket *sock);
int ksocket_send(struct socket *sock);

static void ksocket_start(void)
{
        int size, err;
        //int bufsize = 10;
        

        /* kernel thread initialization */
        
        kthread->running = 1;
        current->flags |= PF_NOFREEZE;

        /* daemonize (take care with signals, after daemonize() they are disabled) */
        daemonize(MODULE_NAME);
        allow_signal(SIGKILL);
        

        /* create a socket */
        if ( ( (err = sock_create(AF_INET, SOCK_DGRAM, IPPROTO_UDP, &kthread->sock)) < 0) ||
             ( (err = sock_create(AF_INET, SOCK_DGRAM, IPPROTO_UDP, &kthread->sock_send)) < 0 ))
        {
                printk(KERN_INFO MODULE_NAME": Could not create a datagram socket, error = %d\n", -ENXIO);
                goto out;
        }

        memset(&kthread->addr, 0, sizeof(struct sockaddr));
        memset(&kthread->addr_send, 0, sizeof(struct sockaddr));
        kthread->addr.sin_family      = AF_INET;
        kthread->addr_send.sin_family = AF_INET;

        kthread->addr.sin_addr.s_addr      = htonl(INADDR_ANY);
        kthread->addr_send.sin_addr.s_addr = in_aton( "192.168.0.10");

        kthread->addr.sin_port      = htons(DEFAULT_PORT);
        kthread->addr_send.sin_port = htons(CONNECT_PORT);

        if ( ( (err = kthread->sock->ops->bind(kthread->sock, (struct sockaddr *)&kthread->addr, sizeof(struct sockaddr) ) ) < 0) ||
               (err = kthread->sock_send->ops->connect(kthread->sock_send, (struct sockaddr *)&kthread->addr_send, sizeof(struct sockaddr), 0) < 0 ))
        {
                printk(KERN_INFO MODULE_NAME": Could not bind or connect to socket, error = %d\n", -err);
                goto close_and_out;
        }

        printk(KERN_INFO MODULE_NAME": listening on port %d\n", DEFAULT_PORT);

        /* main loop */
        for (;;)
        {
                
		memset(&inmessage, 0, sizeof(inmessage));
                size = ksocket_receive(kthread->sock);
		//printk(" the raw mesage %s\n" ,inmessage);
		

		//for(k=0; k<text_len; k++)
		//{
		//	message[k] = inmessage[k];
		//}
		//printk(" only the message %s\n" ,message);

		// Get TagHMAC_TAG_20BYTES
	l=0;
		
		//for(k=text_len; k < (HMAC_TAG_20BYTES + text_len); k++)
		//{
		//	aux_tag[l] = inmessage[k];
	//		l++;
	//	}
		



		//printk("the tag %s\n" , aux_tag);

		//Compare HMAC Tags
		

		//compare digest[i] with aux_tag[i]
		//isis_hmac_sha1(message, text_len, key, key_len, digest);
		tag_flag = 0;
		//for(k=0; k<HMAC_TAG_20BYTES; k++)
		//{
		//	if(digest[k] != aux_tag[k])
		//	{
		//		tag_flag = 1;
		//		break;
		//	}
		//}
	
		if(tag_flag==0)
		{
		gpio_set_value(gpio2,j%2);
        	j++;
		}
	
                if (signal_pending(current))
                        break;
		

                if (size < 0)
                        printk(KERN_INFO MODULE_NAME": error getting datagram, sock_recvmsg error = %d\n", size);
                //else 
                //{
                        //printk(KERN_INFO MODULE_NAME": received %d bytes\n", size);
                        /* data processing */
                        //printk("\n data: %s\n", inmessage);

                        /* sending */
                      	//delay(delay_frame);
                        //strcat(buf, "testing...");
			//gpio_set_value(gpio,i%2);
        		//i++;

                        //ksocket_send(kthread->sock_send);
              //}
        }

close_and_out:
        sock_release(kthread->sock);
        sock_release(kthread->sock_send);
        kthread->sock = NULL;
        kthread->sock_send = NULL;

out:
        kthread->thread = NULL;
        kthread->running = 0;
}

int ksocket_send(struct socket* sock)
{
        int size=0;
	if (sock->sk==NULL)
        return 0;

        oldfs = get_fs();
        set_fs(KERNEL_DS);
        size = sock_sendmsg(sock,&msg,len);
        set_fs(oldfs);

        return size;
}

int ksocket_receive(struct socket* sock)
{
        
        int size = 0;

        if (sock->sk==NULL) return 0;

        iovR.iov_base = &inmessage;
	iovR.iov_len = 1250;
        


	oldfsR = get_fs();
        set_fs(KERNEL_DS);
	
        size = sock_recvmsg(sock,&msgR,1250,msgR.msg_flags);
        set_fs(oldfsR);

        return size;
}

int __init ksocket_init(void)


{	err=gpio_request_one(gpio ,GPIOF_OUT_INIT_LOW ,"LED1");
  	 if (err!=0)
	{
  	  printk(KERN_ALERT "GPIO ACQ failed");
   	  return -EIO;
	}
        
       // int size = 0;
	err=gpio_request_one(gpio2 ,GPIOF_OUT_INIT_LOW ,"LED");
  	 if (err!=0)
	{
  	  printk(KERN_ALERT "GPIO ACQ failed");
   	  return -EIO;
	}

kthread = kmalloc(sizeof(struct kthread_t), GFP_KERNEL);
        memset(kthread, 0, sizeof(struct kthread_t));

        /* start kernel thread */
        kthread->thread = kthread_run((void *)ksocket_start, NULL, MODULE_NAME);
        if (IS_ERR(kthread->thread)) 
        {
                printk(KERN_INFO MODULE_NAME": unable to start kernel thread\n");
                kfree(kthread);
                kthread = NULL;
                return -ENOMEM;
        }

        

        iov.iov_base = &outmessage;
        iov.iov_len = 51;

        msg.msg_flags = 0;
        msg.msg_name = &(kthread->addr_send);
        msg.msg_namelen  = sizeof(struct sockaddr_in);
        msg.msg_control = NULL;
        msg.msg_controllen = 0;
        msg.msg_iov = &iov;
        msg.msg_iovlen = 1;
        msg.msg_control = NULL;

	


	iovR.iov_base = &inmessage;
        iovR.iov_len = 1250;

        msgR.msg_flags = 0;
        msgR.msg_name = &(kthread->addr);
        msgR.msg_namelen  = sizeof(struct sockaddr_in);
        msgR.msg_control = NULL;
        msgR.msg_controllen = 0;
        msgR.msg_iov = &iovR;
        msgR.msg_iovlen = 1;
        msgR.msg_control = NULL;

       
	return 0;
}

void __exit ksocket_exit(void)
{
        int err;
	gpio_free(gpio);
	gpio_free(gpio2);

        if (kthread->thread==NULL)
                printk(KERN_INFO MODULE_NAME": no kernel thread to kill\n");
        else 
        {
                
                err = kthread_stop(kthread->thread);
               

                /* wait for kernel thread to die */
                if (err < 0)
                        printk(KERN_INFO MODULE_NAME": unknown error %d while trying to terminate kernel thread\n",-err);
                else 
                {
                        while (kthread->running == 1)
			{
                                msleep(10);
                        printk(KERN_INFO MODULE_NAME": succesfully killed kernel thread!\n");
			}
                }
        }

        /* free allocated resources before exit */
        if (kthread->sock != NULL) 
        {
                sock_release(kthread->sock);
                kthread->sock = NULL;
        }

        kfree(kthread);
        kthread = NULL;

        printk(KERN_INFO MODULE_NAME": module unloaded\n");

}

/* init and cleanup functions */
module_init(ksocket_init);
module_exit(ksocket_exit);

/* module information */
MODULE_DESCRIPTION("kernel thread listening on a UDP socket (code example)");
MODULE_AUTHOR("Toni Garcia-Navarro <topi@phreaker.net>");
MODULE_LICENSE("GPL");
