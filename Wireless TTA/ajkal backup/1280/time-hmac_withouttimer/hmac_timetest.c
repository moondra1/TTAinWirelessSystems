

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/time.h>
#include <linux/gpio.h>
#include "ISIS_Crypto.h"

#define DRIVER_AUTHOR "Peter Jay Salzman <p@dirac.org>"
#define DRIVER_DESC   "A sample driver"
//char key[10]="whatiswhat";
//char mystring[2]="22";
//char hmac[20];
struct timeval tv;
struct timeval start_tv;
int elapsed = 0;
char message[1280] = "helloworld";
int text_len = 1280;
unsigned char *key = "isis";
int key_len = 4;
unsigned char digest[64];
unsigned char aux_tag[64];
int i=0;
int gpio=26;
int err;


int init_hello_3(void);
void cleanup_hello_3(void);


static int init_hello_4(void)
{
err=gpio_request_one(gpio ,GPIOF_OUT_INIT_LOW ,"LED");
   if (err!=0)
{
  printk(KERN_ALERT "GPIO ACQ failed");
   return -EIO;
    }
//do_gettimeofday(&start_tv);
for(i=0;i<10000;i++){


isis_hmac_sha1(message, text_len, key, key_len, digest);
gpio_set_value(gpio,i%2); 

}
//do_gettimeofday(&tv);
//elapsed =  (tv.tv_usec - start_tv.tv_usec)/1000 ;
//printk("for 60 %d\n",elapsed);
//do_gettimeofday(&start_tv);

   return 0;
}


static void cleanup_hello_4(void)
{ gpio_free(gpio);

   printk(KERN_ALERT "Goodbye, world 4\n");
}


module_init(init_hello_4);
module_exit(cleanup_hello_4);


/*  You can use strings, like this:
 */
MODULE_LICENSE("GPL");           // Get rid of taint message by declaring code as GPL.

/*  Or with defines, like this:
 */
MODULE_AUTHOR(DRIVER_AUTHOR);    // Who wrote this module?
MODULE_DESCRIPTION(DRIVER_DESC); // What does this module do?

/*  This module uses /dev/testdevice.  The MODULE_SUPPORTED_DEVICE macro might be used in
 *  the future to help automatic configuration of modules, but is currently unused other
 *  than for documentation purposes.
 */
MODULE_SUPPORTED_DEVICE("testdevice");
