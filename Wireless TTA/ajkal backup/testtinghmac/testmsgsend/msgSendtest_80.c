#include <linux/inet.h>
#include <net/sock.h>
#include <linux/init.h>
#include <linux/string.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/hrtimer.h>
#include <linux/ktime.h>
#include <linux/gpio.h>


MODULE_LICENSE("GPL");


ktime_t ktime,currtime,interval;

int optval=1;
static struct hrtimer hr_timer;
int err;
int j=0;
int gpio=26;


#define SERVERPORT 5555

int err;
ktime_t ktime ,currtime ,interval;
static struct hrtimer hr_timer;
static struct socket *clientsocket=NULL;

int len;

char outmessage[71]="helloworld";
struct msghdr msg;
struct iovec iov;
mm_segment_t oldfs;
struct sockaddr_in to;

enum hrtimer_restart my_hrtimer_callback( struct hrtimer *timer )
{
	
	j++;
	oldfs = get_fs();
  	set_fs( KERNEL_DS );
	len = sock_sendmsg( clientsocket, &msg, 71 );
	gpio_set_value(gpio,j%2);
	set_fs( oldfs );
  
	currtime  = ktime_get();
  	interval = ktime_set(0,10000000); 
  	hrtimer_forward(timer, currtime , interval);
  
  	return HRTIMER_RESTART;
}

int init_module( void )
{
  err=gpio_request_one(gpio ,GPIOF_OUT_INIT_LOW ,"LED");
   if (err!=0)
{
  printk(KERN_ALERT "GPIO ACQ failed");
   return -EIO;
    }

if( sock_create( PF_INET,SOCK_DGRAM,IPPROTO_UDP,&clientsocket)<0 )
	{
 	   printk( KERN_ERR "server: Error creating clientsocket.n" );
    	   return -EIO;
   	}
if(sock_setsockopt(clientsocket, SOL_SOCKET, SO_BROADCAST, (char *)&optval,sizeof(int))<0)
	{
	printk("err settig bradcast flag");
	return EIO;
	}

  memset(&to,0, sizeof(to));
  to.sin_family = AF_INET;
  to.sin_addr.s_addr = in_aton( "192.168.0.255"); /* destination address */
  to.sin_port = htons( (unsigned short) SERVERPORT );
  memset(&msg,0,sizeof(msg));
  msg.msg_name = &to;
  msg.msg_namelen = sizeof(to);
  iov.iov_base = &outmessage;
  iov.iov_len  = 71;
  msg.msg_control = NULL;
  msg.msg_controllen = 0;
  msg.msg_iov    = &iov;
  msg.msg_iovlen = 1;
  

  printk("HR Timer module installing\n");

  ktime = ktime_set( 2, 0);

  hrtimer_init( &hr_timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL );
  
  hr_timer.function = &my_hrtimer_callback;
  hrtimer_start( &hr_timer, ktime, HRTIMER_MODE_REL );

  

  hrtimer_start( &hr_timer, ktime, HRTIMER_MODE_REL );

  return 0;
}

void cleanup_module( void )
{
  int ret;

  if( clientsocket )
    sock_release( clientsocket );

  ret = hrtimer_cancel( &hr_timer );
  if (ret) printk("The timer was still in use...\n");

  gpio_free(gpio);

  printk("HR Timer module uninstalling\n");

  return;
}
