#include <linux/module.h> 
#include <linux/init.h> 
#include <linux/in.h> 
#include <net/sock.h> 
#include <linux/skbuff.h> 
#include <linux/delay.h> 
#include <linux/inet.h> 
#include <linux/string.h> 
#include <linux/gpio.h>
#include <linux/hrtimer.h>
#include <linux/ktime.h>
//#include "hmac.c" 
 
#define SENDING_PORT 6666
#define REC_PORT 5555 
static struct socket *udpsocket=NULL; /*receiving socket */
static struct socket *clientsocket=NULL; /*sending socket */
ktime_t ktime;
unsigned long delay_frame;
static struct hrtimer hr_timer; 
int j=1;
int i=1;
int gpio=26;
int gpio2=221;
int err;
int id=3;
char inmessage[51];
struct msghdr msg;
struct iovec iov;
mm_segment_t oldfs;
struct sockaddr_in to; /*sending part*/
struct sockaddr_in server; /*receiving part */
char outmessage[51]="Id no 3 at your service master";    
 
//static char hmac[40]; 
//static char *key = "passphrase"; 

static DECLARE_COMPLETION( threadcomplete ); 
struct workqueue_struct *wq; 
 
struct wq_wrapper{ 
        struct work_struct worker; 
    struct sock * sk; 
}; 
 enum hrtimer_restart my_hrtimer_callback( struct hrtimer *timer )
{
int len=0;
	printk(KERN_ALERT"Inside Timer Routine count-> %d",j++);
	memset(&msg,0,sizeof(msg));
        msg.msg_name = &to;
        msg.msg_namelen = sizeof(to);
	iov.iov_base = &outmessage;
  	iov.iov_len  = 51;
  	msg.msg_control = NULL;
  	msg.msg_controllen = 0;
 	 msg.msg_iov    = &iov;
  	msg.msg_iovlen = 1;
  	oldfs = get_fs();
  	set_fs( KERNEL_DS );
        len = sock_sendmsg( clientsocket, &msg, 51 );
	gpio_set_value(gpio,j%2);
	set_fs( oldfs );
       // printk( KERN_ERR "sock_sendmsg returned: %d\n", len);
	return HRTIMER_NORESTART;
}

struct wq_wrapper wq_data; 
 
static void cb_data(struct sock *sk, int bytes){ 
    wq_data.sk = sk; 
    queue_work(wq, &wq_data.worker); 
} 
 
void send_answer(struct work_struct *data){ 
    struct  wq_wrapper * foo = container_of(data, struct  wq_wrapper, worker); 
    int len = 0; 
   
 /* as long as there are messages in the receive queue of this socket*/ 
        while((len = skb_queue_len(&foo->sk->sk_receive_queue)) > 0){ 
        struct sk_buff *skb = NULL; 
        //char temp_message[20]; 
        //char inmessage[51]; 
        //char message[51]; 
	//char check[41]; 
        /* receive packet */ 
        skb = skb_dequeue(&foo->sk->sk_receive_queue);
	
	//gpio_set_value(gpio,j%2);
	gpio_set_value(gpio2,i%2);
        i++;
 
                 
    //printk("message len: %i message: %s\n", skb->len - 8, skb->data +8); /*8 for udp header*/ 
	memset(&inmessage[0], 0, sizeof(inmessage));    
	//memset(&check[0], 0, sizeof(check));  
	//memset(&temp_message[0], 0, sizeof(temp_message));   
	//memset(&message[0], 0, sizeof(message));    
	strncpy(inmessage,skb->data +8,51); 

//if(strcmp(inmessage,"helloworld")==0)

//{hrtimer_start( &hr_timer, ktime, HRTIMER_MODE_REL );
//} 
//else
//printk("in else bhiya");   
 
        //strncpy(temp_message,inmessage,11); 
        //printk("message len: %i inmessage: %s\n", strlen(inmessage), inmessage);     
	//printk("message len: %i temp_message: %s\n", strlen(temp_message), temp_message); 
	//printk(KERN_INFO "[CRYPTO] -> Successfully loaded crypto module.\n"); 
       // hmac_sha1(key, strlen(key), temp_message, 11, hmac, sizeof(hmac)); 
       // printk("message len: %i  FINAL MAC:%s \n",strlen(hmac), hmac); 
         
	//strncpy(message,inmessage,51); 
        //int temp=0; 
        //int ptr = 0; 
	 
	//for (temp=11,ptr=0;temp<51;temp++,ptr++) 
	//{	 
	//	check[ptr]=inmessage[temp]; 
	//} 
	 
      //  check[41]='\0'; 
	//printk("message len: %i  Recieved MAC:%s \n",strlen(check), check); 
	//if (strncmp(check,  hmac,40) == 0) 
		//{ 
		 //printk("Cheers: %s matched with %s\n", check, hmac); 
		//} 
		//else 
		//{ 
		 //printk("Got %s instead of %s\n", check, hmac); 
 
		//} 
 
        kfree_skb(skb); 
    } 

} 
 
static int __init server_init( void ) 
{ 
    
err=gpio_request_one(gpio ,GPIOF_OUT_INIT_LOW ,"LED");
   if (err!=0)
{
  printk(KERN_ALERT "GPIO ACQ failed");
   return -EIO;
}
  err=gpio_request_one(gpio2 ,GPIOF_OUT_INIT_LOW ,"LED1");
   if (err!=0)
{
  printk(KERN_ALERT "GPIO ACQ failed");
   return -EIO;
}
  
  delay_frame=id*1400000;
  ktime = ktime_set( 0, delay_frame);
  hrtimer_init( &hr_timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL );
  hr_timer.function = &my_hrtimer_callback;
  
  
  


     
    printk("INIT MODULE\n"); 
    /* socket to receive data */ 
    if (sock_create(PF_INET, SOCK_DGRAM, IPPROTO_UDP, &udpsocket) < 0) { 
        printk( KERN_ERR "server: Error creating udpsocket.n" ); 
        return -EIO; }
 /*socket to send Data*/
    if (sock_create(PF_INET, SOCK_DGRAM, IPPROTO_UDP, &clientsocket) < 0) { 
        printk( KERN_ERR "server: Error creating udpsocket.n" ); 
        return -EIO;}
     


    memset(&to,0, sizeof(to));
    memset(&server,0, sizeof(server));	

    server.sin_family = AF_INET; 
    server.sin_addr.s_addr = INADDR_ANY; 
    server.sin_port = htons( (unsigned short)REC_PORT); 

    to.sin_family = AF_INET;
    to.sin_addr.s_addr = in_aton( "192.168.0.3"); /*destination broadcasting*/
    to.sin_port = htons( (unsigned short)SENDING_PORT);
   
    err = udpsocket->ops->bind(udpsocket, (struct sockaddr *) &server, sizeof  	(server)); 
    if (err) { 
        sock_release(udpsocket); 
        return -EIO; 
    } 
    udpsocket->sk->sk_data_ready = cb_data; 

    
     
    /* create work queue */     
    INIT_WORK(&wq_data.worker, send_answer); 
    wq = create_singlethread_workqueue("myworkqueue"); 
    if (!wq){ 
        return -ENOMEM; 
    } 
 
    return 0; 
} 
 
static void __exit server_exit( void ) 
{ 
    if (udpsocket) 
        sock_release(udpsocket); 
    if (clientsocket) 
        sock_release(clientsocket); 
 
    if (wq) { 
                flush_workqueue(wq); 
                destroy_workqueue(wq); 
    } 
   gpio_free(gpio);
gpio_free(gpio2);
 hrtimer_cancel( &hr_timer );
  
    printk("EXIT MODULE"); 
} 
 
module_init( server_init ); 
module_exit( server_exit ); 
MODULE_LICENSE("GPL");
