#include <linux/module.h>
#include <linux/version.h>
#include <linux/types.h>
#include <linux/delay.h>
#include <asm/io.h>
#include <linux/init.h>
#include <linux/kernel.h>

#include <linux/serial_reg.h>
#define MOD_NAME serialmodule
#define SERIAL_PORT_BASE 0x70006000

int __init enter_module(void)

{
	
	
	 if( !request_region(SERIAL_PORT_BASE, 8, MOD_NAME ) )
        printk(KERN_ERR "error requesting addr");
	 //turn off dlab
   outb((~(UART_LCR_DLAB)), SERIAL_PORT_BASE+UART_LCR);
	 
	 //disable interrupts
    outb((~(UART_IER_MSI|UART_IER_RLSI|UART_IER_THRI|UART_IER_RDI)), SERIAL_PORT_BASE+UART_IER);
			
			

	
	while(1)
	{ 
		outb(UART_MCR_RTS, SERIAL_PORT_BASE+UART_MCR);
		
		msleep(500);
		
		
		outb((~(UART_MCR_RTS)), SERIAL_PORT_BASE+UART_MCR);
		msleep(500);
	}
	return 0;
}

void __exit cleanup_module()
{ release_region(SERIAL_PORT_BASE, 8, MOD_NAME);
}

module_init(enter_module);
module_exit(cleanup_module);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("ARul Moondra_ISIS");
MODULE_DESCRIPTION("RTS SQAURE WAVE");