  
 int open_port(void)
 {
   int fd; /* File descriptor for the port */


   fd = open("/dev/ttyS0", O_RDWR | O_NOCTTY | O_NDELAY);
   if (fd == -1)
   {
    /*
     * Could not open the port.
     */

     fprintf(stderr, "open_port: Unable to open /dev/ttym1 - %s\n",
             strerror(errno));
   }

   return (fd);
 }

 

  n = write(fd, "A", 4);
  if (n < 0)
    puts("write() of 4 bytes failed!");

  close(fd);